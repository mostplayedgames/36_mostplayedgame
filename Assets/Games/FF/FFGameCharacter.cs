﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FFGameCharacter : MonoBehaviour
{
	protected Rigidbody2D rb2d;
	protected BoxCollider2D collider2d;

	private Vector2 baseColliderSize;

	public float size = 1.0f;

	public float enemySpeed = 30;

	protected virtual void Awake()
	{
		rb2d = GetComponent<Rigidbody2D>();
		collider2d = GetComponent<BoxCollider2D>();
		baseColliderSize = collider2d.size;
	}

	protected virtual void OnEnable()
	{
		if (CompareTag("Enemy"))
		{
			rb2d.velocity = new Vector2(-enemySpeed, 0);
		}

		SetSize(size);
	}

	protected virtual void Update()
	{
		if (transform.position.x <= -70) gameObject.SetActive(false);
	}

	public void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.GetComponent<FFGameCharacter>()) AttemptEat(collision.GetComponent<FFGameCharacter>());
	}

	public virtual void AttemptEat (FFGameCharacter other)
	{
		if (other.size <= size)
		{
			other.GetEaten();
		}
	}

	public virtual void GetEaten()
	{
		// Destroy(this.gameObject);
		gameObject.SetActive(false);
	}

	public virtual void SetSize(float newSize)
	{
		size = newSize;
		transform.localScale = new Vector3(newSize * (CompareTag("Enemy") ? 1 : -1), newSize, newSize);
		baseColliderSize *= size;
	}
}
