﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FFGameInstance : MPGameInstance 
{
	public FFGamePlayer m_objMainCharacter;
	public GameInstanceStates m_curState;

	public Color m_cameraColor;

	public GameObject m_enemy;

	public float m_enemySpawnTime = 1.0f;
	public float m_enemySpawnTimer = 0.0f;

	public Vector3 spawnPoint;
	public float spawnRange = 20;

	public void Awake()
	{
		ZObjectMgr.Instance.AddNewObject(m_enemy.name, 10, this.gameObject);
	}

	public override void Gamescene_SetupLevel ()
	{
		m_curState = GameInstanceStates.IDLE;

		Camera.main.backgroundColor = m_cameraColor;

		m_objMainCharacter.gameObject.SetActive(true);
		m_objMainCharacter.transform.position = new Vector3(-40, 0, 63.8f);
		m_objMainCharacter.transform.localEulerAngles = new Vector3(0, 180, 0);

		// Set Camera
		Camera.main.gameObject.GetComponent<Camera>().orthographicSize = 100;//163;
		Camera.main.gameObject.GetComponent<Camera>().transform.position = new Vector3(0, 0, -250);
		Camera.main.gameObject.GetComponent<Camera>().transform.eulerAngles = new Vector3(0, 0, 0);

		m_enemySpawnTimer = Time.time;

		ZObjectMgr.Instance.ResetAll();
	}

	public override void Gamescene_Score ()
	{
		GameScene.instance.AddCoins(1);
	}

	public override void Gamescene_ButtonUp()
	{
		if (m_curState == GameInstanceStates.IDLE)
		{
			m_curState = GameInstanceStates.INGAME;
		}
	}

	public override void Gamescene_Die ()
	{
		
	}

	public override void Gamescene_Update ()
	{
		if (m_curState == GameInstanceStates.INGAME)
		{
			if (!m_objMainCharacter.isActive) m_objMainCharacter.Activate();
			if (m_enemySpawnTimer + m_enemySpawnTime < Time.time)
			{
				SpawnEnemy(Random.Range(0.5f, 1.5f));
				m_enemySpawnTimer = Time.time;
			}
		}

	}

	public override void Gamescene_ButtonDown (){}

	private void UpdateCameraMovement()
	{
		LeanTween.cancel(Camera.main.gameObject);
		LeanTween.moveLocalY(Camera.main.gameObject, m_objMainCharacter.transform.localPosition.y + 58.0f, 0.3f);
	}

	public void SpawnEnemy(float size)
	{
		//Instantiate(m_enemy, new Vector3(spawnPoint.x, Random.Range(spawnPoint.y - (spawnRange / 2.0f), spawnPoint.y + (spawnRange / 2.0f)), spawnPoint.z), Quaternion.identity);
		GameObject newEnemy= ZObjectMgr.Instance.Spawn3D(m_enemy.name, new Vector3(spawnPoint.x, Random.Range(spawnPoint.y - (spawnRange / 2.0f), spawnPoint.y + (spawnRange / 2.0f)), spawnPoint.z));
		newEnemy.GetComponent<FFGameCharacter>().SetSize(size);
	}

	public void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawCube(spawnPoint, new Vector3(2, spawnRange));
	}
}
