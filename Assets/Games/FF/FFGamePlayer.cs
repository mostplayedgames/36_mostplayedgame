﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FFGamePlayer : FFGameCharacter 
{
	public float ySpeed = 5.0f;
	public bool isActive = false;

	protected override void Awake()
	{
		rb2d = GetComponent<Rigidbody2D>();
	}

	public void Activate(bool activated=true)
	{
		isActive = activated;
	}

	public void Update()
	{
		if (isActive)
		{
			if (Input.GetKey(KeyCode.Mouse0) || Input.touchCount >= 1) rb2d.velocity = new Vector2(0, -ySpeed);
			else rb2d.velocity = new Vector2(0, ySpeed);
		}
		else
		{
			rb2d.velocity = Vector2.zero;
		}
	}

	public void LateUpdate()
	{
		transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -80, 80), transform.position.z);
	}

	public override void AttemptEat(FFGameCharacter other)
	{
		if (other.size < size)
		{
			other.GetEaten();
			GameScene.instance.Score(1);
			Grow();
		}
	}

	public void Grow()
	{
		SetSize(size + 0.01f);
	}

	public override void GetEaten()
	{
		if (isActive)
		{
			// Destroy(this.gameObject);
			gameObject.SetActive(false);
			Activate(false);
			GameScene.instance.Die();
		}
	}
}
