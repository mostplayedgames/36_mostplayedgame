﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TJGameCharacter : MonoBehaviour
{
	protected Rigidbody2D rb2d;
	protected BoxCollider2D collider2d;

	private Vector2 baseColliderSize;

	public float enemySpeed = 30;

	protected virtual void Awake()
	{
		rb2d = GetComponent<Rigidbody2D>();
		collider2d = GetComponent<BoxCollider2D>();
		baseColliderSize = collider2d.size;
	}

	protected virtual void OnEnable()
	{
		if (CompareTag("Enemy"))
		{
			rb2d.velocity = new Vector2(0 , -enemySpeed);
		}
	}

	protected virtual void Update()
	{
		if (transform.position.y <= -120)
		{
			GameScene.instance.Score(1);
			gameObject.SetActive(false);
		}
	}

	public void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.GetComponent<TJGameCharacter>()) collision.GetComponent<TJGameCharacter>().GetHit();
	}

	public virtual void GetHit()
	{
		rb2d.velocity = Vector2.zero;
	}
}
