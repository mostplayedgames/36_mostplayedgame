﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TJGamePlayer : TJGameCharacter 
{
	public bool isActive = false;
	public bool left = true;

	protected override void Awake()
	{
		rb2d = GetComponent<Rigidbody2D>();
	}

	public void Activate(bool activated=true)
	{
		isActive = activated;
	}

	public void Update()
	{
		if (isActive)
		{
			if (Input.GetKeyDown(KeyCode.Mouse0))
			{
				left = !left;

				transform.position = new Vector3((left ? -25 : 25), -60, 64);
			}
		}
	}

	public void LateUpdate()
	{
		transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -80, 80), transform.position.z);
	}

	public override void GetHit()
	{
		if (isActive)
		{
			gameObject.SetActive(false);
			Activate(false);
			GameScene.instance.Die();
		}
	}
}
