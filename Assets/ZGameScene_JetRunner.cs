﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum JetRunnerState
{
	IDLE,
	IN_GAME,
	RESULTS
}

public class ZGameScene_JetRunner : FDGamesceneInstance 
{
	public JetRunnerCharacter m_mainCharacter;

	public GameObject m_prefabObstacle;
	public int m_colorIdx 				= 0;
	public float m_speed 				= 0;
	public float m_addedSpeed 			= 0;
	public List<Color> m_listOfColors 	= new List<Color>();
	public JetRunnerState m_curState;

	private int m_charDirection = 1;
	private float m_currentX = -44f;
	private float m_currentY = -114f;
	private float m_currentPlatformIdx = 0;

	public void Awake()
	{
		ZObjectMgr.Instance.AddNewObject (m_prefabObstacle.name, 20, this.gameObject);

		m_colorIdx = Random.Range (0, m_listOfColors.Count);
		SetCameraColor();

		m_curState = JetRunnerState.IDLE;
	}

	public override void Gamescene_Setuplevel (bool hardreset = false)
	{
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = 137.8f;
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().transform.localPosition = new Vector3 (0, 28f, -250f);
		ZCameraMgr.instance.transform.localEulerAngles = new Vector3 (0, 0, 0);
		Camera.main.orthographicSize = 178.5f;
	
		SetCameraColor();
		//m_curState = JetRunnerState.IN_GAME;
	}

	public override void Gamescene_ButtonDown ()
	{
		
	}

	public override void Gamescene_ButtonUp ()
	{
		if (m_curState == JetRunnerState.IDLE) 
		{
			m_curState = JetRunnerState.IN_GAME;
			//StartMoving ();
		}
	}

	public override void Gamescene_Score ()
	{
		
	}
		
	public override void Gamescene_Update ()
	{
		if (m_curState != JetRunnerState.IN_GAME) {
			return;
		}

		FollowCamera ();
		Controls ();
		CharacterMovement ();
		MonitorAcceleratedSpeed ();
	}

	public override void Gamescene_Die ()
	{

	}

	public override void Gamescene_Unloadlevel ()
	{
		this.gameObject.SetActive (false);
	}

	private void SpawnPlatform()
	{

	}

	private void SpawnLevel()
	{
		GameObject spawnedPlatform = ZObjectMgr.Instance.Spawn2D (m_prefabObstacle.name, Vector2.zero);
		float addToCurrentX = 0;


		// 1 - Look for X
	}

	private void SetCameraColor()
	{
		m_colorIdx++;

		if (m_colorIdx >= m_listOfColors.Count - 1) 
		{
			m_colorIdx = 0;
		}

		Camera.main.backgroundColor = m_listOfColors [m_colorIdx];
	}

	private void Controls()
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			ChangeCharacterDirection ();
			m_speed = m_addedSpeed;
		}

	}

	private void ChangeCharacterDirection()
	{
		m_charDirection = (m_charDirection == 1) ? (-1) : (1);

		if (m_charDirection == 1) {
			Debug.Log ("Go Up");
			m_mainCharacter.transform.localEulerAngles = new Vector3 (0, 0, 40);
		} else if (m_charDirection == -1) {
			Debug.Log ("Go Down");
			m_mainCharacter.transform.localEulerAngles = new Vector3 (0, 0, -40);
		}
	}

	private void StartMoving()
	{
		//m_mainCharacter.GetComponent<Rigidbody2D> ().velocity = new Vector3 (255, 0, 0);
		m_mainCharacter.GetComponent<Rigidbody2D>().velocity = Vector3.right * 100;
	}

	private void MonitorAcceleratedSpeed()
	{

	}

	private void CharacterMovement()
	{
		m_mainCharacter.transform.Translate (Vector3.right * m_speed * Time.deltaTime);
	//	Debug.Log ("CharacterVelocity" + m_mainCharacter.GetComponent<Rigidbody2D> ().velocity);
	//	m_mainCharacter.transform.position += Vector3.right * m_speed * Time.deltaTime;
	}

	private void FollowCamera()
	{
		ZCameraMgr.instance.transform.position = new Vector3(m_mainCharacter.gameObject.transform.position.x + 40f, 15, -250f);
	}
}
