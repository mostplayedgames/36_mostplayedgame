﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StacksGameSceneState
{
	IDLE,
	IN_GAME,
	RESULTS
}

public class ZGameScene_Stacks :  FDGamesceneInstance
{
	public FDStacksCharacter m_mainCharacter;
	public int m_colorIdx;
	public StacksGameSceneState m_curState;
	public float m_spawnTime = 0; 
	public float m_spawnTimeTarget = 1.5f;
	public int m_platformIdx;

	public List<GameObject> m_listOfHouses 			= new List<GameObject> ();
	public List<int> m_listOfDirections 			= new List<int> ();
	public List<int> m_listOfDifficulty 			= new List<int> ();
	public List<Color> m_listOfColors				= new List<Color>();

	public int m_curDirection;
	private int m_curDifficulty;
	private float m_currentY;


	public void Awake()
	{
		ZObjectMgr.Instance.AddNewObject (m_listOfHouses[0].name, 20,this.gameObject);
		ZObjectMgr.Instance.AddNewObject (m_listOfHouses[1].name, 20,this.gameObject);
		ZObjectMgr.Instance.AddNewObject (m_listOfHouses[2].name, 20,this.gameObject);

		m_curDirection = Random.Range(0, m_listOfDirections.Count);

		m_colorIdx = Random.Range (0, m_listOfColors.Count - 1);
		Camera.main.backgroundColor = m_listOfColors[m_colorIdx];
		m_curState = StacksGameSceneState.IDLE;
	}
		
	public override void Gamescene_Setuplevel (bool hardreset = false)
	{
		m_mainCharacter.gameObject.transform.position = new Vector3 (0, -59f, 55f);
		m_mainCharacter.Initialize ();
		//m_mainCharacter.Initialize ();

		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = 137.8f;
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().transform.localPosition = new Vector3 (0, 28f, -250f);
		ZCameraMgr.instance.transform.localEulerAngles = new Vector3 (9.8f, 0, 0);
		Camera.main.orthographicSize = 178.5f;

		m_spawnTime = 0;
		m_spawnTimeTarget = 1;
		m_currentY = -55f;
		m_platformIdx = 0;

		m_colorIdx++;

		if (m_colorIdx >= m_listOfColors.Count - 1) 
		{
			m_colorIdx = 0;
		}
		Camera.main.backgroundColor = m_listOfColors[m_colorIdx];
		m_curState = StacksGameSceneState.IDLE;
		m_mainCharacter.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
		//SpawnLevel ();
	}

	public override void Gamescene_Update ()
	{
		Controls ();
		SpawnLogic ();
		//ZCameraMgr.instance.transform.position = new Vector3(0,m_mainCharacter.gameObject.transform.localPosition.y + 5f, -254f);
	}

	public override void Gamescene_ButtonDown ()
	{

	}

	public void UpdateCameraMovement()
	{
		LeanTween.cancel (Camera.main.gameObject);
		LeanTween.moveLocalY (Camera.main.gameObject, m_mainCharacter.gameObject.transform.localPosition.y + 58f, 0.5f);
	}

	public override void Gamescene_ButtonUp ()
	{
		if(m_curState == StacksGameSceneState.IDLE)
		{
			SpawnPlatform ();
			m_curState = StacksGameSceneState.IN_GAME;
			m_mainCharacter.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;
		}
	}

	public override void Gamescene_ButtonHold (){}

	public override void Gamescene_Score ()
	{
		UpdateCameraMovement ();
		GameScene.instance.AddCoins (1);
		//SpawnPlatform ();
	}

	public override void Gamescene_Die (){}

	public override void Gamescene_Unloadlevel ()
	{
		this.gameObject.SetActive (false);
	}

	public void OnColliderEnter2D(Collider2D p_other)
	{
		if (p_other.gameObject.tag == "EndPlatform") 
		{
			GameScene.instance.Die ();
		}
	}

	private void SpawnLogic()
	{
		m_spawnTime += Time.deltaTime;

		if (m_spawnTime >= m_spawnTimeTarget) 
		{
			m_spawnTime = 0;
			m_spawnTimeTarget = 1f;
			SpawnPlatform ();
		}
	}

	private GameObject GetRandomPlatform()
	{
		string objName = "";
		float randVal = Random.Range (0, 90);

		if (randVal < 30) {
			objName = m_listOfHouses [0].name;
		} else if (randVal < 60) {
			objName = m_listOfHouses [1].name;
		} else if (randVal < 90) {
			objName = m_listOfHouses [2].name;
		}

		return ZObjectMgr.Instance.Spawn2D (objName, Vector2.zero);;
	}

	private Vector2 GetDirection(int p_direction)
	{
		Vector2 retVal = Vector2.zero;
		if (p_direction == 1) {
			retVal = new Vector2 (110, m_currentY);
		} else if (p_direction == -1) {
			retVal = new Vector2 (-110, m_currentY);
		}

		return retVal;
	}

	private float GetDifficulty(int p_difficulty)
	{
		if (p_difficulty == 0) {
			return Random.Range (1.3f, 1.8f);
		} else if (p_difficulty == 1) {
			return Random.Range (1f, 1.3f);
		} else if (p_difficulty == 2) {
			return Random.Range (0.5f, 0.8f);
		}
		return 0.5f;
	}


	public void SpawnPlatform()
	{
		m_curDirection++;
		if (m_curDirection > m_listOfDirections.Count - 1) {
			m_curDirection = 0;
		}
			
		m_curDifficulty++;
		if (m_curDifficulty > m_listOfDifficulty.Count - 1) {
			m_curDifficulty = 0;
		}


		GameObject platform = GetRandomPlatform ();
		Vector2 startPos = GetDirection (m_listOfDirections[m_curDirection]);
		float speed = GetDifficulty (m_listOfDifficulty[m_curDifficulty]);

		platform.transform.localPosition = new Vector3(startPos.x, startPos.y, 55.7f);
		m_platformIdx++;

		if (speed == 0) {
			Debug.Log ("TEST");
		}

		platform.GetComponent<StackPlatform> ().Move (m_platformIdx, m_curDirection, speed, 0);

		m_currentY += 29f;
	}

	private void Controls()
	{
		if (!m_mainCharacter.IsGrounded () || m_curState != StacksGameSceneState.IN_GAME) 
		{
			return;
		}

		if (Input.GetMouseButtonDown (0)) 
		{
			m_mainCharacter.Jump ();
		}
	}
}
