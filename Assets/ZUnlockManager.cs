﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZUnlockManager : ZSingleton<ZUnlockManager> {


	public GameObject m_rootUnlock;
	public GameObject m_rootUnlockObject;
	public GameObject m_objectPanindaButton;
	public GameObject m_objectCloseButton;

	public GameObject m_objectGift;
	public GameObject m_objectCelebrationParticle;
	public GameObject m_objectUnlockPlayButton;
	public GameObject m_objectShareUnlock;

	public Sprite m_currentUnlockImage;

	public GameObject m_unlockBackgroundImage;

	public AudioClip m_audioSwoosh;
	public AudioClip m_audioWinSpacey;
	public AudioClip m_audioSuccess;
	public AudioClip m_audioButton;

	public Text m_textUnlockText;
	public Text m_textPaninda;
	public Text m_textUnlock;
	public Text m_textItemUnlocked;

	public int m_itemId;

	void Awake()
	{
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.P)) 
		{
			ShowUnlock (1,1);
		}
	}

//	void ShowUnlock()
//	{
//		m_textPaninda.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -117);
//		m_objectGift.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
//		m_rootUnlockObject.transform.localScale = Vector3.zero;
//
//		m_objectShareUnlock.SetActive (false);
//
//		m_objectGift.transform.localPosition = new Vector3 (0, -10, 0);
//		m_rootUnlock.SetActive (true);
//		m_objectGift.transform.localPosition += new Vector3 (0, 40f, 0);
//		LeanTween.moveLocal(m_objectGift, m_objectGift.transform.localPosition - new Vector3 (0, 40f, 0), 0.5f).setEase(LeanTweenType.easeOutBounce);
//
//		m_textUnlockText.text = "" + GetCurrentShopPrice () + " THROWS";
//		m_objectUnlockPlayButton.SetActive (false);
//
//		m_textPaninda.text = "SPINNER AVAILABLE!";
//		m_objectPanindaButton.SetActive (true);
//		ZAudioMgr.Instance.PlaySFX (m_audioButton);
//
//		ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
//	}

	public void CloseButton()
	{
		m_rootUnlockObject.gameObject.SetActive (false);
		m_rootUnlock.gameObject.SetActive (false);
		GameScene.instance.ShowShop ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void UnlockBirdAnim()
	{
		BuyBirdFromScreen (m_itemId);

		// + EDIT JOSH if object is image
		m_objectGift.GetComponent<SpriteRenderer>().color = new Color(1,1,1,1);
		m_rootUnlockObject.gameObject.SetActive (true);
		LeanTween.alpha(m_objectGift, 0, 0.75f);


		LeanTween.scale (m_rootUnlockObject, new Vector3 (40.0f, 40.0f, 40.0f), 1.5f).setOnComplete(UnlockBirdAnim2).setEase(LeanTweenType.easeOutExpo);

		//LeanTween.scale (m_rootUnlockObject, new Vector3 (2f, 2f, 2f), 1.5f).setOnComplete(UnlockBirdAnim2).setEase(LeanTweenType.easeOutExpo);
		//m_rootUnlockObject.GetComponent<Image> ().sprite = m_currentUnlockImage;

		m_textPaninda.text = "";
		m_objectPanindaButton.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		//GameScene.instance.ConfirmItemPurchase(m_itemId);
	}

	void UnlockBirdAnim2()
	{
		int itemId = m_itemId;
		m_textPaninda.text = "UNLOCKED!";// + m_currentUnlockedName + "!";
		//m_textItemUnlocked.text = "" + GameScene.instance.m_listShopBirds[m_itemId].m_textName;
		LeanTween.scale (m_textPaninda.gameObject, new Vector3 (0.7f, 0.7f, 0.7f), 0.2f).setEase (LeanTweenType.easeInOutCubic).setLoopCount (2).setLoopPingPong ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		LeanTween.delayedCall (3f, UnlockBirdAnim3);

		ZAudioMgr.Instance.PlaySFX (m_audioSuccess);
		ZAudioMgr.Instance.PlaySFX (m_audioWinSpacey);

		m_objectCelebrationParticle.gameObject.SetActive (false);
		m_objectCelebrationParticle.gameObject.SetActive (true);
	}

	void UnlockBirdAnim3()
	{
		//m_objectPanindaButton.SetActive (true);
		m_objectUnlockPlayButton.SetActive (true);
		LeanTween.scale (m_objectUnlockPlayButton, new Vector3 (0.18f, 0.18f, 0.18f), 0.2f).setEase (LeanTweenType.easeInOutCubic).setLoopCount (2).setLoopPingPong ();
		m_objectShareUnlock.SetActive (true);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		//m_isCharacterUnlocked = false;
	}


	public void PlayFromUnlock()
	{
		m_rootUnlockObject.gameObject.SetActive (false);
		m_rootUnlock.SetActive (false);
		m_objectCelebrationParticle.gameObject.SetActive (false);
		GameScene.instance.m_rootUIScore.gameObject.SetActive (true);
		GameScene.instance.m_rootMain.gameObject.SetActive (true);
		LeanTween.alpha (m_unlockBackgroundImage, 0, 0.2f);
		//UseBird(m_currentItemToUnlock);
		GameScene.instance.SetItem(m_itemId);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		GameScene.instance.m_objectChangeModeBtn.SetActive (true);
		GameScene.instance.m_eState = GAME_STATE.IDLE;
	}

	public void ShowUnlock(int p_itemId, int p_price)
	{
		m_textPaninda.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -117);
		m_objectGift.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
		m_rootUnlockObject.transform.localScale = Vector3.zero;

		m_objectShareUnlock.SetActive (false);

		m_objectGift.transform.localPosition = new Vector3 (0, 80, 0);
		m_rootUnlock.SetActive (true);
		//m_objectGift.transform.localPosition += new Vector3 (0, 30f, 0);
		LeanTween.moveLocal(m_objectGift, m_objectGift.transform.localPosition - new Vector3 (0, 60f, 0), 0.5f).setEase(LeanTweenType.easeOutBounce);


		m_textUnlockText.text = "" + p_price + " FLIPS";
		m_objectUnlockPlayButton.SetActive (false);

		m_textPaninda.text = "UNLOCK NOW!";
		m_textItemUnlocked.text = "";
		m_objectPanindaButton.SetActive (true);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		m_itemId = p_itemId;

		ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
	}

	public void BuyBirdFromScreen(int p_itemId)
	{

		for (int idx = 0; idx < m_rootUnlockObject.transform.childCount; idx++) 
		{
			m_rootUnlockObject.transform.GetChild (idx).gameObject.SetActive (false);
		}

		m_rootUnlockObject.transform.GetChild (p_itemId).gameObject.SetActive (true);

//		int count = m_listPurchaseOrder[m_currentPurchaseCount];
//
//		if (m_coins >= m_listPrices[m_currentPurchaseCount]) {
//			m_coins -= m_listPrices[m_currentPurchaseCount];
//			PlayerPrefs.SetInt ("Hit", m_coins);
//			PlayerPrefs.SetInt ("birdbought" + count, 1);
//
//			ZAnalytics.Instance.SendSpendShopItemEvent ("birdbought" + count, m_listPrices [m_currentPurchaseCount])
//
//			m_currentPurchaseCount++;
//			PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
//			PlayerPrefs.Save ();
//
//			m_textHit.text = m_coins + "/" + GetCurrentShopPrice() + " Tusoks";
//			//m_textHit2.text = m_coins + "/" + GetCurrentShopPrice() + " Tusoks";
//			//ZAudioMgr.Instance.PlaySFX (m_audioButton);
//			//ShopScene.instance.SetupScene ();
//
//			m_currentItemToUnlock = count;
//			m_currentUnlockImage = m_listShopBirds [count].spriteImage;
//			m_currentUnlockedName = m_listShopBirds [count].name;
//
//			UpdateTopBar ();
//			//UpdateBar ();
//		}
	}
}
