﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

	public GameObject m_objectHouse;
	public GameObject m_objectHouseGreen;
	public GameObject m_objectBushLeft;
	public GameObject m_objectBushRight;
	public GameObject m_objectBushFrontLeft;
	public GameObject m_objectBushFrontRight;
	public GameObject m_objectStone1;
	public GameObject m_objectStone2;
	public GameObject m_objectStone3;
	public GameObject m_objectTrash;

	public List<Material> m_listHouseMaterials;

	// Use this for initialization
	void Start () {
		Reset ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Reset()
	{
		GameObject objectHouse = m_objectHouse;
		//if (Random.Range (0, 100) > 80) {
		//	objectHouse = m_objectHouseGreen;
		//	m_objectHouse.gameObject.SetActive (false);
		//	//m_objectHouseGreen.gameObject.SetActive (true);
		//} else {
		//	objectHouse = m_objectHouse;
		//	m_objectHouse.gameObject.SetActive (true);
		//	//m_objectHouseGreen.gameObject.SetActive (false);
		//}


		int randomHouseColor = Random.Range (0, 100);

		if (randomHouseColor > 40) {
			m_objectHouse.GetComponent<Renderer> ().material = m_listHouseMaterials [0];
		} else if (randomHouseColor > 30) {
			m_objectHouse.GetComponent<Renderer> ().material = m_listHouseMaterials [2];
		} else {
			m_objectHouse.GetComponent<Renderer> ().material = m_listHouseMaterials [1];
		}


		//objectHouse.transform.localPosition = new Vector3 (0, 0, 6.4f);//Random.Range (-22f, 70f));
		objectHouse.transform.localPosition = new Vector3 (0, 0, 20f);//Random.Range(6.4f, 20f));//Random.Range (-22f, 70f));

		m_objectBushRight.transform.localPosition = new Vector3 (38.2f, -42f, Random.Range (m_objectHouse.transform.localPosition.z - 30f, -92f));
		m_objectBushLeft.transform.localPosition = new Vector3 (-32.2f, -42f, Random.Range (m_objectHouse.transform.localPosition.z - 30f, -92f));
		m_objectTrash.transform.localPosition = new Vector3 (19.2f, -42f, Random.Range (m_objectHouse.transform.localPosition.z - 30f, -92f));
	}
}
