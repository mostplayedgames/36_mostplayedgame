﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TapToBounceState
{
	IDLE,
	IN_GAME,
	RESULTS
}

public class ZGameScene_TapToBounce : FDGamesceneInstance 
{
	public TapToBounceCharacter m_mainCharacter;
	public int m_colorIdx;
	public TapToBounceState m_curState;

	public List<Color> m_listOfColors				= new List<Color>();

	private Ray ray;
	private RaycastHit hit;

	public void Awake()
	{
//		ZObjectMgr.Instance.AddNewObject (m_listOfHouses[0].name, 20,this.gameObject);
//		ZObjectMgr.Instance.AddNewObject (m_listOfHouses[1].name, 20,this.gameObject);
//		ZObjectMgr.Instance.AddNewObject (m_listOfHouses[2].name, 20,this.gameObject);


		m_colorIdx = Random.Range (0, m_listOfColors.Count - 1);
		Camera.main.backgroundColor = m_listOfColors[m_colorIdx];
		m_curState = TapToBounceState.IDLE;
	}

	public override void Gamescene_Setuplevel (bool hardreset = false)
	{
		m_mainCharacter.gameObject.transform.localPosition = new Vector3 (0, -20f, 55f);
		m_mainCharacter.transform.localEulerAngles = Vector3.zero;
		//m_mainCharacter.Initialize ();

		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = 137.8f;
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().transform.localPosition = new Vector3 (0, 28f, -250f);
		ZCameraMgr.instance.transform.localEulerAngles = new Vector3 (0, 0, 0);
		Camera.main.orthographicSize = 178.5f;


		m_colorIdx++;

		if (m_colorIdx >= m_listOfColors.Count - 1) 
		{
			m_colorIdx = 0;
		}
		Camera.main.backgroundColor = m_listOfColors[m_colorIdx];
		m_curState = TapToBounceState.IDLE;
		m_mainCharacter.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
		//SpawnLevel ();
	}

	public override void Gamescene_Update ()
	{
		Controls ();
		//ZCameraMgr.instance.transform.position = new Vector3(0,m_mainCharacter.gameObject.transform.localPosition.y + 5f, -254f);
	}

	public override void Gamescene_ButtonDown ()
	{

	}

	public void UpdateCameraMovement()
	{
		LeanTween.cancel (Camera.main.gameObject);
		LeanTween.moveLocalY (Camera.main.gameObject, m_mainCharacter.gameObject.transform.localPosition.y + 58f, 0.5f);
	}

	public override void Gamescene_ButtonUp ()
	{

	}

	public override void Gamescene_ButtonHold (){}

	public override void Gamescene_Score ()
	{
		GameScene.instance.AddCoins (1);
	}

	public override void Gamescene_Die (){}

	public override void Gamescene_Unloadlevel ()
	{
		this.gameObject.SetActive (false);
	}

	public void OnColliderEnter2D(Collider2D p_other)
	{
		if (p_other.gameObject.tag == "EndPlatform") 
		{
			GameScene.instance.Die ();
		}
	}
		
	private void Controls()
	{
//		if (m_curState != TapToBounceState.IN_GAME) 
//		{
//			return;
//		}

		//Converting Mouse Pos to 2D (vector2) World Pos
//		Vector2 rayPos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
//		RaycastHit2D hit=Physics2D.Raycast(rayPos, Vector2.zero, 0f);
//
//		if (hit)
//		{
//			Debug.Log(hit.transform.name);
//		//	return hit.transform.gameObject;
//		}

//		RaycastHit hit;
//		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
//
//		if (Physics.Raycast(ray, out hit)) {
//			Transform objectHit = hit.transform;
//			Debug.Log("" + objectHit.name);
//			// Do something with the object that was hit by the raycast.
//		}

//		ray = Camera.main.ScreenPointToRay (Input.mousePosition);
//		if (Physics.Raycast (ray, out hit, 10000.0f)) 
//		{
//			Debug.Log ("hitName: " + hit.collider.name);
//			
//			if (Input.GetMouseButtonDown (0)) 
//			{
//				if (hit.collider.tag == "Player") 
//				{
//					Jump ();
//				}
//			}
//		}

		if (Input.GetMouseButtonDown(0)) 
		{
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

			RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

			if (hit.collider != null) 
			{
				if (m_curState == TapToBounceState.IDLE) 
				{
					m_curState = TapToBounceState.IN_GAME;
					m_mainCharacter.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;
					Jump (mousePos2D);
				} else {
					Jump (mousePos2D);
				}
			}
		}
	}

	private void Jump(Vector2 p_mousePos)
	{
		float dist = Vector3.Distance (p_mousePos, m_mainCharacter.transform.localPosition);


		float sideX = m_mainCharacter.transform.localPosition.x - p_mousePos.x;
		float sideY = m_mainCharacter.transform.localPosition.y - (p_mousePos.y - 1);
		int directionX = (sideX > 0) ? 1: -1;
		int directionY = (sideY > 0) ? 1: -1;
		float xForce = (dist * directionX) * 100;
		Debug.Log ("Dist: " + dist + " Side: " + sideX);

		float minSide = (directionY < 0) ? 9000: 10000;
		float maxSide = (directionY < 0) ? 10000: 15000;
		//Random.Range (29500f, 39500f);//
		float upwardForce = (directionY < 0) ? Random.Range (29500f, 39500f) : Random.Range (20500f, 30500f);
		float sideForce = Random.Range (minSide, maxSide);

		m_mainCharacter.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
		m_mainCharacter.GetComponent<Rigidbody2D> ().angularVelocity = 0;

		m_mainCharacter.GetComponent<Rigidbody2D>().AddForce(Vector3.up * upwardForce);
		if (p_mousePos.x > m_mainCharacter.transform.localPosition.x + 6f) {
			m_mainCharacter.GetComponent<Rigidbody2D> ().AddForce (-Vector3.right * 9000);
		} else if (p_mousePos.x < m_mainCharacter.transform.localPosition.x - 6f) {
			m_mainCharacter.GetComponent<Rigidbody2D> ().AddForce (Vector3.right * 9000);
		}
		else {
		}
		m_mainCharacter.GetComponent<Rigidbody2D> ().AddTorque (200000 * directionX * -1);

		GameScene.instance.Score (1);
	}

//	private void OnMouseButtonDown()
//	{
//		ray = Camera.main.ScreenPointToRay (Input.mousePosition);
//		if (Physics.Raycast (ray, out hit, 10000.0f)) 
//		{
//			if (Input.GetMouseButtonDown (0)) 
//			{
//				if (hit.collider.tag != "Spinner") {
//					return;
//				}
//
//				ZRewardsMgr.instance.m_objectButtons.SetActive (false);
//				m_textModes.gameObject.SetActive (false);
//
//
//				if (m_curGameMode == "ENDLESS" || m_curGameMode == "LEVEL") 
//				{
//					if (!hit.collider.GetComponent<FidgetSpinner> ().IsTouchable (m_isFirstTouch)) {
//						return;
//					}
//
//					if (!m_isFirstTouch) 
//					{
//						m_isFirstTouch = true;
//					} 
//					else 
//					{
//						UpdateScore (1);
//						m_objectSuccessParticle.gameObject.SetActive (false);
//						m_objectSuccessParticle.gameObject.SetActive (true);
//						m_objectSuccessParticle.transform.position = hit.collider.transform.position;
//					}
//
//					if (!IsLowerPartOfScreen ()) {
//						return;
//					}
//
//					m_fidgetSpinner = hit.collider.gameObject;
//					m_fidgetSpinner.GetComponent<Rigidbody> ().useGravity = false;
//					m_initialTouch = hit.point;
//					m_mouseDelta = Vector3.zero;
//
//					m_objectMasterFidget.GetComponent<FidgetSpinner> ().SetIsHolding (true);
//				} 
//				else if (m_curGameMode == "TAP") 
//				{
//
//					if (!m_isFirstTouch) 
//					{
//						m_isFirstTouch = true;
//					} 
//					else 
//					{
//						UpdateScore (1);
//						m_objectSuccessParticle.gameObject.SetActive (false);
//						m_objectSuccessParticle.gameObject.SetActive (true);
//						m_objectSuccessParticle.transform.position = hit.collider.transform.position;
//					}
//
//					m_fidgetSpinner = hit.collider.gameObject;
//					m_fidgetSpinner.GetComponent<Rigidbody> ().useGravity = true;
//					m_fidgetSpinner.GetComponent<Rigidbody> ().velocity = Vector3.zero;
//
//					float upForce = Random.Range (1500.0f, m_upwardForce);
//
//					if (hit.point.x > m_fidgetSpinner.GetComponent<Collider>().bounds.center.x  + 0.15f) 
//					{
//						m_fidgetSpinner.GetComponent<Rigidbody> ().AddForce (Vector3.up * upForce);
//						m_fidgetSpinner.GetComponent<Rigidbody> ().AddForce (-Vector3.right * (m_sidewardForce * 0.5f));
//					} 
//					else if (hit.point.x < m_fidgetSpinner.GetComponent<Collider>().bounds.center.x  - 0.15f) 
//					{
//						m_fidgetSpinner.GetComponent<Rigidbody> ().AddForce (Vector3.up * upForce);
//						m_fidgetSpinner.GetComponent<Rigidbody> ().AddForce (Vector3.right * (m_sidewardForce * 0.5f));
//					} 
//					else 
//					{
//						m_fidgetSpinner.GetComponent<Rigidbody> ().AddForce (Vector3.up * upForce);
//					}
//
//					m_objectMasterFidget.GetComponent<FidgetSpinner> ().SetIsHolding (false);
//				}
//			}
//		}
//	}

}
