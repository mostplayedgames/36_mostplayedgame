﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Title: Zoid Trip Replica
// Developer: Joshua Anilao

public class ZTGameInstance : MPGameInstance 
{
	public GameObject m_objPlayer;
	public GameInstanceStates m_curState;

	public override void Gamescene_SetupLevel (){}
	public override void Gamescene_Score ()
	{
	}

	public override void Gamescene_Update ()
	{
		
	}

	public override void Gamescene_Die (){}
	public override void Gamescene_ButtonUp (){}
	public override void Gamescene_ButtonDown (){}

	private void Controls()
	{

	}
}
