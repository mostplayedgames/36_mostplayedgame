﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CHAR_STATE_LEVEL
{
	IDLE,
	CHARGE,
	JUMP
}

public enum MODE_STATE_LEVEL{
	IDLE,
	INGAME,
	DIE
}

public class FDGamesceneLevel : FDGamesceneInstance {
//	CHAR_STATE m_eState;
//	MODE_STATE_LEVEL m_eModeState;
//	
//	// Use this for initialization
//	void Start () {
//		m_eModeState = MODE_STATE_LEVEL.IDLE;
//	}
//	
//	// Update is called once per frame
//	public override void Gamescene_Update () {
//		if (m_eModeState == MODE_STATE_LEVEL.DIE)
//			return;
//		if (m_eModeState == MODE_STATE_LEVEL.IDLE)
//			return;
//		
//		m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (40f, m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity.y);
//		ZCameraMgr.instance.gameObject.transform.position = new Vector3(m_scriptCharacter.gameObject.transform.position.x - 5f, 35.1f, -90f);
//	}
//	
//	public override void Gamescene_ButtonDown () {
//		if (m_eModeState == MODE_STATE_LEVEL.DIE)
//			return;
//		if (m_eModeState == MODE_STATE_LEVEL.IDLE) {
//			m_eModeState = MODE_STATE_LEVEL.INGAME;
//			m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;
//		}
//		//m_scriptCharacter.Reset ();
//		//m_scriptCharacter.Charge ();
//		
//		//if (m_eState == CHAR_STATE.CHARGE)
//		//	return;
//		
//		//LeanTween.cancel (m_objectCharacter.gameObject);
//		//LeanTween.moveLocalY (m_objectCharacter.gameObject, 11.8f, 1.5f).setOnComplete(Jump);//.setEase(LeanTweenType.easeInQuad);
//		//m_currentChargeTime = Time.time;
//		
//		//m_eState = CHAR_STATE.CHARGE;
//		
//		LeanTween.cancel (ZCameraMgr.instance.gameObject);
//		//		if(m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().bodyType == RigidbodyType2D.Dynamic)
//		//			m_scriptCharacter.Flap ();
//	}
//	
//	public override void Gamescene_ButtonUp () {
//		
//	}
//	
//	public override void Gamescene_Die () {
//		if (m_eModeState == MODE_STATE_LEVEL.DIE)
//			return;
//		
//		//LeanTween.delayedCall (2f, Gamescene_Setuplevel);
//		m_eModeState = MODE_STATE_LEVEL.DIE;
//	}
//	
//	public override void Gamescene_Score () {
//		
//	}
//	
//	public override void Gamescene_Setuplevel(bool hardreset) {
//		
//		if (!hardreset && m_eModeState == MODE_STATE_LEVEL.IDLE)
//			return;
//		
//		//m_scriptCharacter.Reset ();
//		m_scriptCharacter.gameObject.transform.position = GameScene.instance.m_objectCharacterRoot.gameObject.transform.position;
//		m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
//		m_scriptCharacter.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
//		
//		GameScene.instance.ResetCamera ();
//		this.gameObject.SetActive (true);
//		
//		this.transform.localEulerAngles = Vector3.zero;
//		m_eState = CHAR_STATE.IDLE;
//		
//		m_eModeState = MODE_STATE_LEVEL.IDLE;
//		
//		Debug.LogWarning ("Setup Level");
//		GameScene.instance.m_eState = GAME_STATE.IDLE;
//		
//	}
//	
//	public override void Gamescene_Unloadlevel() {
//		this.gameObject.SetActive (false);
//		Debug.Log ("Unload Level");
//	}
}

