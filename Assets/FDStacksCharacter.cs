﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FDStacksCharacter : MPCharacter 
{
	public ZGameScene_Stacks m_gameSceneInstance;
	public bool m_isGrounded;
	public RaycastHit2D m_hit;
	public int m_curPatternIdx;
	public int m_platformPatternIdx;

	public bool IsGrounded()
	{
		return m_isGrounded;

	}

	public void Initialize()
	{
		m_curPatternIdx = 0;
		m_platformPatternIdx = 0;
	}

	public void Update()
	{

	}

	public void Jump()
	{
		this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, 18000f));
	}

	public void OnCollisionEnter2D(Collision2D p_other)
	{
		if (p_other.gameObject.tag == "Die") 
		{
			//LeanTween.cancel (p_other.gameObject);
			int dir = m_gameSceneInstance.m_listOfDirections[m_gameSceneInstance.m_curDirection];

			if (dir == 1) {
				this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-15000,15000));
			} else {
				this.GetComponent<Rigidbody2D>().AddForce(new Vector2(15000,15000));
			}

			m_gameSceneInstance.m_curState = StacksGameSceneState.RESULTS;

			GameScene.instance.Die ();
		} 
		else if (p_other.gameObject.tag == "Platform") 
		{
			LeanTween.cancel (p_other.gameObject);
			m_isGrounded = true;
			m_platformPatternIdx = p_other.gameObject.GetComponent<StackPlatform> ().m_platformIdx;
			bool didReachedEnding = p_other.gameObject.GetComponent<StackPlatform> ().DidReachedEnding ();

			if (m_gameSceneInstance.m_curState == StacksGameSceneState.IN_GAME &&
				m_platformPatternIdx > m_curPatternIdx)
			{
				int scoreValue = (didReachedEnding) ? (2) : (1);
				GameScene.instance.Score (scoreValue);
				m_curPatternIdx++;
			}
		}
	}

	public void OnCollisionExit2D(Collision2D p_other)
	{
		if (p_other.gameObject.tag == "Platform") {
			m_isGrounded = false;
		}
	}

	private bool CheckIfLandedOnRightPlatform()
	{
		return (m_platformPatternIdx <= m_curPatternIdx + 1) ? true : false;
	}
}
