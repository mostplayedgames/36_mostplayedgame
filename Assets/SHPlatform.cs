﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SHPlatform : MonoBehaviour 
{
	public GameObject m_objTarget;
	public float m_projectedSize;

	public void Initialize (float p_size) 
	{
		m_projectedSize = p_size;
		this.transform.localScale = new Vector3 (p_size, 70, 14);
		m_objTarget.transform.parent = null;
		m_objTarget.transform.localScale = new Vector3 (1.1f, 1.8f, 18f);
		m_objTarget.transform.parent = this.transform;
		m_objTarget.gameObject.SetActive (true);
	}

	public void PlatformReached()
	{
		m_objTarget.gameObject.SetActive (false);
	}

	public bool IsInCenter(Vector3 p_point)
	{
		float minX = m_objTarget.GetComponent<Renderer> ().bounds.center.x - m_objTarget.GetComponent<Renderer> ().bounds.extents.x;
		float maxX = m_objTarget.GetComponent<Renderer> ().bounds.center.x + m_objTarget.GetComponent<Renderer> ().bounds.extents.x;

		if (minX < p_point.x && maxX > p_point.x) {
			return true;
		} else {
			return false;
		}
	}

	public float GetBounds()
	{
		return this.transform.position.x + this.GetComponent<Renderer> ().bounds.extents.x - 10;
	}
}
