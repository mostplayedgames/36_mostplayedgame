﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MPModesButton : MonoBehaviour 
{
	public MPGameInstanceData m_gameData;
	public Image m_btnBG;
	public Image m_btnTitle;
	public Image m_badgeIcon;
	public Color[] m_badgeColors;
	public Text m_textBestScore;
	public Text m_textRank;
	public Vector2 pos1;
	public Vector2 pos2;
	public float dist;

	public void Initialize(MPGameInstanceData p_gameData, int p_rankNum)
	{
		m_gameData 				= p_gameData;
		m_btnTitle.sprite 		= m_gameData.TitleArtwork;
		m_btnBG.sprite 			= m_gameData.ButtonArtwork;
		m_btnBG.color 			= m_gameData.ColorTheme;
		m_textBestScore.text 	= "BEST " + m_gameData.GetBestScore();
		m_textRank.text		 	= "RANK: #" + (p_rankNum + 1) + "";
		m_badgeIcon.color		= m_badgeColors[m_gameData.GetBadge()];
		
	}

	public void RefreshData()
	{
		m_textBestScore.text 	= "BEST " + m_gameData.GetBestScore();
		m_badgeIcon.color = m_badgeColors[m_gameData.GetBadge()];
	}

	public void SelectMode()
	{
		int SelectedMode = m_gameData.ID;
		MPModeSelectManager.Instance.SetCurrentGame (SelectedMode);
		GameScene.instance.LoadLevelPreview ();
		GameScene.instance.m_eState = GAME_STATE.SHOP;
	}



	public void Update()
	{
		CheckIfOffscreen ();
	}

	private void CheckIfOffscreen()
	{
		pos1 = Camera.main.transform.position - new Vector3(0, 100, 0);
		pos2 = this.transform.position;
		dist = Vector2.Distance (pos2, pos1);

		if (dist > 150) {
			m_btnBG.gameObject.SetActive (false);
			m_btnTitle.gameObject.SetActive (false);
			m_textBestScore.gameObject.SetActive (false);
			m_textRank.gameObject.SetActive (false);
			this.GetComponent<Image> ().enabled = false;
		} else {
			m_btnBG.gameObject.SetActive (true);
			m_btnTitle.gameObject.SetActive (true);
			m_textBestScore.gameObject.SetActive (true);
			m_textRank.gameObject.SetActive (true);
			this.GetComponent<Image> ().enabled = true;
		}
	}
}
