﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackPlatform : MonoBehaviour {

	public int m_platformIdx;
	public int m_direction;
	public float m_speed;
	public float m_timer;
	private bool m_didReachCenter;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{

			
	}

	public void Move(int p_platformIdx,int p_direction, float p_speed, float p_timer)
	{
		m_timer = p_timer;
		m_speed = p_speed;
		if (p_speed == 0) {
			p_speed = 1;
		}
		m_platformIdx = p_platformIdx;
		m_direction = p_direction;
		LeanTween.cancel (this.gameObject);
		m_didReachCenter = false;
		LeanTween.rotateY (this.gameObject, 0, p_speed).setOnComplete (ReachedCenter);
		//LeanTween.moveLocalX (this.gameObject, 0 , p_speed).setOnComplete(ReachedCenter);
	}

	public bool DidReachedEnding()
	{
		return m_didReachCenter;
	}

	private void ReachedCenter()
	{
		m_didReachCenter = true;
	}
}
