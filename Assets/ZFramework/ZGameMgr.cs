﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;

[System.Serializable]
public class LeaderboardData
{
	public string name;
	public string saveName;
	public string leaderboardIOS;
	public string leaderboardAndroid;
}

public class ZGameMgr : MonoBehaviour {

	public static ZGameMgr instance;

	public List<ZGameScene> m_listGameScenes;

	public int NOTIFICATION_ID;

	public string RATEUS_URL_IOS;
	public string RATEUS_URL_ANDROID;

	public string FOLLOWUS_URL_IOS;
	public string FOLLOWUS_URL_ANDROID;

	public string LIKEUS_URL_IOS;
	public string LIKEUS_URL_ANDROID;

	public string SUBSCRIBE_URL_IOS;
	public string SUBSCRIBE_URL_ANDROID;

	public string FOLLOWTWITCH_URL_IOS;
	public string FOLLOWTWITCH_URL_ANDROID;

	public string MOREGAMES_URL_IOS;
	public string MOREGAMES_URL_ANDROID;

	public string BANNER_URL_IOS;
	public string BANNER_URL_ANDROID;

	public string INTERSTITIAL_URL_IOS;
	public string INTERSTITIAL_URL_ANDROID;

	public string CROSS_PROMO_ADS_ANDROID;
	public string CROSS_PROMO_ADS_IOS;

	public string IAP_ID_REMOVEADS;
	public string IAP_ID_COIN1;

	public string GAME_HASHTAG;

	public string INSTANT_NOTIFICATION_MESSAGE;
	public string NOTIFICATION_MESSAGE;
	public int NOTIFICATION_TIME;

	public List<LeaderboardData> m_listLeaderboard;

	public int UPDATE_NUMBER;

	public GameObject m_pauseObject;

	public bool isPause = false;
	public bool isOnline = false;

	public bool m_startInterrupt;

	// Use this for initialization
	void Awake () 
	{
		instance = this;
		Application.targetFrameRate = 60;
		isPause = false;

		m_startInterrupt = false;
	}

	void Start() 
	{
		InitializeManagers ();
		ShowScene ("GameScene");
		m_pauseObject.SetActive (false);
		RefreshInternet ();
		ZNotificationMgr.Instance.ClearNotifications ();
	}

	void InitializeManagers()
	{
		ZAdsMgr.Instance.enabled = true;
		ZRateUsMgr.Instance.enabled = true;
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public void AddScene(ZGameScene scene)
	{
		m_listGameScenes.Add (scene);
	}

	public void ShowScene(string name)
	{
		foreach(ZGameScene scene in m_listGameScenes){
			if( scene.gameObject.name == name ){
				scene.gameObject.SetActive(true);
			}
			else
			{	scene.gameObject.SetActive(false);
			}
		}
	}

	public void HideScene(string name)
	{
		foreach(ZGameScene scene in m_listGameScenes){
			if( scene.gameObject.name == name ){
				scene.gameObject.SetActive(false);
			}
		}
	}

	public void Resume()
	{
		m_pauseObject.SetActive (false);
		Time.timeScale = 1;
		isPause = false;
	}

	void OnApplicationPause( bool pauseStatus )
	{
		if (pauseStatus) 
		{
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.INSTANT_NOTIFICATION_MESSAGE, 120);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME * 4);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME * 12);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME * 20);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME * 28);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME * 34);

		} 
		else 
		{
			ZNotificationMgr.Instance.ClearNotifications ();
		}

		if (pauseStatus && GameScene.instance.m_eState == GAME_STATE.SHOOT) {
			m_pauseObject.SetActive (true);
			Time.timeScale = 0;
			isPause = true;
		} else {
			if (!m_startInterrupt) {
				LeanTween.delayedCall (0.01f, FocusRoutine);
			} else {
				m_startInterrupt = false;
			}
		}
	}

	void FocusRoutine()
	{
		ZGameMgr.instance.RefreshInternet ();
	}

	public void RefreshInternet()
	{
		//StartCoroutine ("CheckInternet");
		CheckInternetReachability();
	}

	void CheckInternetReachability(){
		switch (Application.internetReachability) {
		case NetworkReachability.NotReachable:
			isOnline = false;
			break;
		case NetworkReachability.ReachableViaCarrierDataNetwork:
			isOnline = true;
			break;
		case NetworkReachability.ReachableViaLocalAreaNetwork:
			isOnline = true;
			break;
		default :
			isOnline = false;
			break;
		}
	}

	IEnumerator CheckInternet()
	{
		string HtmlText = GetHtmlFromUri("http://google.com");
		if(HtmlText == "")
		{
			isOnline = false;
		}
		else if(!HtmlText.Contains("schema.org/WebPage"))
		{
			//Redirecting since the beginning of googles html contains that 
			//phrase and it was not found

			isOnline = false;
		}
		else
		{
			isOnline = true;
		}

		return null;
	}

	string GetHtmlFromUri(string resource)
	{
		string html = string.Empty;
		HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
		req.Timeout = 5000;
		try
		{
			using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
			{
				bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
				if (isSuccess)
				{
					using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
					{
						//We are limiting the array to 80 so we don't have
						//to parse the entire html document feel free to 
						//adjust (probably stay under 300)
						char[] cs = new char[80];
						reader.Read(cs, 0, cs.Length);
						foreach(char ch in cs)
						{
							html +=ch;
						}
					}
				}
			}
		}
		catch
		{
			return "";
		}
		return html;
	}
}
