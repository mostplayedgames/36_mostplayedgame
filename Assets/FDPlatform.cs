﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FDPlatform : MonoBehaviour {

	public float m_platformIdx;

	public void InitializePlatform(float p_platformIdx, int p_behaviour)
	{
		m_platformIdx = p_platformIdx;
		SetBehaviour (p_behaviour);
	}

	public float GetPlatformIdx()
	{
		return m_platformIdx;
	}

	private void SetBehaviour(int p_behaviour)
	{
		//Debug.Log ("PlatformBehaviour: " + p_behaviour);
	}
}
