﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BDGameInstance : MPGameInstance 
{
	public BDCharacter m_objCharacter;
	public GameObject m_prefabTarget;
	public ParticleSystem m_prefabParticlehit;

	public GameObject m_objTopObstacle;
	public GameObject m_objBelowObstacle;

	public GameInstanceStates m_curState;

	public Color m_colorBG;
	public int m_colorIdx;

	public float m_forceAdded;
	public float m_currentY;

	public int m_currentTuningIdx;

	public List<Vector3> m_listOfEasyObstacles = new List<Vector3>();
	public List<Vector3> m_listOfObstacles = new List<Vector3> ();
	public List<Vector3> m_listOfHardObstacles = new List<Vector3>();

	public List<Color> m_listOfColors = new List<Color>();

	public void Awake()
	{
		ZObjectMgr.Instance.AddNewObject (m_prefabTarget.name, 20, this.gameObject);
		ZObjectMgr.Instance.AddNewObject (m_prefabParticlehit.name, 10, this.gameObject);
		m_currentTuningIdx = Random.Range (0, m_listOfObstacles.Count);
	}

	public override void Gamescene_SetupLevel ()
	{
		m_currentY = 0;

		// Set Camera
		Camera.main.gameObject.GetComponent<Camera>().orthographicSize = 100;
		Camera.main.gameObject.GetComponent<Camera> ().transform.position = new Vector3 (0, 75, -250);
		Camera.main.gameObject.GetComponent<Camera> ().transform.eulerAngles = new Vector3 (7, 0, 0);
		Camera.main.backgroundColor = m_colorBG;
		m_objCharacter.Initialize ();

		Vector3 topVec = new Vector3 (0, m_objCharacter.transform.localPosition.y + 150, 0);
		Vector3 botVec = new Vector3 (0, m_objCharacter.transform.localPosition.y - 100, 0);


		m_objTopObstacle.transform.localPosition = topVec;
		m_objBelowObstacle.transform.localPosition = botVec;

		m_curState = GameInstanceStates.IDLE;
		SpawnTarget ();
	}

	public override void Gamescene_Update ()
	{
		if (m_curState == GameInstanceStates.INGAME) 
		{
			Controls ();
		}
	}

	public override void Gamescene_Die (){}

	public override void Gamescene_Score ()
	{
		UpdateCameraMovement ();
		SpawnTarget ();
	}

	public override void Gamescene_ButtonUp ()
	{
		if (m_curState != GameInstanceStates.INGAME) 
		{
			m_curState = GameInstanceStates.INGAME;	
		}
	}
	public override void Gamescene_ButtonDown (){}

	public void SpawnParticle(Vector2 p_pos)
	{
		GameObject particle = ZObjectMgr.Instance.Spawn2D (m_prefabParticlehit.name, p_pos);
		particle.GetComponent<ParticleSystem> ().Stop ();
		particle.GetComponent<ParticleSystem> ().Play ();
	}

	public void SpawnTarget()
	{
		int gameScore = GameScene.instance.GetScore();
		List<Vector3> currentObstacle;
		LeanTweenType tweenType = LeanTweenType.easeInOutSine;;

		if (gameScore < 8) 
		{
			currentObstacle = m_listOfEasyObstacles;
		} 
		else if (gameScore < 15) 
		{
			currentObstacle = m_listOfObstacles;
		} 
		else 
		{
			float randTween = Random.Range (0, 100);
			if (randTween > 50) 
			{
				tweenType = LeanTweenType.easeInOutSine;
			}
			else
			{
				tweenType = LeanTweenType.easeInOutExpo;
			}
			currentObstacle = m_listOfHardObstacles;
		}

		float speed = currentObstacle [m_currentTuningIdx].x;
		float dist = currentObstacle [m_currentTuningIdx].y;

		m_currentY += dist;
		float xPos = 0;

		float randVal = Random.Range (0, 100);

		GameObject target = ZObjectMgr.Instance.Spawn2D (m_prefabTarget.name, Vector3.zero);
		LeanTween.cancel (target.gameObject);


		if (randVal < 50) {
			target.transform.position = new Vector2 (40, m_currentY);
			LeanTween.moveLocalX (target.gameObject, -40, speed).setLoopPingPong().setEase(tweenType);
		} else {
			target.transform.position = new Vector2 (-40, m_currentY);
			LeanTween.moveLocalX (target.gameObject, 40, speed).setLoopPingPong ().setEase(tweenType);
		}

		m_currentTuningIdx++;

		if (m_currentTuningIdx > m_listOfObstacles.Count - 1) {
			m_currentTuningIdx = 0;
		}
	}

	private void UpdateCameraMovement()
	{
		LeanTween.cancel (Camera.main.gameObject);
		LeanTween.cancel (m_objBelowObstacle.gameObject);
		LeanTween.cancel (m_objTopObstacle.gameObject);

		LeanTween.moveLocalY (Camera.main.gameObject, m_objCharacter.transform.localPosition.y + 90.0f, 0.3f);
		LeanTween.moveLocalY (m_objTopObstacle.gameObject, m_objCharacter.transform.localPosition.y + 165.0f, 0.3f);
		LeanTween.moveLocalY (m_objBelowObstacle.gameObject, m_objCharacter.transform.localPosition.y - 80.0f, 0.3f);
	}

	private void Controls()
	{
		if (Input.GetMouseButtonDown (0) && m_curState == GameInstanceStates.INGAME) 
		{
			m_curState = GameInstanceStates.GAMEOVER;
			m_objCharacter.Shoot ();
		}
	}
}
