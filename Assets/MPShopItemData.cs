﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemRarity
{
	COMMON,
	EPIC,
	RARE
}
	
public class MPShopItemData : MonoBehaviour 
{
	public int ItemId;
	public ItemRarity Rarity;
	public GameObject ItemObject;
	public string ItemName;
	public string ItemDescription;
	public List<Color> ColorTheme;

	public void UnlockItem()
	{
		PlayerPrefs.SetInt ("item" + ItemId, 1);
	}

	public bool IsItemUnlocked()
	{
		return (PlayerPrefs.GetInt ("item" + ItemId) > 0) ? true : false;
	}
}
