﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FDGamesceneInstance : MonoBehaviour {

	public MPCharacter m_objCharacter;
	public string m_gameTitle;
	public string m_gameTutorial;
	public Color m_colorGameTitle;
	public Sprite m_titleArtwork;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public virtual void Gamescene_Initialize()
	{
	}

	// Update is called once per frame
	public virtual void Gamescene_Update () 
	{

	}

	public void Gamescene_SetCharacter(int p_idx)
	{
		m_objCharacter.SetCharacter (p_idx);
//		for (int idx =0; idx < m_scriptCharacter.transform.childCount; idx++) 
//		{
//			m_scriptCharacter.transform.GetChild (idx).gameObject.SetActive (false);
//		}
//		m_scriptCharacter.transform.GetChild(p_idx).gameObject.SetActive(true);
	}

	public virtual void Gamescene_ButtonDown () {

	}

	public virtual void Gamescene_ButtonUp () {

	}

	public virtual void Gamescene_ButtonHold()
	{

	}

	public virtual void Gamescene_Die () {

	}

	public virtual void Gamescene_Score () {

	}

	public virtual void Gamescene_Setuplevel(bool hardreset=false) 
	{
	}

	public virtual void Gamescene_Unloadlevel() {
	}
}
