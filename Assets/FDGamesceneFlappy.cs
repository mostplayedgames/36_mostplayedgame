﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FDGamesceneFlappy : FDGamesceneInstance {
//
//	public GameObject m_prefabPoles;
//	public GameObject m_prefabPolesHard;
//	public GameObject m_prefabStaggeredPoles;
//	public GameObject m_prefabTwoHoleStaggeredPoles;
//	public GameObject m_prefabPoleBase;
//
//	public GameObject m_objectCoin;
//	public GameObject m_objectMessage;
//
//	public GameObject m_spotLight;
//
//	public GameObject m_prefabBlock;
//
//	public Image m_imageFullBar;
//	public Color m_colorFullBarYellow;
//	public Color m_colorFullBarGain;
//	public Color m_colorFullBarFull;
//
//	// Spawn Tuning
//	// Considering going for a vector 3 in tuning, we maybe able to set more stuff when we use vector
//	// 		x is the type, y is the potential
//	// Also, the tuning will always start from 0, rubberbanding will start if player gets x number of
//	// 		score overall
//	// 0 - Full Random
//	// 1 - Force Easy
//	// 2 - Force Medium
//	// 3 - Force Hard
//	// 4 - Force Hard 1 
//	// 5 - Force Hard 2
//	// 6 - Force Hard 3
//	//
//	public List<int> m_listSpawnTuning;
//	public List<int> m_listSpawnTuningHard;
//
//	public List<int> m_listSpawnTuningType;
//
//
//
//	int m_currentSpawnTuningCount;
//	int m_currentSpawnTuningTypeCount;
//	float m_currentPoleY;
//
//	public List<Parallex> m_listParallax;
//
//	public List<Color> m_listCameraColors;
//	public Light m_directionalLight;
//
//	CHAR_STATE m_eState;
//	MODE_STATE_LEVEL m_eModeState;
//	//int m_currentLevelLocation = 0;
//
//	// Use this for initialization
//	void Awake () {
//		ZObjectMgr.Instance.AddNewObject (m_prefabPoles.name, 10,this.gameObject);
//		ZObjectMgr.Instance.AddNewObject (m_prefabPolesHard.name, 10, this.gameObject);
//		ZObjectMgr.Instance.AddNewObject (m_prefabBlock.name, 15, this.gameObject);
//		//ZObjectMgr.Instance.AddNewObject (m_prefabStaggeredPoles.name, 40,this.gameObject);
//		//ZObjectMgr.Instance.AddNewObject (m_prefabTwoHoleStaggeredPoles.name, 40,this.gameObject);
//		//ZObjectMgr.Instance.AddNewObject (m_prefabPoleBase.name, 40,this.gameObject);
//
//		//m_currentLevelLocation = PlayerPrefs.GetInt ("FlappyLevel");
//		m_currentPoleY = Random.Range (POLE_MIN_HEIGHT, POLE_MAX_HEIGHT);
//		m_eModeState = MODE_STATE_LEVEL.DIE;
//		//m_eModeState = MODE_STATE_LEVEL.IDLE;
//		m_currentSpawnTuningCount = Random.Range(0, m_listSpawnTuning.Count);
//		m_currentSpawnTuningTypeCount = 0;//.Range(0, m_listSpawnTuningType.Count);
//	}
//
//	// Update is called once per frame
//	public override void Gamescene_Update () {
//		if (m_eModeState == MODE_STATE_LEVEL.DIE)
//			return;
//		if (m_eModeState == MODE_STATE_LEVEL.IDLE)
//			return;
//
//		//30f
//		//52f
//		//m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (63f, m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity.y); //60f
//		switch (GameScene.instance.m_birdType) {
//		case 0: 
//			m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (63f, m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity.y); //60f
//			break;
//		case 2: 
//			m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (63f, m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity.y); //60f
//			break;
//		case 3: 
//			//m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (68f, m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity.y); //60f
//			//m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (78f, m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity.y); //60f
//			m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (95f, m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity.y); //60f
//			break;
//		case 4: 
//			//m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (63f, m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().velocity.y); //60f
//			break;
//		}
//
//		ZCameraMgr.instance.gameObject.transform.position = new Vector3(m_scriptCharacter.gameObject.transform.position.x - 20f, 46.78f, -254f);
//
//		// 15.7f 10.63 0
//	}
//
//	public override void Gamescene_ButtonDown () {
//		if (m_eModeState == MODE_STATE_LEVEL.DIE)
//			return;
//		if (m_eModeState == MODE_STATE_LEVEL.IDLE) {
//			m_eModeState = MODE_STATE_LEVEL.INGAME;
//			m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;
//		}
//
//		foreach(Parallex parallaxObject in m_listParallax){
//			parallaxObject.StartParallax ();
//		}
//		//m_scriptCharacter.Reset ();
//		//m_scriptCharacter.Charge ();
//
//		//if (m_eState == CHAR_STATE.CHARGE)
//		//	return;
//
//		//LeanTween.cancel (m_objectCharacter.gameObject);
//		//LeanTween.moveLocalY (m_objectCharacter.gameObject, 11.8f, 1.5f).setOnComplete(Jump);//.setEase(LeanTweenType.easeInQuad);
//		//m_currentChargeTime = Time.time;
//
//		//m_eState = CHAR_STATE.CHARGE;
//
//		LeanTween.cancel (ZCameraMgr.instance.gameObject);
////		if(m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().bodyType == RigidbodyType2D.Dynamic)
////			m_scriptCharacter.Flap ();
//	}
//
//	public override void Gamescene_ButtonUp () {
//
//	}
//
//	public override void Gamescene_Die () {
//		if (m_eModeState == MODE_STATE_LEVEL.DIE)
//			return;
//
//		//LeanTween.delayedCall (2f, Gamescene_Setuplevel);
//
//		LeanTween.delayedCall (0.2f, Coin_Fadeout);
//
//		m_objectMessage.transform.position = m_scriptCharacter.gameObject.transform.position + new Vector3(25f, 0f, 0);
//		LeanTween.cancel (m_objectCoin);
//		Color textColor = m_objectMessage.GetComponent<Renderer> ().material.color;
//		m_objectMessage.GetComponent<Renderer> ().material.color = new Color(textColor.r, textColor.g, textColor.b, 1);
//		LeanTween.move (m_objectMessage.gameObject, m_objectMessage.transform.position + new Vector3 (0, 7f, 0), 0.5f).setEase (LeanTweenType.easeOutQuad).setOnComplete(Coin_Fadeout);
//		m_objectMessage.GetComponent<TextMesh> ().text = "Ouch";
//
//		foreach(Parallex parallaxObject in m_listParallax){
//			parallaxObject.StopParallax ();
//		}
//		m_eModeState = MODE_STATE_LEVEL.DIE;
//	}
//
//	float currentX = 70f;
//	int currentXIndex = 0;
//
//	public GameObject m_objectSky;
//
//	void Coin_Fadeout()
//	{
//		LeanTween.alpha (m_objectCoin.gameObject, 0, 0.5f);
//	}
//
//	public override void Gamescene_Score () {
//		//if (GameScene.instance.GetScore () > 5) {
//			//GameObject spawnObject = ZObjectMgr.Instance.Spawn2D (m_prefabPoles.name, new Vector2 (currentX, Random.Range(41,129)));
//			//GameObject spawnObject;
//			SpawnPole ();
//			if( GameScene.instance.GetScore() % 2 == 0){
//				//SpawnBlock ();
//			}
//		//}
//
//
//
//
//		//LeanTween.color (m_objectSky, Color.blue, 0.2f).setLoopPingPong().setLoopCount(2);
//		//m_scriptCharacter.Rotate ();
//
//		int cameraColors = Mathf.FloorToInt(GameScene.instance.GetScore () / 5f);
//		if (cameraColors >= m_listCameraColors.Count - 1) {
//			cameraColors = m_listCameraColors.Count - 1;
//		}
//		if (cameraColors >= m_listCameraColors.Count - 2) {
//			m_spotLight.gameObject.SetActive(true);
//			m_directionalLight.gameObject.SetActive (false);
//		}
//		m_directionalLight.color = m_listCameraColors [cameraColors];
//
//
//
//
//
//		//m_spotLight.SetActive (true);
//		//m_directionalLight.intensity = 1f;
//
//		//LeanTween.delayedCall (0.1f, ScoreRoutine);
//	}
//
//	void ScoreRoutine()
//	{
//		if (GameScene.instance.GetCurrentCoins() >= GameScene.instance.GetCurrentShopPrice ()) {
//			m_imageFullBar.color = m_colorFullBarFull;
//		} else {
//			m_imageFullBar.color = m_colorFullBarYellow;
//		}
//		//m_directionalLight.intensity = 0.7f;
//	//	m_spotLight.SetActive (false);
//	}
//
//	// 0 Basic
//	// 1 Basic Moving Vertical
//	// 2 Basic Moving Horizontal
//	// 3 Ref
//
//	int m_poleColorRounder = 0;
//	void SpawnPole(bool isHard=false)
//	{
//		
//		
//		GameObject spawnObject;
//		isHard = false;
//		//Debug.Log ("Current Score 1 : " + currentXIndex);
//		/*if (currentXIndex >= 30 && Random.Range (0, 100) > 40) {
//			spawnObject = ZObjectMgr.Instance.Spawn2D (m_prefabPolesHard.name, new Vector2 (currentX, m_currentPoleY));
//			isHard = true;
//		} 
//		else if (currentXIndex >= 15 && Random.Range (0, 100) > 80) {
//			spawnObject = ZObjectMgr.Instance.Spawn2D (m_prefabPolesHard.name, new Vector2 (currentX, m_currentPoleY));
//			isHard = true;
//		} else {
//			spawnObject = ZObjectMgr.Instance.Spawn2D (m_prefabPoles.name, new Vector2 (currentX, m_currentPoleY));
//		}*/
//		/*if (currentXIndex >= 5 && Random.Range (0, 100) > 80) {
//			spawnObject = ZObjectMgr.Instance.Spawn2D (m_prefabPolesHard.name, new Vector2 (currentX, m_currentPoleY));
//			spawnObject.GetComponent<PoleObject> ().ForceClose ();
//			isHard = true;
//		}
//		else {*/
//			spawnObject = ZObjectMgr.Instance.Spawn2D (m_prefabPoles.name, new Vector2 (currentX, m_currentPoleY));
//		//}
//
//		LeanTween.cancel (spawnObject);
//		spawnObject.GetComponent<PoleObject> ().Reset ();
//
//
//		spawnObject.GetComponent<PoleObject> ().SetPipeColor (Mathf.FloorToInt(m_poleColorRounder / 5f));
//
//
//		m_poleColorRounder++;
//
//		//if (currentXIndex > 3 && Random.Range (0, 100) > 80) {
//		//	spawnObject.GetComponent<PoleObject> ().ForceClose ();
//		//}
//
//
//		// Potential Levelling ( Levels change every 5 )
//		// Level 1 - Up and Down
//		// Level 2 - Left and Right
//		// Level 3 - Up Down Left Right
//		// Level 4 - Tighter spaces ( this should be fixed, yung mga movement can be modified )
//
//		// TUNING : STEP 2 : DEFINE MOVEMENT
//		// We define from here what level the player is and define movement from here
//		// 4 up and down movement
//		// 9 left and right movement
//		// 14 left right up and down movement
//		// after reaching a certain score, the movement should rubberband, not by too much but only a bit pwede na
//
//
//
//
//
//		//float addToCurrentX = Random.Range (55f, 80f);
//		//float addToCurrentX = Random.Range (30f, 80f);
//		int direction = 0;
//		float addToCurrentX = Random.Range (30f, 90f);
//		float difficultyToAdd = 60f;
//
//		//float addToCurrentXMultipler = 1.1f;
//
//		//Debug.Log ("Current Score : " + currentXIndex);
//		//if (currentXIndex >= 15)
//		//	addToCurrentXMultipler = 1.05f;
//
//		int spawnTuning = m_listSpawnTuning [m_currentSpawnTuningCount% m_listSpawnTuning.Count] ;
//
//		if (currentXIndex > 15) 
//		{
//			spawnTuning = m_listSpawnTuningHard [m_currentSpawnTuningCount % m_listSpawnTuningHard.Count];
//		}
//
//		int randomTuning = Random.Range (0, 100);
//		if (spawnTuning == 0) 
//		{
//			if (randomTuning > 70) 
//			{
//				spawnTuning = 3;
//			} 
//			else if (randomTuning > 40) 
//			{
//				spawnTuning = 2;
//			} 
//			else 
//			{
//				spawnTuning = 1;
//			}
//		}
//
//		if (spawnTuning == 1) 
//		{
//			spawnTuning = 2;
//		}
//
//		//if( !isHard )
//		//	spawnObject.GetComponent<PoleObject> ().Spawn (spawnTuning, currentXIndex);
//		spawnObject.GetComponent<PoleObject> ().Spawn (spawnTuning, currentXIndex);
//
//		// 1 - SET DIRECTION
//		if (direction == 0) {
//			if (Random.Range (0, 100) > 50)
//				direction = -1;
//			else
//				direction = 1;
//		}
//
//		if (m_currentPoleY == POLE_MAX_HEIGHT) 
//		{
//			direction = -1;
//		}
//		else if (m_currentPoleY == POLE_MIN_HEIGHT) 
//		{
//			direction = 1;
//		} 
//
//		// 2 - X POS
//		int randomAddCurrentX = Random.Range(0,100);
//		if (randomAddCurrentX > 80) 
//		{
//			addToCurrentX = Random.Range (80f, 100f);
//		} 
//		else if (randomAddCurrentX > 30) 
//		{
//			addToCurrentX = Random.Range (60, 80f);
//		} 
//		else 
//		{
//			addToCurrentX = Random.Range (27f, 60f);
//		}
//
//		addToCurrentX = 80f;
//
//		// 3 - DEFINE Y DEPENDING ON DIFFICULTY
//		//spawnTuning = 3;
//		if (direction == 1) 
//		{
//			switch (spawnTuning) {
//			case 1: // EASY
//				difficultyToAdd = Random.Range (addToCurrentX * 0.3f, addToCurrentX * 0.5f);
//				break;
//			case 2: // MEDIUM
//				if (addToCurrentX <= 50f) 
//				{
//					addToCurrentX = 50f;
//				}
//				difficultyToAdd = Random.Range (addToCurrentX * 0.5f, addToCurrentX * 0.9f);
//				break;
//			case 3: // HARD
//				if (addToCurrentX <= 70f) 
//				{
//					addToCurrentX = 70f;
//				}
//				if( currentXIndex > 30 )
//					difficultyToAdd = addToCurrentX * 1.2f;
//				else if( currentXIndex > 20 )
//					difficultyToAdd = addToCurrentX * 1.1f;
//				else
//					difficultyToAdd = addToCurrentX * 1f;//Random.Range (addToCurrentX * 1.1f, addToCurrentX * 1.4f);
//
//				break;
//			}
//		} 
//		else 
//		{
//			switch (spawnTuning) 
//			{
//			case 1: // EASY
//				difficultyToAdd = Random.Range (addToCurrentX * 0.2f, addToCurrentX * 0.3f);
//				break;
//			case 2: // MEDIUM
//				if (addToCurrentX <= 50f) {
//					addToCurrentX = 50f;
//				}
//				difficultyToAdd = Random.Range (addToCurrentX * 0.5f, addToCurrentX * 0.7f);
//				break;
//			case 3: // HARD
//				if (addToCurrentX <= 80f) {
//					addToCurrentX = 80f;
//				}
//
//				if( currentXIndex > 30 )
//					difficultyToAdd = addToCurrentX * 1.6f;
//				else if( currentXIndex > 20 )
//					difficultyToAdd = addToCurrentX * 1.5f;
//				else
//					difficultyToAdd = addToCurrentX * 1.4f;//Random.Range (addToCurrentX * 0.9f, addToCurrentX * 1.2f);
//				break;
//			}
//		}
//
//		//Debug.Log ("Current X Index : " + currentXIndex + " Add X : " + addToCurrentX + " Difficulty TO Add :" + difficultyToAdd);
//
//		// CAP Y VALUES
//		/*if (direction == 0) {
//			if (Random.Range (0, 100) > 50)
//				direction = -1;
//			else
//				direction = 1;
//		}*/
//
//		/*if (m_currentPoleY == POLE_MAX_HEIGHT) {
//			direction = -1;
//		}else if (m_currentPoleY == POLE_MIN_HEIGHT) {
//			direction = 1;
//		} 
//
//		// TUNING : Random tuning random in 3 difficulties
//		
//		*/
//		/*
//		// SET WHAT HEIGHT WE WILL ADD TO THE POLES
//		// FROM HERE LET US SET THE DISTANCE DEPENDING ON THE DIFFICULTY
//		int randomPoleHeightType = Random.Range (0, 100);
//		// TUNING : STEP 1 : DEFINE EMH
//		float difficultyToAdd = 60f;
//
//		// UP DIRECTION
//		float BIG_MIN_ADD_X = 90f;
//		float MED_MIN_ADD_X = 50f;
//		float SML_MIN_ADD_X = 27f;
//
//		float BIG_MIN_ADD_Y = 110f;
//		float MED_MIN_ADD_Y = 50f;
//		float SML_MIN_ADD_Y = 12f;
//
//		// DOWN DIRECTION
//		float BIG_MIN_MNS_X = 60f;
//		float MED_MIN_MNS_X = 80f;
//		float SML_MIN_MNS_X = 27f;
//
//		float BIG_MIN_MNS_Y = 120f;
//		float MED_MIN_MNS_Y = 70f;
//		float SML_MIN_MNS_Y = 12f;
//
//		// Randomize what height difficulty it is
//
//
//
//
//		switch(GameScene.instance.m_birdType){
//		case 3:
//			switch (spawnTuning) {
//			case 1: // Easy
//				if (direction == 1) {
//					if (randomPoleHeightType > 66) { // Big Add
//						addToCurrentX = Random.Range(BIG_MIN_ADD_X, BIG_MIN_ADD_X + 20f);//Random.Range (90f, 100);
//						difficultyToAdd = BIG_MIN_ADD_Y;//Random.Range (120f, 132f);
//					} else if (randomPoleHeightType > 33) { // Medium Add
//						addToCurrentX = Random.Range(MED_MIN_ADD_X, BIG_MIN_ADD_X);//Random.Range (50f, 100f);
//						difficultyToAdd = MED_MIN_ADD_Y;//Random.Range (70f, 100f);
//					} else { // Small Add
//						addToCurrentX = Random.Range(SML_MIN_ADD_X,BIG_MIN_ADD_X);//Random.Range (27f, 75f);
//						difficultyToAdd = SML_MIN_ADD_Y;//Random.Range (12f, 70f);
//					}
//				} else {
//					if (randomPoleHeightType > 66) { // Big Add
//						addToCurrentX = Random.Range(BIG_MIN_MNS_X, BIG_MIN_ADD_X + 20f);//Random.Range (52f, 110f);
//						difficultyToAdd = BIG_MIN_MNS_Y;//Random.Range (120f, 132f);
//					} else if (randomPoleHeightType > 33) { // Medium Add
//						addToCurrentX = Random.Range(MED_MIN_MNS_X,BIG_MIN_ADD_X);//Random.Range (50f, 100f);
//						difficultyToAdd = MED_MIN_MNS_Y;//Random.Range (70f, 100f);
//					} else { // Small Add
//						addToCurrentX = Random.Range(SML_MIN_MNS_X, BIG_MIN_ADD_X);//Random.Range (27f, 75f);
//						difficultyToAdd = SML_MIN_MNS_Y;//Random.Range (12f, 70f);
//					}
//				}
//				break;
//			case 2: // Medium
//				if (direction == 1) {
//					if (randomPoleHeightType > 66) { // Big Add
//						addToCurrentX = Random.Range(BIG_MIN_ADD_X, BIG_MIN_ADD_X + 20f);//Random.Range (90f, 100);
//						difficultyToAdd = BIG_MIN_ADD_Y;//Random.Range (120f, 132f);
//					} else if (randomPoleHeightType > 33) { // Medium Add
//						addToCurrentX = Random.Range(MED_MIN_ADD_X, BIG_MIN_ADD_X);//Random.Range (50f, 100f);
//						difficultyToAdd = MED_MIN_ADD_Y;//Random.Range (70f, 100f);
//					} else { // Small Add
//						addToCurrentX = Random.Range(SML_MIN_ADD_X,MED_MIN_ADD_X + 20f);//Random.Range (27f, 75f);
//						difficultyToAdd = SML_MIN_ADD_Y;//Random.Range (12f, 70f);
//					}
//				} else {
//					if (randomPoleHeightType > 66) { // Big Add
//						addToCurrentX = Random.Range(BIG_MIN_MNS_X, BIG_MIN_MNS_X + 20f);//Random.Range (52f, 110f);
//						difficultyToAdd = BIG_MIN_MNS_Y;//Random.Range (120f, 132f);
//					} else if (randomPoleHeightType > 33) { // Medium Add
//						addToCurrentX = Random.Range(MED_MIN_MNS_X,BIG_MIN_MNS_X);//Random.Range (50f, 100f);
//						difficultyToAdd = MED_MIN_MNS_Y;//Random.Range (70f, 100f);
//					} else { // Small Add
//						addToCurrentX = Random.Range(SML_MIN_MNS_X, MED_MIN_MNS_X + 20f);//Random.Range (27f, 75f);
//						difficultyToAdd = SML_MIN_MNS_Y;//Random.Range (12f, 70f);
//					}
//				}
//				break;
//			case 3: // Hard
//				if (direction == 1) {
//					if (randomPoleHeightType > 66) { // Big Add
//						addToCurrentX = Random.Range(BIG_MIN_ADD_X, BIG_MIN_ADD_X + 5f);//Random.Range (90f, 100);
//						difficultyToAdd = BIG_MIN_ADD_Y;//Random.Range (120f, 132f);
//					} else if (randomPoleHeightType > 33) { // Medium Add
//						addToCurrentX = Random.Range(MED_MIN_ADD_X, BIG_MIN_ADD_X);//Random.Range (50f, 100f);
//						difficultyToAdd = MED_MIN_ADD_Y;//Random.Range (70f, 100f);
//					} else { // Small Add
//						addToCurrentX = Random.Range(SML_MIN_ADD_X,MED_MIN_ADD_X);//Random.Range (27f, 75f);
//						difficultyToAdd = SML_MIN_ADD_Y;//Random.Range (12f, 70f);
//					}
//				} else {
//					if (randomPoleHeightType > 66) { // Big Add
//						addToCurrentX = Random.Range(BIG_MIN_MNS_X, BIG_MIN_MNS_X + 5f);//Random.Range (52f, 110f);
//						difficultyToAdd = BIG_MIN_MNS_Y;//Random.Range (120f, 132f);
//					} else if (randomPoleHeightType > 33) { // Medium Add
//						addToCurrentX = Random.Range(MED_MIN_MNS_X,BIG_MIN_MNS_X);//Random.Range (50f, 100f);
//						difficultyToAdd = MED_MIN_MNS_Y;//Random.Range (70f, 100f);
//					} else { // Small Add
//						addToCurrentX = Random.Range(SML_MIN_MNS_X, MED_MIN_MNS_X);//Random.Range (27f, 75f);
//						difficultyToAdd = SML_MIN_MNS_Y;//Random.Range (12f, 70f);
//					}
//				}
//				break;
//			}
//
//
//			break;
//		default:
//			switch (spawnTuning) {
//			case 1: // Easy
//				addToCurrentX = Random.Range (27f, 75f);
//				difficultyToAdd = Random.Range (12f, 70f);
//				break;
//			case 2: // Medium
//				addToCurrentX = Random.Range (50f, 75f);
//				difficultyToAdd = Random.Range (70f, 100f);
//				break;
//			case 3: // Hard
//				addToCurrentX = Random.Range (62f, 70f);
//				difficultyToAdd = Random.Range (120f, 132f);
//				break;
//			}
//			break;
//		}
//		*/
//		/*if (direction == 1) {
//			// TUNING : STEP 5 : CAP VALUES
//			// CAP X VALUES
//			if (addToCurrentX <= 37f) {
//				if (Random.Range (0, 100) > 75) {
//					difficultyToAdd = 0;
//				}
//				if (difficultyToAdd >= 10f)
//					difficultyToAdd = 10f;
//			} else if (addToCurrentX <= 47f) {
//				if (difficultyToAdd >= 27f)
//					difficultyToAdd = 27f;
//			} else if (addToCurrentX < 57f) {
//				if (difficultyToAdd >= 40f)
//					difficultyToAdd = 40f;
//			} else if (addToCurrentX < 67f) {
//				if (difficultyToAdd >= 75f)
//					difficultyToAdd = 75f;
//			}
//		}*/
//
//
//		
//
//
//		if (direction == 1) {
//			m_currentPoleY += difficultyToAdd;//60f;//Random.Range(20, 6
//		} else {
//			m_currentPoleY -= difficultyToAdd;//60f;
//		}
//
//		if (m_currentPoleY >= POLE_MAX_HEIGHT) {
//			m_currentPoleY = POLE_MAX_HEIGHT;
//		}
//		if (m_currentPoleY <= POLE_MIN_HEIGHT) {
//			m_currentPoleY = POLE_MIN_HEIGHT;
//		}
//
//		if (currentXIndex <= 0) {
//			m_currentPoleY = 90f;
//		}
//
//		currentX += addToCurrentX;// * addToCurrentXMultipler;//75f;//60f;//55f;// Random.Range (45f, 60f);
//		currentXIndex++;
//
//
//		spawnObject.transform.localPosition = new Vector2 (currentX, m_currentPoleY);
//
//
//
//
//		// SET BEHAVIORS
//		switch (m_listSpawnTuningType[m_currentSpawnTuningTypeCount % m_listSpawnTuningType.Count]) {
//		case 0:
//			spawnObject.GetComponent<PoleObject>().Behavior_GapClosing();
//			break;
//		case 1:
//			spawnObject.GetComponent<PoleObject>().Behavior_GapMoving();
//			break;
//		case 2:
//			spawnObject.GetComponent<PoleObject>().Behavior_GapFixed();
//			break;
//		}
//		
//
//		if (spawnObject.transform.position.x > m_previousXblock) {
//			SpawnBlock ();
//		}
//
///*
//		if (addToCurrentX >= 60 && !isHard) {
//			if (currentXIndex == 14 || (currentXIndex > 14 && Random.Range (0, 100) > 80)) {
//				if (Random.Range (0, 100) > 50) {
//					spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x + 5f, spawnObject.transform.position.y, spawnObject.transform.position.z);
//			LeanTween.moveX (spawnObject, spawnObject.transform.position.x + Random.Range(5f,8f), Random.Range (0.8f, 1.8f)).setLoopPingPong ().setEase (LeanTweenType.easeInOutQuad);
//					currentX += 20f;
//				} else {
//					spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x, spawnObject.transform.position.y - 5f, spawnObject.transform.position.z);
//					LeanTween.moveY (spawnObject, spawnObject.transform.position.y + 10f, Random.Range (0.8f, 1.8f)).setLoopPingPong ().setEase (LeanTweenType.easeInOutQuad);
//				}
//			} else if (currentXIndex == 9 || (currentXIndex > 9 && Random.Range (0, 100) > 80)) {
//				//if (Random.Range (0, 100) > 80) {
//				spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x + 5f, spawnObject.transform.position.y, spawnObject.transform.position.z);
//		LeanTween.moveX (spawnObject, spawnObject.transform.position.x + Random.Range(10f,14f), Random.Range (1f, 2f)).setLoopPingPong ().setEase (LeanTweenType.easeInOutQuad);
//				currentX += 20f;
//				//} else {
//				//	LeanTween.moveY (spawnObject, spawnObject.transform.position.y + 10f, Random.Range (1.3f, 1.8f)).setLoopPingPong ().setEase (LeanTweenType.easeInOutQuad);
//				//}
//			} else if (currentXIndex == 4 || (currentXIndex > 4 && Random.Range (0, 100) > 80)) {
//				spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x, spawnObject.transform.position.y - 5f, spawnObject.transform.position.z);
//		LeanTween.moveY (spawnObject, spawnObject.transform.position.y + Random.Range(10f,15f), Random.Range (1f, 2f)).setLoopPingPong ().setEase (LeanTweenType.easeInOutQuad);
//			}
//		}
//		*/
//
//		/*if (Random.Range (0, 100) > 50)
//			LeanTween.moveY (spawnObject, POLE_MAX_HEIGHT, Random.Range (5f, 5f)).setLoopPingPong ();
//		else
//			LeanTween.moveY (spawnObject, POLE_MIN_HEIGHT, Random.Range (5f, 5f)).setLoopPingPong ();
//*/
//		
//
///*
//		spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x, 
//													spawnObject.transform.position.y - 5f, 
//													spawnObject.transform.position.z);
//		LeanTween.moveY (spawnObject, spawnObject.transform.position.y + Random.Range(10f,15f), 
//													Random.Range (1f, 2f)).setLoopPingPong ().setEase (LeanTweenType.easeInOutQuad);
//*/
///*
//
//if (addToCurrentX >= 60 && !isHard) {
//	if (currentXIndex == 14 || (currentXIndex > 14 && Random.Range (0, 100) > 80)) {
//		if (Random.Range (0, 100) > 50) {
//			spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x + 5f, spawnObject.transform.position.y, spawnObject.transform.position.z);
//			LeanTween.moveX (spawnObject, spawnObject.transform.position.x + Random.Range(5f,8f), Random.Range (0.8f, 1.8f)).setLoopPingPong ().setEase (LeanTweenType.easeInOutQuad);
//			currentX += 20f;
//		} else {
//			spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x, spawnObject.transform.position.y - 5f, spawnObject.transform.position.z);
//			LeanTween.moveY (spawnObject, spawnObject.transform.position.y + 10f, Random.Range (0.8f, 1.8f)).setLoopPingPong ().setEase (LeanTweenType.easeInOutQuad);
//		}
//	} else if (currentXIndex == 9 || (currentXIndex > 9 && Random.Range (0, 100) > 80)) {
//		//if (Random.Range (0, 100) > 80) {
//		spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x + 5f, spawnObject.transform.position.y, spawnObject.transform.position.z);
//		LeanTween.moveX (spawnObject, spawnObject.transform.position.x + Random.Range(10f,14f), Random.Range (1f, 2f)).setLoopPingPong ().setEase (LeanTweenType.easeInOutQuad);
//		currentX += 20f;
//		//} else {
//		//	LeanTween.moveY (spawnObject, spawnObject.transform.position.y + 10f, Random.Range (1.3f, 1.8f)).setLoopPingPong ().setEase (LeanTweenType.easeInOutQuad);
//		//}
//	} else if (currentXIndex == 4 || (currentXIndex > 4 && Random.Range (0, 100) > 80)) {
//		spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x, spawnObject.transform.position.y - 5f, spawnObject.transform.position.z);
//		LeanTween.moveY (spawnObject, spawnObject.transform.position.y + Random.Range(10f,15f), Random.Range (1f, 2f)).setLoopPingPong ().setEase (LeanTweenType.easeInOutQuad);
//	}
//}*/
//
//		/*
//
//
//		// EMH
//		// Range this up when it gets harder
//		int randomEMH = Random.Range(0,100);
//		float difficultyToAdd = 60f;
//
//		if (GameScene.instance.GetScore () > 15) {
//			if (randomEMH > 90) {
//				difficultyToAdd = 130f;
//			} else if (randomEMH > 75) {
//				difficultyToAdd = 120f;
//			} else if (randomEMH > 60) {
//				difficultyToAdd = 100f;
//			} else if (randomEMH > 35) {
//				difficultyToAdd = 75f;
//			} else {
//				difficultyToAdd = 60f;
//			}
//
//		} else {
//			
//			if (randomEMH > 95) {
//				difficultyToAdd = 130f;
//			} else if (randomEMH > 85) {
//				difficultyToAdd = 120f;
//			} else if (randomEMH > 70) {
//				difficultyToAdd = 100f;
//			} else if (randomEMH > 45) {
//				difficultyToAdd = 75f;
//			} else {
//				difficultyToAdd = 60f;
//			}
//		}
//
//		if( addToCurrentX < 40f ){
//			if (Random.Range (0, 100) > 75) {
//				difficultyToAdd = 0;
//			}
//			if (difficultyToAdd >= 5f)
//				difficultyToAdd = 5f;
//		}
//		else if( addToCurrentX < 50f ){
//			if (difficultyToAdd >= 5f)
//				difficultyToAdd = 5f;
//		}
//		else if( addToCurrentX < 60f ){
//			if (difficultyToAdd >= 35f)
//				difficultyToAdd = 35f;
//		}else if( addToCurrentX < 75f) {
//			if (difficultyToAdd >= 75f)
//				difficultyToAdd = 75f;
//		}
//
//		if (m_currentPoleY == POLE_MAX_HEIGHT) {
//			m_currentPoleY -= difficultyToAdd;
//		}else if (m_currentPoleY == POLE_MIN_HEIGHT) {
//			m_currentPoleY += difficultyToAdd;
//		}else if (Random.Range (0, 100) > 50) {
//			m_currentPoleY += difficultyToAdd;//60f;//Random.Range(20, 6
//		} else {
//			m_currentPoleY -= difficultyToAdd;//60f;
//		}
//
//		if (m_currentPoleY >= POLE_MAX_HEIGHT) {
//			m_currentPoleY = POLE_MAX_HEIGHT;
//		}
//		if (m_currentPoleY <= POLE_MIN_HEIGHT) {
//			m_currentPoleY = POLE_MIN_HEIGHT;
//		}
//		*/
//
//
//		m_currentSpawnTuningTypeCount++;
//		if (m_currentSpawnTuningTypeCount >= m_listSpawnTuningType.Count - 1) {
//			m_currentSpawnTuningTypeCount = 0;
//		}
//
//		m_currentSpawnTuningCount++;
//		if (m_currentSpawnTuningCount >= m_listSpawnTuning.Count - 1) {
//			m_currentSpawnTuningCount = 0;
//		}
//
//		//PlayerPrefs.SetInt ("FlappyLevel",m_currentPoleY );
//	}
//
//	float POLE_MIN_HEIGHT = 27f;
//	//float POLE_MAX_HEIGHT = 160f;
//float POLE_MAX_HEIGHT = 160f;
//
//	void SpawnLevel(){
//		//return;
//		currentX = 100f;
//		currentXIndex = 0;
//		for (int x = 0; x < 5; x++) {
//			//ZObjectMgr.Instance.Spawn2D (m_prefabPoles.name, new Vector2 (70f + (x * Random.Range(60f,60f)), Random.Range(41,129)));
//			GameObject spawnObject = null;
//
//			//Hack first
//			//if (m_listSpawnTuning [m_currentSpawnTuningCount] == 2)
//			//	m_listSpawnTuning [m_currentSpawnTuningCount] = 1;
//			//m_currentPoleY
//			//if (currentXIndex == 0) {
//				//m_currentPoleY = 90;
//			//}
//			
//			/*if (false && m_listSpawnTuning [m_currentSpawnTuningCount] == 1) {
//				spawnObject = ZObjectMgr.Instance.Spawn2D (m_prefabStaggeredPoles.name, new Vector2 (currentX, m_currentPoleY));
//			} else if (false && m_listSpawnTuning [m_currentSpawnTuningCount] == 2) {
//				//spawnObject = ZObjectMgr.Instance.Spawn2D (m_prefabTwoHoleStaggeredPoles.name, new Vector2 (currentX, m_currentPoleY));
//				spawnObject = ZObjectMgr.Instance.Spawn2D (m_prefabTwoHoleStaggeredPoles.name, new Vector2 (currentX, m_currentPoleY));
//			}else{
//				spawnObject = ZObjectMgr.Instance.Spawn2D (m_prefabPoles.name, new Vector2 (currentX, m_currentPoleY));
//			}*/
//
//			// TUNING : STEP 3 : DEFINE TYPE OF OBSTACLE
//			// Get EMH Fix ish controlled value from our list then spawn the necesary pole from it
//			// This should rubber band tho, it needs to move into different stuff after
//			// Level 2
//
//				
//			SpawnPole (false);
//			//ZObjectMgr.Instance.Spawn2D (m_prefabPoleBase.name, new Vector2 (currentX, m_currentPoleY)).transform.position = 
//				//new Vector3(spawnObject.transform.position.x + 1, -55.3f, spawnObject.transform.position.z);
//
//		}
//	}
//
//	float m_previousXblock;
//	void SpawnBlock()
//	{
//		GameObject spawnObject;
//		spawnObject = ZObjectMgr.Instance.Spawn2D (m_prefabBlock.name, new Vector2 (m_currentBlockX, -27f));
//		spawnObject.transform.localPosition = new Vector3 (m_currentBlockX, -36.4f, 111.6f);
//		spawnObject.GetComponent<Block> ().Reset ();
//		m_previousXblock = spawnObject.transform.position.x;
//		m_currentBlockX += 80f;
//	}
//
//	float m_currentBlockX;
//	void SpawnInitialBlocks()
//	{
//		m_currentBlockX = -37f;
//
//		for (int x = 0; x < 15; x++) {
//			SpawnBlock ();
//		}
//	}
//
//	public override void Gamescene_Setuplevel(bool hardreset) {
//		//Debug.LogWarning ("Setup Level 12");
//		if (!hardreset && m_eModeState == MODE_STATE_LEVEL.IDLE)
//			return;
//
//		m_spotLight.gameObject.SetActive(false);
//		m_directionalLight.gameObject.SetActive (true);
//		m_objectMessage.transform.localPosition = new Vector3 (-9999, 0, 0);
//		//
//		//m_currentPoleY = Random.Range (POLE_MIN_HEIGHT, POLE_MAX_HEIGHT);
//		//m_currentPoleY = Random.Range (POLE_MIN_HEIGHT + 60f, POLE_MAX_HEIGHT-60f);
//		 
//		m_directionalLight.color = m_listCameraColors [0];
//
//		if (GameScene.instance.GetCurrentCoins() >= GameScene.instance.GetCurrentShopPrice ()) {
//			m_imageFullBar.color = m_colorFullBarFull;
//		} else {
//			m_imageFullBar.color = m_colorFullBarYellow;
//		}
//
//		//m_scriptCharacter.Reset ();
//		m_scriptCharacter.gameObject.transform.position = GameScene.instance.m_objectCharacterRoot.gameObject.transform.position;
//		m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
//		m_scriptCharacter.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
//		switch(GameScene.instance.m_birdType){
//		case 2: 
//			//m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().gravityScale = 95f;
//			m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().gravityScale = 90f;
//		break;
//		case 3: 
//			m_scriptCharacter.gameObject.GetComponent<Rigidbody2D> ().gravityScale = 95f;
//			break;
//		}
//		LeanTween.cancel (m_scriptCharacter.gameObject);
//		LeanTween.moveLocalY (m_scriptCharacter.gameObject, m_scriptCharacter.gameObject.transform.localPosition.y + 8, 1f).setLoopPingPong ().setEase (LeanTweenType.easeInOutCubic);
//
//		m_currentPoleY = Random.Range (POLE_MIN_HEIGHT + 40f, POLE_MAX_HEIGHT-40f);
//
//		GameScene.instance.ResetCamera ();
//		this.gameObject.SetActive (true);
//
//		ZObjectMgr.Instance.ResetAll ();
//	m_poleColorRounder = Mathf.FloorToInt (Random.Range (0, 10) * 5f);
//
//		SpawnLevel ();
//		SpawnInitialBlocks ();
//
//		this.transform.localEulerAngles = Vector3.zero;
//		m_eState = CHAR_STATE.IDLE;
//
//		m_eModeState = MODE_STATE_LEVEL.IDLE;
//
//		//Debug.Log ("Setup Level");
//		GameScene.instance.m_eState = GAME_STATE.IDLE;
//
//		foreach(Parallex parallaxObject in m_listParallax){
//			parallaxObject.Reset ();
//		}
//
//		
//	}
//
//	public override void Gamescene_Unloadlevel() {
//		this.gameObject.SetActive (false);
//		Debug.Log ("Unload Level");
//	}
}
