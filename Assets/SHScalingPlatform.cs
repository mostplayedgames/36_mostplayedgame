﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SHScalingPlatform : MonoBehaviour {

	public SHPlatform m_platformLanded;
	public GameObject m_endPoint;
	public GameObject m_scaledObj;
	public GameObject m_objCollided;
	public RaycastHit2D m_rayHit;
	public bool m_startChecking;
	public string m_collidedWith;
	public int m_score;

	public void Initialize()
	{
		m_startChecking = true;
		m_score = 0;
		m_objCollided = null;
	}

	public int GetPlatformStatus()
	{
		return m_score;
	}
		

	public void StartChecking()
	{
		Vector3 rayStartPoint = m_endPoint.transform.position + new Vector3 (0, 10, 0);

		Debug.DrawRay (rayStartPoint, -m_endPoint.transform.up * 15f);

		RaycastHit2D hit = Physics2D.Raycast (rayStartPoint, -m_endPoint.transform.up, 15.0f);

		if (hit.collider != null) 
		{
			if (hit.collider.gameObject.tag == "Platform") 
			{
				m_objCollided = hit.collider.transform.gameObject;
				m_platformLanded = m_objCollided.GetComponent<SHPlatform> ();

				if (m_platformLanded.IsInCenter (hit.point)) 
				{
					m_score = 2;
				} else {
					m_score = 1;
				}
			}
		} 
		else 
		{
			m_score = 0;
		}
	}
}
