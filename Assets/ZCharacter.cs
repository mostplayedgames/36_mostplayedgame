﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MPCharacter : MonoBehaviour 
{
	public GameObject m_objCharacter;

	public void SetCharacter(int p_idx)
	{
		for (int idx =0; idx < m_objCharacter.transform.childCount; idx++) 
		{
			m_objCharacter.transform.GetChild (idx).gameObject.SetActive (false);
		}
		m_objCharacter.transform.GetChild(p_idx).gameObject.SetActive(true);
	}

	public void LoadCharacters()
	{

	}

}
