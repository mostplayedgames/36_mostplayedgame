﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum POLE_STATE{
	GAPFIXED,
	GAPMOVING,
	GAPCLOSING,
	GAPCHANGEPOS,
	GAPCHOMPING
}
public class PoleObject : MonoBehaviour {

	public bool isHit = true;

	public GameObject m_objectScore1;
	public GameObject m_objectScore2;

	public GameObject m_topPipeObject; 
	public GameObject m_bottomPipeObject; 

	public Vector3 m_topPipePosition; 
	public Vector3 m_bottomPipePosition;

	public Vector2 m_difficultyChange;

	public TextMesh m_textMultiplier;

	public List<Material> m_listColorMaterials;

	public GameObject m_objectCoin;
	public GameObject m_objectSubCoin;

	int m_tuningDifficulty;
	int m_currentIndex;
	float m_gapDistanceTuning;

	POLE_STATE m_poleType;
	// Use this for initialization
	void Start () {
		//m_poleType = POLE_STATE.GAPFIXED;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Spawn(int tuningDifficulty, int currentIndex)
	{
		//Tuning_SetGapDistance (tuningDifficulty, currentIndex);
		m_tuningDifficulty = tuningDifficulty;
		m_currentIndex = currentIndex;

		int multiplierIndex = Mathf.FloorToInt((currentIndex+1) / 10f) + 1;
		if (multiplierIndex >= 5)
			multiplierIndex = 5;
		multiplierIndex--;
		
		//if (multiplierIndex >= 1)
		//	m_textMultiplier.text = "+" + multiplierIndex;
		//else
			m_textMultiplier.text = "";

		switch (multiplierIndex) {
		case 2:
			m_textMultiplier.color = GameScene.instance.m_listTextMultiplierColor [1];
			break;
		case 3:
			m_textMultiplier.color = GameScene.instance.m_listTextMultiplierColor [2];
			break;
		case 4:
			m_textMultiplier.color = GameScene.instance.m_listTextMultiplierColor [3];
			break;
		case 5:
			m_textMultiplier.color = GameScene.instance.m_listTextMultiplierColor [4];
			break;
		default:
			m_textMultiplier.color = GameScene.instance.m_listTextMultiplierColor [0];
			break;
		}
		
		Tuning_SetGapDistance ();

		if (currentIndex > 20) {
			m_objectCoin.SetActive (true);
		} else if (currentIndex > 15) {
			if (Random.Range (0, 100) > 20) {
				m_objectCoin.SetActive (true);
			} else {
				m_objectCoin.SetActive (false);
			}
		} else if (currentIndex > 10) {
			if (Random.Range (0, 100) > 40) {
				m_objectCoin.SetActive (true);
			} else {
				m_objectCoin.SetActive (false);
			}
		} else if (currentIndex > 5) {
			if (Random.Range (0, 100) > 60) {
				m_objectCoin.SetActive (true);
			} else {
				m_objectCoin.SetActive (false);
			}
		} else {
			if (Random.Range (0, 100) > 70) {
				m_objectCoin.SetActive (true);
			} else {
				m_objectCoin.SetActive (false);
			}//m_objectCoin.SetActive (true);
		}

		LeanTween.cancel (m_objectCoin.gameObject);

		m_objectCoin.transform.localPosition = new Vector3 (0, -0.665f, 0);
		m_objectCoin.transform.localScale = new Vector3 (1.25f, 1.25f, 1.25f);
		m_objectSubCoin.GetComponent<ZRotator> ().enabled = true;
	}

	public void FlyCoin()
	{
		m_objectSubCoin.GetComponent<ZRotator> ().enabled = false;
		m_objectSubCoin.transform.localEulerAngles = Vector3.zero;
	}

	public void SetPipeColor(int index)
	{
		this.m_topPipeObject.GetComponent<Renderer> ().material = m_listColorMaterials [index % m_listColorMaterials.Count];
		this.m_bottomPipeObject.GetComponent<Renderer> ().material = m_listColorMaterials [index % m_listColorMaterials.Count];


	}

	public void Tuning_SetGapDistance()
	{
		LeanTween.cancel (m_bottomPipeObject);
		LeanTween.cancel (m_topPipeObject);
		m_gapDistanceTuning = Random.Range (m_difficultyChange.x, m_difficultyChange.y);

		int bestScore = GameScene.instance.GetBestScore ();
		int newBestScoreBasis = Mathf.FloorToInt(bestScore / 10f);

		if (m_currentIndex > 50 - bestScore) {
			m_gapDistanceTuning = Random.Range (-0.025f, -0.02f);
		} 
		else if (m_currentIndex > 40 - bestScore) {
			m_gapDistanceTuning = Random.Range (-0.02f, -0.01f);
		} 
		else if (m_currentIndex > 30 - bestScore) {
			m_gapDistanceTuning = Random.Range (0, -0.01f);
		} 
		else if (m_currentIndex > 20 - bestScore) {
			m_gapDistanceTuning = Random.Range (0, 0.02f);
		} else if (m_currentIndex > 10 - bestScore) {
			m_gapDistanceTuning = Random.Range (0f, 0.035f);
		} else if (m_currentIndex > 7 - bestScore) {
			m_gapDistanceTuning = Random.Range (0.035f, 0.04f);
		} else if (m_currentIndex > 5 - bestScore) {
			m_gapDistanceTuning = Random.Range (0.035f, 0.05f);
		} else {
			m_gapDistanceTuning = Random.Range (0.05f, 0.06f);
		}

		//m_gapDistanceTuning = -0.045f;

		m_topPipeObject.transform.localPosition = m_topPipePosition + new Vector3 (0, m_gapDistanceTuning, 0);
		m_bottomPipeObject.transform.localPosition = m_bottomPipePosition - new Vector3 (0, m_gapDistanceTuning, 0);

		//LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3(0,difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
		//LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3(0, difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
	}

	public void IntroAnim_Fixed()
	{
	}

	public void IntroAnim_ClawIn()
	{
	}

	public void IntroAnim_MoveIn()
	{
	}

	public void Behavior_GapFixed()
	{
		m_poleType = POLE_STATE.GAPFIXED;
	}

	public void Behavior_GapMoving ()
	{
		isActivate = false;
		m_poleType = POLE_STATE.GAPMOVING;
	}

	bool isActivate = true;
	public void Behavior_GapClosing ()
	{
		//ForceClose ();

		isActivate = false;
		m_poleType = POLE_STATE.GAPCLOSING;
	}

	public void Behavior_GapChangePos ()
	{
		m_poleType = POLE_STATE.GAPCHANGEPOS;
	}

	public void Behavior_Chomping ()
	{
		m_poleType = POLE_STATE.GAPCHOMPING;
	}

	public void MovePole()
	{
		GameObject spawnObject = this.gameObject;

		float exactYPosition = 0;
		exactYPosition = spawnObject.transform.position.y;
		if (spawnObject.transform.position.y > 87f) { // going up
			spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x, 
				spawnObject.transform.position.y - 50f, 
				spawnObject.transform.position.z);

			LeanTween.moveY (spawnObject, exactYPosition, Random.Range (1.8f, 2.6f)).setLoopPingPong ();
		} else { // going down
			spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x, 
				spawnObject.transform.position.y + 50f, 
				spawnObject.transform.position.z);

			LeanTween.moveY (spawnObject, exactYPosition, Random.Range (1.8f, 2.6f)).setLoopPingPong ();
		}


		m_topPipeObject.transform.localPosition = m_topPipePosition + new Vector3 (0, m_gapDistanceTuning + 0.01f, 0);
		m_bottomPipeObject.transform.localPosition = m_bottomPipePosition - new Vector3 (0, m_gapDistanceTuning + 0.01f, 0);

	}

	public void ForceClose()
	{
		LeanTween.cancel (m_bottomPipeObject);
		LeanTween.cancel (m_topPipeObject);

		m_topPipeObject.transform.localPosition = m_topPipePosition + new Vector3 (0, m_gapDistanceTuning + 0.31f, 0);
		m_bottomPipeObject.transform.localPosition = m_bottomPipePosition - new Vector3 (0, m_gapDistanceTuning + 0.31f, 0);

		//m_topPipeObject.transform.localPosition += new Vector3 (0, 0.47f, 0);
		//m_bottomPipeObject.transform.localPosition -= new Vector3 (0, 0.47f, 0);
		float randomClose = Random.Range (3.2f, 3.2f);
		LeanTween.moveLocalY (m_topPipeObject, 0.224f, randomClose);//.setEase(LeanTweenType.easeInQuad);
		LeanTween.moveLocalY (m_bottomPipeObject, -1.595f, randomClose);//.setEase(LeanTweenType.easeInQuad);
		//CloseIn ();
	}

	void CloseIn()
	{
		LeanTween.cancel (m_bottomPipeObject);
		LeanTween.cancel (m_topPipeObject);
		float randomClose = Random.Range (2f, 2.8f);
		LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3 (0, -0.3f, 0), randomClose);//.setEase(LeanTweenType.easeInQuad);
		LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3(0, -0.3f,0), randomClose);//.setEase(LeanTweenType.easeInQuad);
	}

	public void Activate()
	{
		if (isActivate)
			return;
		else
			isActivate = true;
		
		Debug.Log ("Activate");
		switch (m_poleType) {
		case POLE_STATE.GAPMOVING:
				MovePole ();
			break;
		case POLE_STATE.GAPCLOSING:
				ForceClose ();
			break;
		}
	}

	public void Score()
	{
		isHit = true;
		m_objectScore1.GetComponent<Renderer>().material.color = Color.yellow;
		m_objectScore2.GetComponent<Renderer>().material.color = Color.yellow;

//		GameScene.instance.m_particleScore1.transform.position = m_objectScore1.transform.position;
//		GameScene.instance.m_particleScore2.transform.position = m_objectScore2.transform.position;
//		GameScene.instance.m_particleScore1.gameObject.SetActive (true);
//		GameScene.instance.m_particleScore2.gameObject.SetActive (true);
//		//m_particleSystem.gameObject.GetComponent<ParticleSystem> ().spe
//
//		if (GameScene.instance.GetScore() > 0 && GameScene.instance.GetScore () % 5 == 0) {
//			GameScene.instance.m_particleScore1.gameObject.GetComponent<ParticleSystem> ().startSpeed = 100f;
//			GameScene.instance.m_particleScore2.gameObject.GetComponent<ParticleSystem> ().startSpeed = 100f;
//			GameScene.instance.m_particleScore1.gameObject.GetComponent<ParticleSystem> ().Emit (Random.Range (55, 80));
//			GameScene.instance.m_particleScore2.gameObject.GetComponent<ParticleSystem> ().Emit (Random.Range (55, 80));
//			//Handheld.Vibrate ();
//		} else {
//			GameScene.instance.m_particleScore1.gameObject.GetComponent<ParticleSystem> ().startSpeed = 50f;
//			GameScene.instance.m_particleScore2.gameObject.GetComponent<ParticleSystem> ().startSpeed = 50f;
//			GameScene.instance.m_particleScore1.gameObject.GetComponent<ParticleSystem> ().Emit (Random.Range (15, 20));
//			GameScene.instance.m_particleScore2.gameObject.GetComponent<ParticleSystem> ().Emit (Random.Range (15, 20));
//
//		}
	}

	public void Reset(){
		isHit = false;
		m_objectScore1.GetComponent<Renderer>().material.color = Color.white;
		m_objectScore2.GetComponent<Renderer>().material.color = Color.white;


	}


	public void Spawn_Old(int tuningDifficulty, int currentIndex)
	{

		/*
		//if (currentIndex <= 0)
		//	return;

		//m_topPipeObject.transform.localPosition += new Vector3 (0, 3, 0);
		//m_bottomPipeObject.transform.localPosition -= new Vector3 (0, 3, 0);


		float difficulty = Random.Range (m_difficultyChange.x, m_difficultyChange.y);

		if (currentIndex > 40) {
			difficulty = Random.Range (-0.05f, -0.02f);
		} 
		else if (currentIndex > 30) {
			difficulty = Random.Range (0, -0.02f);
		} 
		else if (currentIndex > 20) {
			difficulty = Random.Range (0, 0.02f);
		} else if (currentIndex > 10) {
			difficulty = Random.Range (0f, 0.035f);
		} else {
			difficulty = Random.Range (0f, 0.07f);
		}


		//LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3(0,difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
		//LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3(0, difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);

		switch(GameScene.instance.m_birdType){
		case 4:
			LeanTween.cancel (m_bottomPipeObject);
			LeanTween.cancel (m_topPipeObject);
			if (Random.Range (0, 100) > 100) {
				LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3 (0, difficulty + 0.4f, 0), 0.8f).setOnComplete (CloseIn);//.setEase(LeanTweenType.easeInQuad);
				LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3 (0, difficulty + 0.4f, 0), 0.8f);//.setEase(LeanTweenType.easeInQuad);
			} else {
				m_topPipeObject.transform.localPosition = m_topPipePosition + new Vector3 (0, difficulty + 0.4f, 0);
				m_bottomPipeObject.transform.localPosition = m_bottomPipePosition - new Vector3 (0, difficulty + 0.4f, 0);

				LeanTween.moveLocalY (m_topPipeObject, m_topPipeObject.transform.localPosition.y + 0.5f, 0.8f).setLoopPingPong ();//.setEase(LeanTweenType.easeInQuad);
				LeanTween.moveLocalY (m_bottomPipeObject, m_bottomPipeObject.transform.localPosition.y + 0.5f, 0.8f).setLoopPingPong ();//.setEase(LeanTweenType.easeInQuad);
			}
			break;
		default:
			//LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3(0,difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
			//LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3(0, difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
			LeanTween.cancel (m_bottomPipeObject);
			LeanTween.cancel (m_topPipeObject);
			if (true){//Random.Range (0, 100) > 0) {
				LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3(0,difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
				LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3(0, difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
			} else {
				m_topPipeObject.transform.localPosition = m_topPipePosition + new Vector3(0,difficulty + 0.05f,0);
				m_bottomPipeObject.transform.localPosition = m_bottomPipePosition - new Vector3(0, difficulty- 0.05f,0) ;

				float timeRandom = Random.Range (0.5f, 1f);
				LeanTween.moveLocalY (m_topPipeObject, m_topPipeObject.transform.localPosition.y - 0.1f, timeRandom).setLoopPingPong ();//.setEase(LeanTweenType.easeInQuad);
				LeanTween.moveLocalY (m_bottomPipeObject, m_bottomPipeObject.transform.localPosition.y + 0.1f, timeRandom).setLoopPingPong ();//.setEase(LeanTweenType.easeInQuad);
			}
			break;
		}*/
	}

}
