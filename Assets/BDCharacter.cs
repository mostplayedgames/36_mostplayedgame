﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BDCharacter : MonoBehaviour {

	public BDGameInstance m_gameInstance;

	public ParticleSystem m_hitParticle;

	public float m_forceAdded;

	public void OnCollisionEnter2D(Collision2D p_other)
	{
		if (p_other.collider.tag == "Target") 
		{
			GameScene.instance.Score (1);
			p_other.gameObject.SetActive (false);
			LeanTween.cancel (p_other.gameObject);
			m_gameInstance.SpawnParticle (p_other.transform.position);
			ResetCharacter ();
		} 
		else if (p_other.collider.tag == "Platform") 
		{
			ResetCharacter (true);
			GameScene.instance.Die ();
		}
	}

	public void Initialize()
	{
		this.transform.position = Vector3.zero;
		this.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
		this.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		this.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
	}

	public void Shoot()
	{
		this.GetComponent<Rigidbody2D> ().isKinematic = false;
		this.GetComponent<Rigidbody2D> ().AddForce (this.transform.right * m_forceAdded);
	}

	private void ResetCharacter(bool p_isDeath = false)
	{
		this.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
		m_gameInstance.m_curState = GameInstanceStates.INGAME;

		if (GameScene.instance.GetScore () == 0 || p_isDeath) 
		{
			this.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
		}
	}
}
