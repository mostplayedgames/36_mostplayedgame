﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FCCharacterDieChecker : MonoBehaviour {

	// Use this for initialization
	void Start () {
		LeanTween.scaleY (this.gameObject, 0.97f, 1f).setLoopPingPong ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Floor") {
			GameScene.instance.Die ();
		}
	}
}
