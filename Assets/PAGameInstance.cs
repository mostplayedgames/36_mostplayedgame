﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PAGameState
{
	IDLE,
	IN_GAME,
	RESULTS
}

// Party Animal Game Instance
// Template script to test the new framework
public class PAGameInstance : MPGameInstance 
{
	public PAGameState m_curPAState;
	public PACharacter m_PACharacter;

	public GameObject m_prefabPlatform;

	public List<int> m_listOfObstacleBehaviours 	= new List<int>();
	public List<int> m_listOfDifficulty 			= new List<int> ();
	public List<Color> m_listOfColors 				= new List<Color> ();

	public float m_currentPlatformIdx 				= 0;

	private int m_currentObstacleBehaviour			= 0;
	private int m_currentDifficulty					= 0;

	private float m_chargeTime 		= 0;
	private float m_currentX 		= -44;
	private float m_currentY 		= -114f;
	private int m_colorIdx 			= 0;
	private float POLE_MIN_HEIGHT 	= 27f;
	private float POLE_MAX_HEIGHT 	= 160f;


	public void Awake()
	{
		ZObjectMgr.Instance.AddNewObject (m_prefabPlatform.name, 10, this.gameObject);
		m_PACharacter.gameObject.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;

		m_colorIdx = Random.Range (0, m_listOfColors.Count - 1);
		Camera.main.backgroundColor = m_listOfColors [m_colorIdx];

		m_currentObstacleBehaviour = Random.Range (0, m_listOfObstacleBehaviours.Count);
		m_currentDifficulty = 0;
	}

	public override void Gamescene_SetupLevel ()
	{
		m_PACharacter.gameObject.transform.position = GameScene.instance.m_objectCharacterRoot.gameObject.transform.position;
		m_PACharacter.gameObject.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
		m_PACharacter.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		m_PACharacter.Initialize ();

		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = 137.8f;
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().transform.localPosition = new Vector3 (12f, 15f, -250);
		ZCameraMgr.instance.transform.localEulerAngles = new Vector3 (0, 0, 0);
		Camera.main.orthographicSize = 125f;

		m_colorIdx++;

		if (m_colorIdx >= m_listOfColors.Count - 1) 
		{
			m_colorIdx = 0;
		}
		Camera.main.backgroundColor = m_listOfColors[m_colorIdx];

		SpawnLevel ();	
	}

	public override void Gamescene_Update ()
	{
		Controls ();
		ZCameraMgr.instance.transform.position = new Vector3(m_PACharacter.gameObject.transform.position.x + 40f, 15, -250f);
	}

	public override void Gamescene_Score ()
	{
		SpawnPlatform ();
		GameScene.instance.AddCoins (1);
	}

	public override void Gamescene_Die (){}
	public override void Gamescene_ButtonDown (){}
	public override void Gamescene_ButtonUp (){}
	public override void Gamescene_ButtonHold (){}

	private void SpawnLevel()
	{
		m_currentX = -44f;
		m_currentPlatformIdx = 0;
		for (int i = 0; i < 5; i++) 
		{
			SpawnPlatform ();
		}
	}

	private void SpawnPlatform()
	{
		GameObject spawnedPlatform = ZObjectMgr.Instance.Spawn2D (m_prefabPlatform.name, Vector2.zero);
		spawnedPlatform.transform.parent = this.transform;
		float addToCurrentX = Random.Range (30, 90);

		int spawnTuning = m_listOfDifficulty[m_currentDifficulty % m_listOfDifficulty.Count];

		// If spawn tuning is 0 Go Wild Go Random
		int randomTuning = Random.Range (0, 100);
		if (spawnTuning == 0) {
			if (randomTuning > 70) {
				spawnTuning = 3;
			} else if (randomTuning > 40) {
				spawnTuning = 2;
			} else {
				spawnTuning = 1;
			}
		}

		if (spawnTuning == 1) {
			spawnTuning = 2;
		}

		// 1 - Set Direction
		float difficultyToAdd = 60f;
		int direction = 0;
		if (direction == 0) 
		{
			direction = (Random.Range (0, 100) > 50) ? (-1) : (1);
		}

		if (m_currentY == POLE_MAX_HEIGHT) 
		{
			direction = -1;
		}
		else if (m_currentY == POLE_MIN_HEIGHT) 
		{
			direction = 1;
		} 

		// 2 Look for X
		if (m_currentPlatformIdx < 4) {
			addToCurrentX = 80f;
		}
		else
		{
			int randomAddCurrentX = Random.Range (0, 100);
			if (randomAddCurrentX > 80) {
				addToCurrentX = Random.Range (80f, 100f);
			} else if (randomAddCurrentX > 30) {
				addToCurrentX = Random.Range (60, 80f);
			} else {
				addToCurrentX = Random.Range (50f, 60f);
			}
		}

		// 3 Find Y
		if (spawnTuning >= 3) 
		{
			spawnTuning = 2;
		}
		if (direction == 1) 
		{
			switch (spawnTuning) 
			{
				case 1:
				{
					difficultyToAdd = Random.Range (m_currentY - 30f, m_currentY + 30f);
				}
				break;

				case 2:
				difficultyToAdd = Random.Range (m_currentY - 50f, m_currentY + 50f);
				break;

				case 3:
				if (addToCurrentX <= 70f) 
				{
					addToCurrentX = 70f;
				}
				if( m_currentPlatformIdx > 30 )
					difficultyToAdd = addToCurrentX * 1.2f;
				else if( m_currentPlatformIdx > 20 )
					difficultyToAdd = addToCurrentX * 1.1f;
				else
					difficultyToAdd = addToCurrentX * 1f;//Random.Range (addToCurrentX * 1.1f, addToCurrentX * 1.4f);

				break;
			}
		} else {
			switch (spawnTuning) 
			{
			case 1: // EASY
				difficultyToAdd = Random.Range (m_currentY - 20f, m_currentY + 30f);
				break;
			case 2: // MEDIUM
				difficultyToAdd = Random.Range (m_currentY - 50f, m_currentY + 70f);
				break;
			case 3: // HARD
				if (addToCurrentX <= 80f) {
					addToCurrentX = 80f;
				}


				if( m_currentPlatformIdx > 30 )
					difficultyToAdd = addToCurrentX * 1.6f;
				else if( m_currentPlatformIdx > 20 )
					difficultyToAdd = addToCurrentX * 1.5f;
				else
					difficultyToAdd = addToCurrentX * 1.4f;//Random.Range (addToCurrentX * 0.9f, addToCurrentX * 1.2f);
				break;
			}
		}

		float newYPos = difficultyToAdd;

		if (m_currentPlatformIdx == 0) 
		{
			newYPos = -114f;
			addToCurrentX = 0;
		} 
		m_currentX += addToCurrentX;


		int currentBehaviour = m_listOfObstacleBehaviours [m_currentObstacleBehaviour & m_listOfObstacleBehaviours.Count];

		//Debug.Log ("Current Behaviour: " + currentBehaviour);

		spawnedPlatform.transform.localPosition = new Vector2 (m_currentX, newYPos);
		spawnedPlatform.GetComponent<FDPlatform> ().InitializePlatform (m_currentPlatformIdx, currentBehaviour);

		m_currentPlatformIdx++;



		m_currentDifficulty++;

		if (m_currentDifficulty >= m_listOfDifficulty.Count - 1) {
			m_currentDifficulty = 0;
		}

		m_currentObstacleBehaviour++;
		if (m_currentObstacleBehaviour >= m_listOfObstacleBehaviours.Count - 1) {
			m_currentObstacleBehaviour = 0;
		}
	}

	private void Controls()
	{
//		if (!m_PACharacter.canCharacterJump ()) 
//		{
//			return;
//		}

		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			GameScene.instance.Score (2);
			Debug.Log ("UpdateScore: ");
		}

		if (Input.GetMouseButtonDown (0)) 
		{
			m_PACharacter.ChargeJump ();
		}
		else if (Input.GetMouseButton (0)) 
		{
			m_chargeTime += 15 * Time.deltaTime;
		}
		else if(Input.GetMouseButtonUp(0))
		{
			m_PACharacter.Jump (m_chargeTime);
			m_chargeTime = 0;
		}
	}
}
