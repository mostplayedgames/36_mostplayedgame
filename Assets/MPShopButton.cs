﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MPShopButton : MonoBehaviour 
{	
	public MPShopItemData m_shopItemData;

	public GameObject m_objChar;
	public Image m_imgOverlay; 
	public Image m_imgBackground;
	public Text m_textQuestionMark;

	public void Initialize(MPShopItemData p_characterData)
	{
		m_shopItemData = p_characterData;
		GameObject obj = Instantiate (p_characterData.ItemObject, this.transform.position, Quaternion.identity);
		obj.transform.parent = m_objChar.transform;
		obj.transform.localPosition = Vector3.zero;
		obj.transform.localEulerAngles = new Vector3 (18, -135, 18);
		obj.transform.localScale = new Vector3 (3.5f, 3.5f, 3.5f);
	}

	public void SelectItem()
	{
		GameScene.instance.SetItem (m_shopItemData.ItemId);

	}

	public void Update()
	{
		//CheckIfOffscreen ();
	}

	private void CheckIfOffscreen()
	{
		Vector3 pos1 = Camera.main.transform.position - new Vector3(0, 100, 0);
		Vector3 pos2 = this.transform.position;
		float dist = Vector2.Distance (pos2, pos1);

		if (dist > 150) {
			m_objChar.gameObject.SetActive (false);
			m_imgOverlay.gameObject.SetActive (false);
			m_imgBackground.gameObject.SetActive (false);
			m_textQuestionMark.gameObject.SetActive (false);
			this.GetComponent<Image> ().enabled = false;
		} else {
			m_objChar.gameObject.SetActive (true);
			m_imgOverlay.gameObject.SetActive (true);
			m_imgBackground.gameObject.SetActive (true);
			m_textQuestionMark.gameObject.SetActive (true);
			this.GetComponent<Image> ().enabled = true;
		}
	}
}
