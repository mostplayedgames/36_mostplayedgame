﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Mode", menuName = "MPGameData")]
public class MPGameData : ScriptableObject 
{
	[Header("GAME ID")]
	public string GameName;
	public string GameId;
	public string LeaderboardID;

	[Space]
	[Header("ARTWORK")]
	public Color GameColor;
	public Sprite TitleArtwork;
	public Sprite ButtonArtwork;

	[Space]
	[Header("GAME DATA")]
	public string Tutorial;
	public int Highscore;
	public int Attempts;
}
 