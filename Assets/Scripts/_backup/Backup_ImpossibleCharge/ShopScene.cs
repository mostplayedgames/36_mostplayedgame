﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShopScene : ZGameScene 
{

	public static ShopScene instance;

	public Color m_colorBird;
	public Color m_colorNotbought;

	public AudioClip m_audioButton;
	public AudioClip m_audioReward;
	public AudioClip m_audioCelebrate;
	public AudioClip m_audioPop;
	public AudioClip m_audioCharge;
	public AudioClip m_audioFall;

	public GameObject m_objectShopContainer;

	public GameObject m_objectButton;
	public GameObject m_objectButtonUnlock;

	public ParticleSystem m_particleCelebrate;

	public List<ZShopButton> m_listButtons;

	public Text m_textPrice;
	public Text m_textButtonPrice;
	public Text m_textRarity;

	public Sprite m_spriteLocked;
	public Sprite m_spriteUnlocked;

	public Text m_textName;

	public GameObject m_objectFlash;

	public GameObject m_objectScroll;

	//public List<GameObject> m_objectBottles;

	public RectTransform m_rectTransformContent;

	int currentUnlockedBird;
	void Awake()
	{
		instance = this;

		currentUnlockedBird = PlayerPrefs.GetInt ("currentUnlockedBird");
	}

	public void Back()
	{
		this.gameObject.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		GameScene.instance.m_rootUIScore.gameObject.SetActive (true);
		GameScene.instance.m_rootMain.gameObject.SetActive (true);
		//GameScene.instance.SetupLevel ();


		// ResetItemsToUnlock
		//GameScene.instance.m_listItemsToUnlock.Clear ();
		//GameScene.instance.m_newBottlesToUnlock = 6;

		GameScene.instance.m_eState = GAME_STATE.IDLE;
		GameScene.instance.SetupLevel ();
		//GameScene.instance.ResetCamera ();
	}


	GameObject boxObject;
	int unlockCount = 0;
	float currentTimer;
	float currentTimerUp;
	int currentShopRarity;

	public void UnlockNow()
	{
		//boxObject = GameScene.instance.m_objectBox;

		GameScene.instance.SpendCoins (MPShopManager.Instance.GetCurrentShopPrice ());

		m_objectButtonUnlock.SetActive (false);

		int unlockType = MPUnlockManager.Instance.m_listToRandomUnlock [currentUnlockedBird+1];
		if (unlockType >= 20) {
			unlockCount = 13;
			currentShopRarity = 2;
		} else if (unlockType >= 12) {
			unlockCount = 14;
			currentShopRarity = 1;
		} else {
			unlockCount = 15;
			currentShopRarity = 0;
		}
		//unlockCount = 15;//14//13
		currentTimer = 0.001f;
		currentTimerUp = 0;
		m_textRarity.text = "";

		m_textName.text = "";
		UnlockRoutine ();
	}

	void UnlockRoutine()
	{
		if( unlockCount < 5 )
			LeanTween.rotateLocal (boxObject, boxObject.transform.localEulerAngles + new Vector3 (0, 90, 0), 0.2f).setEase(LeanTweenType.easeInOutCubic).setOnComplete(UnlockRoutineStop);
		else
			LeanTween.rotateLocal (boxObject, boxObject.transform.localEulerAngles + new Vector3 (0, 90, 0), 0.12f).setEase(LeanTweenType.easeInOutCubic).setOnComplete(UnlockRoutineStop);
		
		unlockCount--;
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		currentTimerUp += 0.005f;
		currentTimer += currentTimerUp;
	}

	void UnlockRoutineStop()
	{
		if (unlockCount <= 0) {
			LeanTween.scale (boxObject, boxObject.transform.localScale + new Vector3 (20f, 20f, 20f), 0.2f).setEase (LeanTweenType.easeInOutCubic).setOnComplete(UnlockRoutineStop2);//.setLoopPingPong ().setLoopCount (2);
			//UnlockRoutineShake();
			ZAudioMgr.Instance.PlaySFX (m_audioReward);
		} else {
			LeanTween.delayedCall (currentTimer, UnlockRoutine);
		}
	}

	void UnlockRoutineStop2()
	{
		LeanTween.delayedCall(0.15f, UnlockRoutineShake);
	}

	void UnlockRoutineShake()
	{
		ZAudioMgr.Instance.PlaySFX (m_audioCharge);

		LeanTween.scale (boxObject, boxObject.transform.localScale - new Vector3 (0, 50f, 0), 2f).setEase (LeanTweenType.easeInOutCubic).setOnComplete(Explode);//.setOnComplete(UnlockRoutineShake);//.setLoopPingPong ().setLoopCount (2);
		boxObject.transform.localEulerAngles += new Vector3(0,10,0);
		//boxObject.transform.localPosition += new Vector3 (0, 0, 10);
		LeanTween.rotate (boxObject, boxObject.transform.localEulerAngles + new Vector3 (0, -10, 0), 0.04f).setLoopPingPong ();//.setEase(LeanTweenType.easeInOutQuad;
	}

	void Explode()
	{
		LeanTween.scale (boxObject, boxObject.transform.localScale + new Vector3 (200f, 200f, 200f), 0.2f);
		m_particleCelebrate.GetComponent<ParticleSystem> ().Clear ();
		m_particleCelebrate.gameObject.SetActive (true);
		m_particleCelebrate.GetComponent<ParticleSystem> ().Play ();
		if (currentShopRarity == 2) {
			m_particleCelebrate.GetComponent<ParticleSystem> ().Emit (200);
		} else if (currentShopRarity == 1) {
			m_particleCelebrate.GetComponent<ParticleSystem> ().Emit (100);
		}
		LeanTween.delayedCall (0.2f, Explode2);

		ZAudioMgr.Instance.PlaySFX (m_audioPop);
		ZAudioMgr.Instance.StopSFX (m_audioCharge);
	}

	void Explode2()
	{
		m_objectFlash.gameObject.SetActive (true);


		LeanTween.delayedCall (0.1f, DisableFlash);
	}

	void DisableFlash()
	{
		UnlockNextBird ();
		m_objectFlash.gameObject.SetActive (false);


	}

	void UnlockNextBird()
	{
		boxObject.gameObject.SetActive (false);
		currentUnlockedBird = PlayerPrefs.GetInt ("currentUnlockedBird");
		currentUnlockedBird++;
		//GameScene.instance.BuyItem (MPUnlockManager.Instance.m_listToRandomUnlock[currentUnlockedBird]);
		PlayerPrefs.SetInt ("currentUnlockedBird", currentUnlockedBird);
		PlayerPrefs.Save ();

		Debug.Log ("Unlock Next Bird");

		m_listButtons [MPUnlockManager.Instance.m_listToRandomUnlock[currentUnlockedBird-1]].m_imageButtonImage.GetComponent<Button> ().Select ();

		GameScene.instance.ShowShop (true);

		ZAudioMgr.Instance.PlaySFX (m_audioCelebrate);

		LeanTween.delayedCall (1f, UnlockNextBirdRoutine);
		//ShopScene.instance.S ();
	}

	void UnlockNextBirdRoutine()
	{
		GameScene.instance.ResetCoins ();
	}

	public void SelectBird()
	{
		int x = 0;
		foreach (ShopBird shopItem in GameScene.instance.m_listShopBirds) {
			if (PlayerPrefs.GetInt ("birdbought" + x) > 0) {
				if (x == GameScene.instance.GetCurrentSelectedItem ()) {
					m_listButtons [x].m_imageButtonImage.color = m_colorBird;
					if (shopItem.rarity == 1) {
						m_textRarity.color = Color.yellow;
						m_textRarity.text = "RARE";
					} else if (shopItem.rarity == 2) {
						m_textRarity.color = Color.magenta;
						m_textRarity.text = "EPIC";
					} else {
						m_textRarity.color = Color.white;
						m_textRarity.text = "";
					}
				}
				else 
					m_listButtons [x].m_imageButtonImage.color = m_colorNotbought;
			}
			x++;
		}
	}

	public void SetupScene(bool p_isUnlock = false)
	{
		//if( !isUnlock )
		//m_rectTransformContent.localPosition = new Vector3 (0, -30, 0);



		m_textName.transform.localScale = new Vector3 (1, 1, 1);
		LeanTween.cancel (m_textName.gameObject);

		if (GameScene.instance.GetCurrentSelectedItem () < 12f) 
		{
			m_objectScroll.transform.localPosition = new Vector3 (-1f, -0.0673768f, 7f);
		} 
		else 
		{
			m_objectScroll.transform.localPosition = new Vector3 (-1f, 217.81f, 7f);
		} 
		/*
		for (int x = 0; x < m_objectBottles.Count; x++) {
			if (PlayerPrefs.GetInt ("birdbought" + x) > 0) {
				//GameScene.instance.m_listShopBirds [x].objectImage.SetActive (true);
				//GameScene.instance.m_listShopBirds [x].imageButtons.GetComponent<Button> ().interactable = false;
				//GameScene.instance.m_listShopBirds [x].objectImage.GetComponent<Image> ().color = new Color (1,1,1,1);
				//GameScene.instance.m_listShopBirds [x].imageButtons.color = m_colorBird;
				//GameScene.instance.m_listShopBirds [x].objectPrice.GetComponent<Text> ().color = m_colorTextColorBought;
				//m_listObjectShadow [x].SetActive (true);

				if (x > 30) 
				{
					GameScene.instance.m_newBottlesToUnlock -= 1;
				}

				m_objectBottles[x].SetActive(true);
				GameScene.instance.m_listShopBirds [x].imageButtons.enabled = true;
			} else {
				//GameScene.instance.m_listShopBirds [x].objectImage.SetActive (false);
				//GameScene.instance.m_listShopBirds [x].imageButtons.GetComponent<Button> ().interactable = true;

				//GameScene.instance.m_listShopBirds [x].objectImage.GetComponent<Image> ().color = new Color (0, 0, 0, 0.5f);
				//GameScene.instance.m_listShopBirds [x].imageButtons.color = m_colorNotbought;
				//GameScene.instance.m_listShopBirds [x].objectPrice.SetActive (true);
				GameScene.instance.m_listItemsToUnlock.Add(x);

				GameScene.instance.m_listShopBirds [x].imageButtons.enabled = false;
				//m_listObjectShadow [x].SetActive (false);
				//GameScene.instance.m_listShopBirds [x].objectImage.GetComponent<Image> ().enabled = false;
				m_objectBottles[x].SetActive(false);
			}
		}

		/*fom_colorBirdr (int x = 0; x < 10; x++) {
			if (PlayerPrefs.GetInt ("ballbought" + x) > 0) {
				GameScene.instance.m_listShopBalls [x].objectImage.SetActive (true);
				GameScene.instance.m_listShopBalls [x].objectImage.GetComponent<Image> ().color = new Color (0, 0, 0, 0.2f);
				GameScene.instance.m_listShopBalls [x].imageButtons.color = m_colorBalls;
				GameScene.instance.m_listShopBalls [x].objectPrice.SetActive (false);
			} else {
				GameScene.instance.m_listShopBalls [x].objectImage.SetActive (false);
				GameScene.instance.m_listShopBalls [x].objectImage.GetComponent<Image> ().color = new Color (1,1,1,1);
				GameScene.instance.m_listShopBalls [x].imageButtons.color = m_colorNotboughBalls;
				GameScene.instance.m_listShopBalls [x].objectPrice.SetActive (true);
			}
		}*/

		/*int y = 0;
		foreach (Text txt in m_listText) 
		{
			if (PlayerPrefs.GetInt ("birdbought" + y) > 0)
				txt.text = "";
			else
				txt.text = "?";
				//txt.text = GameScene.instance.GetCurrentShopPrice () + " FLIPS";
				
			y++;
		}*/

		//foreach (Text txt in m_listTextBalls) {
		//	txt.text = 250 + "";
		//}

		//m_textButtonPrice.text = "" + GameScene.instance.GetCurrentShopPrice ();

		m_objectFlash.gameObject.SetActive (false);

		int x = 0;
		foreach (ShopBird shopItem in GameScene.instance.m_listShopBirds) 
		{
			if (PlayerPrefs.GetInt ("birdbought" + x) > 0) {
				m_listButtons [x].m_imageButtonOverlay.gameObject.SetActive (false);
				m_listButtons [x].m_textLabel.gameObject.SetActive (false);
				m_listButtons [x].m_imageButtonImage.sprite = m_spriteUnlocked;

				if (x == GameScene.instance.GetCurrentSelectedItem ()) {
					m_listButtons [x].m_imageButtonImage.color = m_colorBird;
					if (shopItem.rarity == 1) {
						m_textRarity.color = Color.yellow;
						m_textRarity.text = "RARE";
					} else if (shopItem.rarity == 2) {
						m_textRarity.color = Color.magenta;
						m_textRarity.text = "EPIC";
					} else {
						m_textRarity.color = Color.white;
						m_textRarity.text = "";
					}
				} else {
					m_listButtons [x].m_imageButtonImage.color = m_colorNotbought;
				}
			}
			else 
			{
				m_listButtons [x].m_imageButtonOverlay.gameObject.SetActive (true);
				m_listButtons [x].m_textLabel.gameObject.SetActive (true);
				m_listButtons [x].m_imageButtonImage.sprite = m_spriteLocked;;
				m_listButtons [x].m_imageButtonImage.color = m_colorNotbought;
			}


			//else
				

			m_listButtons [x].m_imageCharacterPortrait.sprite = shopItem.imagePortrait;

			x++;
		}

		if (p_isUnlock) {
			m_objectButtonUnlock.SetActive (false);
			m_objectButton.gameObject.SetActive (true);

			m_objectShopContainer.SetActive (true);
			Vector3 currentPosition;
			currentPosition = m_objectShopContainer.gameObject.transform.localPosition;
			m_objectShopContainer.gameObject.transform.localPosition -= new Vector3 (0, 60, 0);
			LeanTween.moveLocal (m_objectShopContainer.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic);

		} else {
			m_objectButton.SetActive (false);
			m_objectButtonUnlock.SetActive (true);
			m_objectShopContainer.SetActive (false);

			m_textRarity.text = "";

			m_textName.text = "UNLOCK\nNEW GIFT!";
			m_textName.transform.localScale = new Vector3 (1, 1, 1);
			LeanTween.scale (m_textName.gameObject, new Vector3 (1.1f, 1.1f, 1.1f), 1f).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);

			m_textName.gameObject.SetActive (true);

		//	boxObject = GameScene.instance.m_objectBox;

			LeanTween.cancel (boxObject);
			boxObject.transform.localEulerAngles = new Vector3 (0, 0, 0);
			boxObject.transform.localScale = new Vector3 (120f, 120f, 120f);

			ZAudioMgr.Instance.PlaySFX (m_audioFall);

		}

		//m_textPrice.text = "" + GameScene.instance.GetCurrentShopPrice ();

		GameScene.instance.m_rootUIScore.gameObject.SetActive (false);
		GameScene.instance.m_rootMain.gameObject.SetActive (false);
	}

	public override void OnButtonUp(ZButton button)
	{
		if (button.name == "btn_play") {
		}
	}
	
	public override void OnButtonDown(ZButton button)
	{
	}
}
