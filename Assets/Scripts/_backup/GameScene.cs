﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

//using ChartboostSDK;
using UnityEngine.Advertisements;

public enum GAME_STATE
{
	START,
	IDLE,
	SHOP,
	AIMING,
	SHOOT,
	IN_GAME,
	RESULTS,
	UNLOCK
}

[System.Serializable]
public class ShopBird
{
	public GameObject objectCharacter;
	public Sprite imagePortrait;
	public string m_textCharacterName;
	public Color colorTrailStart;
	public Color colorTrailEnd;
	public Color colorFeathers;
	public bool isUpdateWeatherColors;
	public Color colorScore;
	public Color colorCameraWeather;
	public Color colorSky;
	public bool willFlat;
	public float spinRate;
	public int rarity;
}

// iTunes - https://itunes.apple.com/us/app/make-pana-blue-eagle/id1119208392?mt=8
// https://play.google.com/store/apps/details?id=com.mostplayed.archereagle
// itms-apps://itunes.apple.com/app/idYOUR_APP_ID
// from iTunes PH : https://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8
// itms - itms-apps://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8

public class GameScene : MonoBehaviour 
{
	public static GameScene instance;
	public bool m_isInDevelopment;
	public MPGameInstance m_inDevelopmentInstance;

	[Header("Stuff")]
	[Space]
	public GameObject m_objModesParent;

	public GameObject m_rootUIScore;
	public GameObject m_rootMain;

	public List<ShopBird> m_listShopBirds;

	public Text m_textTitle;
	public Text m_textTitleShadow;

	public Text m_textTutorial;
	public List<int> m_listUnlockProgress;

	public List<int> m_listItemsToUnlock;
	public int m_newBottlesToUnlock;

	public GameObject m_objectWatchVideo;
	public GameObject m_objectChangeModeBtn;

	public GameObject m_objectParticleBlood;
	public GameObject m_objectParticlePerfect;

	public GameObject m_objectTarget;
	public GameObject m_objectMiss;
	public GameObject m_objectSuccess;
	public GameObject m_objectTutorial;
	public GameObject m_objectTutorialTitle;
	public GameObject m_objectTopBar;
	public GameObject m_objectModeIndicator;
	public GameObject m_objectHitParticle;
	public GameObject m_objectHitBow;
	public GameObject m_objectEarned;

	public GameObject m_objectWindManager;

	public GameObject m_objectTextTutorial;

	public GameObject m_objectRootArrows;
	public GameObject m_objectNoInternet;
	public GameObject m_objectReadyUnlock;
	public GameObject m_objectFullBar;
	public GameObject m_objUiModeTransition;

	public List<GameObject> m_listButtons;

	public AudioClip m_audioShoot;
	public AudioClip m_audioDie;
	public AudioClip m_audioDieBurn;
	public AudioClip m_audioMiss;
	public AudioClip m_audioMiss1;
	public AudioClip m_audioMiss2;
	public AudioClip m_audioMiss3;
	public AudioClip m_audioMiss4;
	public AudioClip m_audioMiss5;
	public AudioClip m_audioSuccess;
	public AudioClip m_audioBow;
	public AudioClip m_audioButton;
	public AudioClip m_audioHeadshot;
	public AudioClip m_audioCatch;
	public AudioClip m_audioBuy;
	public AudioClip m_audioScore;
	public AudioClip m_audioCoin;
	public AudioClip m_audioReward;
	public AudioClip m_audioBonusScore;

	public AudioClip m_audioThrow;

	public Color m_colorTextCorrect;
	public Color m_colorTextMiss;
	public Color m_colorTextSuccess;

	public List<Color> m_listTextMultiplierColor;

	public Text m_textScore;
	public Text m_textScoreShadow;
	public Text m_textLevel;
	public Text m_textHit;
	public Text m_textMiss;
	public Text m_textMessage;
	public Text m_textModes;
	public Text m_textTries2;
	public Text m_textBest;
	public Text m_textPerScore;

	public Text m_textModeCount;
	public Text m_textTotalScoreInRapidMode;
	public Text m_textTotalModesPlayed;

	public TextMesh m_textMeshCoin;
	public TextMesh m_textMeshCoinShadow;

	public GAME_STATE m_eState;

	public List<Image> m_listOfButtons;
	public List<Image> m_listOfButtonsResults;

	public GameObject m_objectCharacterRoot;

	public MPGameInstance m_scriptGamesceneInstance;
	public List<MPGameInstance> m_listGamesceneInstance = new List<MPGameInstance>();

	int m_currentHighscore;
	int m_currentLevel;
	public int m_currentScore;
	int m_currentAds;
	int m_coins;
	int m_currentMode;
	int m_currentItem;
	int m_currentAttempts;
	int m_currentCoinAdd;
	int m_gameModeCount;
	public int m_gameModeTries;
	int m_totalScore;
	public int m_currentFidgetObstacle;

	float m_currentIncentifiedAdsTime;
	float m_currentDirectionTime;

	float m_currentAdsTime;
	string m_stringSuffix = "";


	[Header("Navigation")]
	[Space]
	public GameObject m_navigationBar;

	[Header("Shuffle")]
	public Image m_shuffleButtonImage;
	public Sprite m_shuffleSprite;
	public Sprite m_repeatSprite;
	
	public Text m_shuffleButtonText;
	public bool m_shuffle = false;
	public int m_numBeforeShuffle = 3;
	private int m_currNumShuffle = 0;

	public Vector3 m_previousScorePos;
	public Vector3 m_previousShadowScorePos;
	
	[Header("Badges")]
	public Text m_textBadge;
	public Image m_imageBadge;
	public Color[] m_badgeColors;

	public GameObject m_rewardedBadgeObject;
	public Image m_rewardedBadgeImage;

	void Awake()
	{
		instance = this;

		Application.targetFrameRate = 60;

		if (!PlayerPrefs.HasKey ("Coins")) 
		{
			PlayerPrefs.SetInt ("Coins", 0);
		}

//		PlayerPrefs.DeleteAll();

//		PlayerPrefs.DeleteKey("Mode");

		if (!PlayerPrefs.HasKey ("Mode")) 
		{
			PlayerPrefs.SetInt ("Mode", 0);
		}

		if (!PlayerPrefs.HasKey ("Removeads")) 
		{
			PlayerPrefs.SetInt ("Removeads", 0);
		}

		if (!PlayerPrefs.HasKey ("currentItem")) 
		{	
			PlayerPrefs.SetInt ("currentItem", 0);
		}

		if (!PlayerPrefs.HasKey ("currentPurchaseCount")) 
		{	
			PlayerPrefs.SetInt ("currentPurchaseCount", 0);
		}

		if (!PlayerPrefs.HasKey ("attempts")) 
		{
			PlayerPrefs.SetInt ("attempts", 0);
		}

		if (!PlayerPrefs.HasKey ("currentUnlockedBird")) {
			PlayerPrefs.SetInt ("currentUnlockedBird", 0);
		}
		//PlayerPrefs.SetInt ("currentUnlockedBird", 0);

		for (int x = 0; x < 100; x++) 
		{
			if (!PlayerPrefs.HasKey ("item" + x)) 
			{
				if (x == 0) {
					PlayerPrefs.SetInt ("item" + x, 1);
				} else {
					PlayerPrefs.SetInt ("item" + x, 0);
				}
			}

//			PlayerPrefs.SetInt ("birdbought" + x, 0);
//			if (x == 0) {
//				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
//					PlayerPrefs.SetInt ("birdbought" + x, 1);
//				}
//			} else {
//				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
//					PlayerPrefs.SetInt ("birdbought" + x, 0);
//				}
//			}
			//PlayerPrefs.SetInt ("birdbought" + x, 0);
		}

		PlayerPrefs.Save ();

		ZAudioMgr.Instance.enabled = true;
		ZAdsMgr.Instance.enabled = true;
		ZIAPMgr.Instance.enabled = true;
		ZPlatformCenterMgr.Instance.enabled = true;
		ZNotificationMgr.Instance.enabled = true;

//		m_previousScorePos = m_textScore.GetComponent<RectTransform>().anchoredPosition;
//		m_previousShadowScorePos = m_textScoreShadow.GetComponent<RectTransform>().anchoredPosition;

	}

	
	void Start()
	{
		if (!PlayerPrefs.HasKey ("Level")) {
			PlayerPrefs.SetInt ("Level", 1);
		} 
			
		if (PlayerPrefs.GetInt ("Removeads") <= 0) {
			ZAdsMgr.Instance.ShowBanner (BANNER_TYPE.BOTTOM_BANNER);
		}


		if (!m_isInDevelopment) 
		{
			MPModeSelectManager.Instance.InitializeManager (); // Initilaize ModeSelectManger
			MPModeSelectManager.Instance.SetCurrentGame (m_currentMode);
			MPShopManager.Instance.InitializeManager ();
		}
		else {
			m_scriptGamesceneInstance = m_inDevelopmentInstance;
		}

		Application.targetFrameRate = 60;

		ZPlatformCenterMgr.Instance.Login ();

		m_coins = PlayerPrefs.GetInt ("Coins");
		m_textHit.text = m_coins + "/" + MPShopManager.Instance.GetCurrentShopPrice() + m_stringSuffix;
		m_currentItem = PlayerPrefs.GetInt ("currentItem");
		m_currentMode = PlayerPrefs.GetInt ("Mode");
		m_currentAttempts = PlayerPrefs.GetInt ("Attempts");
		m_currentIncentifiedAdsTime = 60;
		m_currentDirectionTime = 0.1f;
		m_gameModeTries = 0;
		m_totalScore = 0;
		m_gameModeCount = 1;

		m_currentCoinAdd = PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);

		if (m_currentCoinAdd <= 0)
			m_currentCoinAdd = 1;

		m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_coins * 1.0f / MPShopManager.Instance.GetCurrentShopPrice ()), 1, 1);

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name;

		m_objectWatchVideo.SetActive (false);


	
		m_eState = GAME_STATE.START;

//		m_previousScorePos = m_textScore.GetComponent<RectTransform>().anchoredPosition;
//		m_previousShadowScorePos = m_textScoreShadow.GetComponent<RectTransform>().anchoredPosition;

		SetupLevel ();
		m_currentAds = 2;

		ZAdsMgr.Instance.RequestInterstitial ();
	
		m_currentAdsTime = 35;
	}
		
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.P)) {
			AddCoins (100);
		}

		if (ZGameMgr.instance.isPause)
			return;
		
		m_currentAdsTime -= Time.deltaTime;
		m_currentIncentifiedAdsTime -= Time.deltaTime;


		if (m_eState == GAME_STATE.IN_GAME) 
		{
			m_scriptGamesceneInstance.Gamescene_Update ();
		}
	}

	public void ButtonDown()
	{
		if (m_eState == GAME_STATE.IN_GAME)
			return;
		
		m_scriptGamesceneInstance.Gamescene_ButtonDown();
		foreach (Image obj in m_listOfButtons) 
		{
			obj.gameObject.SetActive (false);
		}

		m_objectTopBar.SetActive (false);
		m_objectTextTutorial.SetActive (false);


		if (m_textBest.gameObject.activeSelf)
		{
			m_textBest.gameObject.SetActive (false);
			m_textScore.text = "0";
			m_textScoreShadow.text = "0";
			m_objectEarned.SetActive (false);

			m_objectNoInternet.SetActive (false);
			m_textTutorial.gameObject.SetActive (false);
		}
		m_objectWatchVideo.SetActive (false);
		m_eState = GAME_STATE.IN_GAME;

		m_navigationBar.SetActive(false);
	}

	public void ButtonHold()
	{
		m_scriptGamesceneInstance.Gamescene_ButtonHold ();
	}

	public void ButtonUp()
	{
		m_scriptGamesceneInstance.Gamescene_ButtonUp();
	}


	public void Die()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;
		m_scriptGamesceneInstance.m_gameData.UpdateAttempts();

		m_textScore.color = m_colorTextMiss;
		m_objectMiss.SetActive(true);
		m_objectWatchVideo.SetActive(false);


		SetTextMiss();
		m_textMiss.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
		LeanTween.cancel(m_textMiss.gameObject);
		LeanTween.scale(m_textMiss.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3(0.2f, 0.2f, 0.2f), 0.2f).setLoopCount(2).setLoopPingPong().setEase(LeanTweenType.easeInOutCubic);

		m_scriptGamesceneInstance.Gamescene_Die();

		m_textScore.text = m_currentScore + "";
		m_textScoreShadow.text = m_currentScore + "";

		ZCameraMgr.instance.DoShake();
		ZAudioMgr.Instance.PlaySFX(m_audioDie, Random.Range(0.8f, 1.2f));

		m_eState = GAME_STATE.RESULTS;

		PlayerPrefs.SetInt("Coins", m_coins);
		m_textHit.text = m_coins + "/" + MPShopManager.Instance.GetCurrentShopPrice() + m_stringSuffix;
		m_currentLevel = m_currentScore;

		m_totalScore += m_currentScore;

		ZAnalytics.Instance.SendEarnCoinsEvent("mode" + m_currentMode, m_currentLevel - m_currentScore);

		m_currentAds++;

#if UNITY_ANDROID
		ZPlatformCenterMgr.Instance.PostScore(PlayerPrefs.GetInt(ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
#else
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
#endif

		m_textTries2.text = "";
		ZAnalytics.Instance.SendLevelFail("mode" + m_currentMode, m_currentLevel, m_currentScore);
		m_gameModeTries++;


		float waitTime = 1.0f;

		if (m_scriptGamesceneInstance.m_gameData.GetHighScoreStatus())
		{
			if (m_scriptGamesceneInstance.m_gameData.GetBadgeStatus())
			{
				m_rewardedBadgeImage.color = m_badgeColors[m_scriptGamesceneInstance.m_gameData.GetBadge()];
				m_rewardedBadgeObject.SetActive(true);
			}
			waitTime = 3.0f;
		}

		if (m_shuffle)
		{
			m_currNumShuffle++;

			if (m_currNumShuffle >= m_numBeforeShuffle)
			{
				m_currNumShuffle = 0;
				LeanTween.delayedCall(waitTime, EnterRandomModeTransition);
			}
			else
			{
				LeanTween.delayedCall(waitTime, DieRoutine);
			}

			m_textModeCount.text = m_currNumShuffle + "/" + m_numBeforeShuffle;
		}
		else LeanTween.delayedCall(waitTime, DieRoutine);
	}

	private void EnterRandomModeTransition()
	{
		m_objUiModeTransition.transform.localPosition = new Vector3 (1000, 0, 0);
		LeanTween.cancel (m_objUiModeTransition);
		LeanTween.moveLocalX (m_objUiModeTransition, 0, 0.5f).setOnComplete (LoadRandomMode).setEase(LeanTweenType.easeInOutSine);
	}


	private void EnterRewardTransition()
	{
		m_objUiModeTransition.transform.localPosition = new Vector3(1000, 0, 0);
		LeanTween.cancel(m_objUiModeTransition);
		LeanTween.moveLocalX(m_objUiModeTransition, 0, 0.5f).setOnComplete(LoadRandomMode).setEase(LeanTweenType.easeInOutSine);
	}


	private void LoadRandomMode()
	{
		m_scriptGamesceneInstance.Gamescene_UnloadLevel ();
		LeanTween.cancel (m_objUiModeTransition.gameObject);
		LeanTween.scale (m_objUiModeTransition.gameObject, m_objUiModeTransition.transform.localScale + new Vector3 (0.5f, 0.5f, 0.5f), 0.5f).setLoopPingPong ().setLoopCount(2).setOnComplete(ExitRandomModeTransition);
		MPModeSelectManager.Instance.LoadRandomGame ();
		m_textTotalScoreInRapidMode.text = "TOTAL SCORE: " + m_totalScore + "/ 100";
		m_textTotalModesPlayed.text = "LOADING NEXT CHALLENGE " + m_gameModeCount + "/10";
		SetupLevel ();
		//DieRoutine ();
	}

	private void ExitRandomModeTransition()
	{
		LeanTween.cancel (m_objUiModeTransition);
		LeanTween.moveLocalX (m_objUiModeTransition, -1000, 0.5f).setEase(LeanTweenType.easeInOutSine);
	}

	private void SetTextMiss()
	{
		int chatvalue = Random.Range (0, 15);
		switch (chatvalue) {
		case 0 : 
			m_textMiss.text = "Miss!";
			break;
		case 1 : 
			m_textMiss.text = "Woops!";
			break;
		case 2 : 
			m_textMiss.text = "Oh no!";
			break;
		case 3 : 
			m_textMiss.text = "No!";
			break;
		case 4 : 
			m_textMiss.text = "Argh!";
			break;
		case 5 : 
			m_textMiss.text = "Nooo!";
			break;
		case 6 : 
			m_textMiss.text = "Ow!";
			break;
		case 7:
			m_textMiss.text = "Too Bad!";
			break;
		case 8:
			m_textMiss.text = "D'oh!";
			break;
		case 9:
			m_textMiss.text = "Ouch!";
			break;
		case 10:
			m_textMiss.text = "Aww!";
			break;
		case 11:
			m_textMiss.text = "Almost!";
			break;
		case 12:
			m_textMiss.text = "Bam!";
			break;
		case 13:
			m_textMiss.text = "Pow!";
			break;
		default : 
			m_textMiss.text = "Miss!";
			break;
		}
	}

	void DieRoutine()
	{
		LeanTween.moveLocal(m_textScore.gameObject, m_textScore.gameObject.transform.localPosition + new Vector3(84f, -9f, 0), 0.1f);
		LeanTween.scale (m_textScore.gameObject, new Vector3 (1.2f, 1.2f, 0.8f), 0.1f);
		ZAudioMgr.Instance.PlaySFX(m_audioButton);
		LeanTween.delayedCall (1f, ShowResultsScreen);
	}

	public int GetCurrentCoinAdd()
	{
		return m_currentCoinAdd;
	}
		
	void ShowUpgradeCoin()
	{
		m_textPerScore.gameObject.SetActive (true);
		m_textPerScore.text = "+" + GetCurrentCoinAdd () + "/SCORE";

		Vector3 currentPosition = m_textPerScore.gameObject.transform.localPosition;
		m_textPerScore.gameObject.transform.localPosition += new Vector3 (0, 20, 0);
		LeanTween.moveLocal (m_textPerScore.gameObject, currentPosition, 0.2f);

		m_currentCoinAdd = PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
		if (m_currentCoinAdd <= 0)
			m_currentCoinAdd = 1;

		LeanTween.delayedCall (1f, ShowUpgradeCoin2);

		ZAudioMgr.Instance.PlaySFX(m_audioButton);
	}

	void ShowUpgradeCoin2()
	{
		m_textPerScore.text = "+" + GetBestScore () + "/SCORE";

		LeanTween.cancel (m_textPerScore.gameObject);
		m_textPerScore.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
		LeanTween.scale (m_textPerScore.gameObject, m_textPerScore.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);


		LeanTween.delayedCall (2f, SetupLevel);

		ZAudioMgr.Instance.PlaySFX(m_audioReward);
	}

	void ShowResultsScreen()
	{
		if (m_coins >= MPShopManager.Instance.GetCurrentShopPrice() && PlayerPrefs.GetInt ("currentPurchaseCount") < m_listShopBirds.Count-1) 
		{
			ZAudioMgr.Instance.PlaySFX(m_audioReward);
			m_objectReadyUnlock.SetActive (true);
			LeanTween.scale (m_objectReadyUnlock, m_objectReadyUnlock.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase (LeanTweenType.easeInOutCubic);

			LeanTween.delayedCall (2f, ShowUnlock);
			Debug.Log ("Results : Show Unlock");
		} else if (m_currentLevel > 3 && m_currentAds > 7 && m_currentAdsTime <= 0 && ZAdsMgr.Instance.IsInterstitialAvailable ()) {
			m_currentAds = 0;
			ZAdsMgr.Instance.ShowInsterstitial ();
			m_currentAdsTime = 50;
			LeanTween.delayedCall (1f, SetupLevel);
			Debug.Log ("Results : Show Interestitial");
		} else if (m_currentLevel > 3 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			ZRewardsMgr.instance.ShowRewardSuccess ();
			ZAudioMgr.Instance.PlaySFX(m_audioReward);
			LeanTween.delayedCall (2.5f, SetupLevel);
			Debug.Log ("Results : Show Reward");
		} else {
			SetupLevel ();
			Debug.Log ("Results : Setup Level");
		}
	}

	void ShowResultsScrenButtons()
	{
		Vector3 currentPosition;
		foreach (Image buttonResults in m_listOfButtonsResults) {
			buttonResults.gameObject.SetActive (true);
			currentPosition = buttonResults.gameObject.transform.localPosition;
			buttonResults.gameObject.transform.localPosition -= new Vector3 (0, 50, 0);
			LeanTween.moveLocal (buttonResults.gameObject, currentPosition, 0.3f).setEase(LeanTweenType.easeOutCubic);
		}
	}

	void ShowContinueRoutine(){
		ShowRewardedAdPopup ("Continue?", "Watch an ad to continue!");
		m_currentIncentifiedAdsTime = 120;
	}

	void ShowShopAvailable()
	{
		m_objectReadyUnlock.SetActive (true);
		LeanTween.scale (m_objectReadyUnlock, m_objectReadyUnlock.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
		LeanTween.delayedCall (1.505f, SetupLevel);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ShowReward()
	{
		LeanTween.delayedCall (2f, SetupLevel);
		ZRewardsMgr.instance.ShowReward ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void Score(int p_score, bool m_isBonus = false)
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;
		
		m_currentScore += p_score;
 		m_textScore.text = "" + m_currentScore;
		m_textScoreShadow.text = "" + m_currentScore;
		m_currentLevel = m_currentScore;

		m_scriptGamesceneInstance.m_gameData.UpdateScore (m_currentScore);
		m_scriptGamesceneInstance.Gamescene_Score ();

		if (!m_isBonus) 
		{
			ZAudioMgr.Instance.PlaySFX (m_audioScore);
		} 
		else 
		{
			ZAudioMgr.Instance.PlaySFX (m_audioBonusScore);
		}
		
		m_textScore.text = m_currentScore + "";
		m_textScoreShadow.text = m_currentScore + "";
	}

	void Success()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		m_currentAds++;

		#if UNITY_ANDROID
		ZPlatformCenterMgr.Instance.PostScore(PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);

		#else

		ZPlatformCenterMgr.Instance.PostScore(PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
		#endif
	}

	public void ShowAdPopup(string title, string message)
	{	
		if (ZAdsMgr.Instance.removeAds > 0)
			return;

		if (!Advertisement.IsReady ("rewardedVideo"))
			return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowAdPopUpClose;
	}
	private void OnShowAdPopUpClose(MNDialogResult result) {
		if (result == MNDialogResult.YES) {
			Advertisement.Show ("rewardedVideo");
			//m_objectTulong.SetActive (true);
			//m_currentAds = 0;
		} else {
			//m_currentAds = 2;
		}
	}

	public void ShowRewardedVideo()
	{
		ShowOptions options = new ShowOptions();
		options.resultCallback = AdCallbackhanler;
		Advertisement.Show ("rewardedVideo", options);

		m_objectWatchVideo.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void ShowRewardedAdPopup(string title, string message)
	{	
		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowRewardedAdPopUpClose;
	}
	private void OnShowRewardedAdPopUpClose(MNDialogResult result) 
	{
		if (result == MNDialogResult.YES) 
		{
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhanler;
			Advertisement.Show ("rewardedVideo", options);
		} else 
		{
			LeanTween.delayedCall (1f, SetupLevel);
		}
	}

	void AdCallbackhanler(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
				ContinueLevel ();
			break;
		case ShowResult.Skipped:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		case ShowResult.Failed:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		}
	}

	void SuccessRoutine()
	{
		//m_objectSuccess.SetActive (false);
		m_textLevel.text = "Level";
		m_textScore.text = "" + m_currentLevel;
		LeanTween.delayedCall (1f, SuccessRoutine2);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void SuccessRoutine2()
	{
		m_textScore.text = "" + (m_currentLevel+1);
		LeanTween.delayedCall (1.5f, SuccessRoutine3);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		if (m_currentLevel > 1 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			ZRewardsMgr.instance.ShowRewardSuccess ();
		}
	}

	void SuccessRoutine3()
	{
		m_objectSuccess.SetActive (false);
		m_currentLevel++;
		m_currentScore = 0;

		PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName, m_currentLevel);

		SetupLevel ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public int GetLevel()
	{
		if (m_currentMode == 2 || m_currentMode == 12 || m_currentMode == 14) 
		{
			return m_currentScore;
		} else {
			return m_currentLevel;
		}
	}

	public void Play()
	{	

		m_eState = GAME_STATE.IDLE;
		m_textScore.color = m_colorTextCorrect;

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		
		m_objectTopBar.SetActive(false);
		m_objectEarned.SetActive (false);
		m_objectNoInternet.SetActive (false);

		m_textScore.color = m_colorTextCorrect;
		m_eState = GAME_STATE.IDLE;

	}

	public void ChangeMode(int p_modeSelected)
	{
		m_scriptGamesceneInstance = MPModeSelectManager.Instance.m_currentMode;

		m_currentMode = p_modeSelected;
		
		if (m_currentMode >= m_listGamesceneInstance.Count) {
			m_currentMode = 0;
		} else if (m_currentMode < 0)
		{
			m_currentMode = m_listGamesceneInstance.Count - 1;
		}

		m_currentScore = 0;
		ZAnalytics.Instance.SendModesButton ();

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name;

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		m_eState = GAME_STATE.IDLE;

		Debug.Log("Current Mode: " + m_currentMode);
	}

	public void NextMode()
	{
		MPModeSelectManager.Instance.NextGame();
		// ChangeMode(m_currentMode + 1);
	}

	public void PrevMode()
	{
		MPModeSelectManager.Instance.PrevGame();
		// ChangeMode(m_currentMode - 1);
	}

	public void ToggleShuffle()
	{
		if (m_shuffle)
		{
			if (m_numBeforeShuffle == 0)
			{
				m_numBeforeShuffle = 3;
				m_shuffleButtonText.text = "Shuffle 3";
				m_textModeCount.gameObject.SetActive(true);
				m_shuffleButtonImage.sprite = m_shuffleSprite;
			}
			else
			{
				m_shuffle = !m_shuffle;
				m_currNumShuffle = 0;

				m_shuffleButtonText.text = "Repeat";
				m_textModeCount.gameObject.SetActive(false);
				m_shuffleButtonImage.sprite = m_repeatSprite;
			}
		}
		else
		{
			m_shuffle = !m_shuffle;
			m_numBeforeShuffle = 0;

			m_shuffleButtonText.text = "Shuffle";
			m_textModeCount.gameObject.SetActive(false);
			m_shuffleButtonImage.sprite = m_shuffleSprite;
		}

	}

	public void ChangeMode()
	{
		m_currentMode++;
		if (m_currentMode >= m_listGamesceneInstance.Count) 
		{
			m_currentMode = 0;
		}

		int gamesceneCount = 0;
		foreach (MPGameInstance instance in m_listGamesceneInstance) {
			if (gamesceneCount != m_currentMode) {
				instance.gameObject.SetActive (false);
			} else {
				instance.gameObject.SetActive (true);
			}
			gamesceneCount++;
		}
		m_listGamesceneInstance [m_currentMode].gameObject.SetActive (true);


		m_currentScore = 0;
		ZAnalytics.Instance.SendModesButton ();

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name;// + " MODE";
		//m_textModes2.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name;// + " MODE";

		

		PlayerPrefs.SetInt ("Mode", m_currentMode);

		m_scriptGamesceneInstance.Gamescene_UnloadLevel ();
		m_scriptGamesceneInstance = m_listGamesceneInstance [m_currentMode];


		SetupLevel ();
		m_scriptGamesceneInstance.Gamescene_SetupLevel ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void RemoveAds()
	{
		ZIAPMgr.Instance.PurchaseRemoveAds ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendNoAdsButton ();
	}

	public void FollowUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public string ScreenshotName = "archereagle_screenshot.png";
	string m_screenshotText;
	string m_screenshotPath;

	public void ShareScreenshotWithText(string text)
	{
		m_screenshotText = text;
		m_screenshotPath = Application.persistentDataPath + "/" + ScreenshotName;
		ScreenCapture.CaptureScreenshot(ScreenshotName);

		//LeanTween.delayedCall(1f,ShareScreenshotRoutine);
		StartCoroutine("ScreenshotWriteCheck");
	}

	IEnumerator ScreenshotWriteCheck()
	{
		while (true == true) {
			if (System.IO.File.Exists(m_screenshotPath)) {
				ShareScreenshotRoutine ();
				yield return null;
			} else {
				yield return new WaitForSeconds (0.5f);
			}
		}
	}

	void ShareScreenshotRoutine ()
	{
		#if UNITY_ANDROID
		StartCoroutine(AndroidShare (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG));
		#else
		Share (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG);
		#endif 
		StopCoroutine("ScreenshotWriteCheck");

	}

	private IEnumerator AndroidShare(string shareText, string imagePath, string url, string subject = "")
	{
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);

		return null;
	}

	public void Share(string shareText, string imagePath, string url, string subject = "")
	{
		#if UNITY_ANDROID
		/*AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");

		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);*/
		#elif UNITY_IOS
		CallSocialShareAdvanced(shareText, subject, url, imagePath);
		#else
		Debug.Log("No sharing set up for this platform.");
		#endif
	}

	#if UNITY_IOS
	public struct ConfigStruct
	{
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}

	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}

	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
	}
	#endif

	public void SharePhoto()
	{
		//ShareScreenshotWithText ("");
		//ZFollowUsMgr.Instance.ShareScreenshotWithText ();
		#if UNITY_ANDROID
		if (UniAndroidPermission.IsPermitted (AndroidPermission.WRITE_EXTERNAL_STORAGE)) {
			ZFollowUsMgr.Instance.Share ();
			ZAnalytics.Instance.SendShareButton ();
		} else {
			UniAndroidPermission.RequestPremission (AndroidPermission.WRITE_EXTERNAL_STORAGE, () => {
				ZFollowUsMgr.Instance.Share ();
				ZAnalytics.Instance.SendShareButton ();
			}, () => {
			// add not permit action
				ZFollowUsMgr.Instance.Share ();
				ZAnalytics.Instance.SendShareButton ();
			});
		}
		#else
		ZFollowUsMgr.Instance.Share ();
		ZAnalytics.Instance.SendShareButton ();
		#endif
	}

	public void OpenLeaderboard()
	{
		#if UNITY_ANDROID
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.LEADERBOARD_ANDROID);
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAg");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAw");
		}*/
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
		#else
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		#endif // UNITY_ANDROID
	
	}

	public void LikeUs()
	{
		//ZPlatformCenterMgr.Instance.Login ();
		//return;
		//ShareScreenshotWithText ("HELLO");
		//ZPlatformCenterMgr.Instance.PostScore(10);
		//return;

		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public GameObject m_objectShop;


	public void ShowModeSelect()
	{
		MPModeSelectManager.Instance.ShowModeSelectScreen (true);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		m_eState = GAME_STATE.SHOP;
	}

	public void ShowShop(bool p_isUnlock = false)
	{
		LeanTween.cancel (ZCameraMgr.instance.gameObject);
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = 60;
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().transform.localPosition = new Vector3 (-48f, 9.94f, -254);

		m_rootUIScore.gameObject.SetActive (false);
		m_rootMain.gameObject.SetActive (false);

		m_objectShop.gameObject.SetActive (true);
		MPShopManager.Instance.ShowShopScreen ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		ZAnalytics.Instance.SendShopButton ();

		m_eState = GAME_STATE.SHOP;
	}

	public void RateUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.RATEUS_URL_ANDROID);
		#else
		MNIOSNative.RedirectToAppStoreRatingPage(ZGameMgr.instance.RATEUS_URL_IOS);
		#endif

		ZAnalytics.Instance.SendRateUsButton ();
	}

	public void BuyCoins()
	{
		ZIAPMgr.Instance.PurchaseCoins ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);


	}

	public void Resume(){
		ZGameMgr.instance.Resume ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void AddCoins(int index, bool playSound=false)
	{
		m_coins += index;
		PlayerPrefs.SetInt ("Coins", m_coins);

		m_textHit.text = m_coins + "/" + MPShopManager.Instance.GetCurrentShopPrice () + m_stringSuffix;

		ZAudioMgr.Instance.PlaySFX (m_audioCoin);

		if (index <= 0) 
		{
			m_textMeshCoin.text = "";
			m_textMeshCoinShadow.text = "";
		} 
		else 
		{
			m_textMeshCoin.text = "+" + index;
			m_textMeshCoinShadow.text = "+" + index;
		}

		float xScale = Mathf.Min(1,m_coins * 1.0f / MPShopManager.Instance.GetCurrentShopPrice());

		if (index > 5) {
			m_objectFullBar.GetComponent<Image> ().color = m_colorFullBarGain;
			LeanTween.scale (m_objectFullBar.gameObject, new Vector3 (xScale, 1, 1), 0.6f).setEase (LeanTweenType.easeInCubic).setOnComplete (AddCoinPlayAudioFull);
			ZAudioMgr.Instance.PlaySFX (m_audioAddCoinGain);
		} else {
			m_objectFullBar.gameObject.transform.localScale = new Vector3 (xScale, 1, 1);
			m_objectFullBar.GetComponent<Image> ().color = m_colorFullBarGain;
			LeanTween.delayedCall (0.2f, AddCoinRoutine3);
		}
	}

	public Color m_colorFullBarYellow;
	public Color m_colorFullBarGain;
	public Color m_colorFullBarFull;

	public AudioClip m_audioAddCoinGain;
	public AudioClip m_audioAddCoinFull;

	void AddCoinPlayAudioFull()
	{
		ZAudioMgr.Instance.PlaySFX (m_audioAddCoinFull);
		if (GetCurrentCoins() >= MPShopManager.Instance.GetCurrentShopPrice ()) 
		{
			m_objectFullBar.GetComponent<Image>().color = m_colorFullBarFull;
		} 
		else 
		{
			m_objectFullBar.GetComponent<Image>().color = m_colorFullBarYellow;
		}
	}

	void AddCoinRoutine3()
	{
		if (GetCurrentCoins() >= MPShopManager.Instance.GetCurrentShopPrice ()) {
			m_objectFullBar.GetComponent<Image>().color = m_colorFullBarFull;
		} else {
			m_objectFullBar.GetComponent<Image>().color = m_colorFullBarYellow;
		}
	}

	public Color m_colorCOinYellow;

	public void SpendCoins(int index)
	{
		m_coins -= index;
		PlayerPrefs.SetInt ("Coins", m_coins);

		//m_currentPurchaseCount++;

		m_textHit.text = m_coins + "/" +  MPShopManager.Instance.GetCurrentShopPrice () + m_stringSuffix;
		ZAudioMgr.Instance.PlaySFX (m_audioCoin);


		m_textMeshCoin.text = "+" + index;
		m_textMeshCoinShadow.text = "+" + index;

		m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_coins * 1.0f /  MPShopManager.Instance.GetCurrentShopPrice ()), 1, 1);
		m_objectFullBar.GetComponent<Image> ().color = m_colorCOinYellow;
	}

	public void ResetCoins()
	{
		m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_coins * 1.0f /  MPShopManager.Instance.GetCurrentShopPrice ()), 1, 1);
	}

	void AddCoinRoutine()
	{
		ZRewardsMgr.instance.ShowShopButton (true);
		LeanTween.delayedCall (1f, ShowShopAvailable);
	}

	public void RestoreAds()
	{
		ZIAPMgr.Instance.RestorePurchases ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendRestoreIAPButton ();
	}

	public void MoreGames()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendMoreGamesButton ();
	}
	
	public int GetCurrentSelectedItem()
	{
		return m_currentItem;
	}

	public void SetItem(int count)
	{
		if (PlayerPrefs.GetInt ("item" + count) > 0)
		{
			m_currentItem = count;
			PlayerPrefs.SetInt ("currentItem", m_currentItem);

			m_scriptGamesceneInstance.Gamescene_SetCharacter (count);
			MPShopManager.Instance.SelectItem (m_currentItem);

			ZAudioMgr.Instance.PlaySFX (m_audioButton);
			ZAnalytics.Instance.SendUseChar (count);

			m_eState = GAME_STATE.IDLE;
		} 
	}

	public void ShowUnlock()
	{
		MPUnlockManager.Instance.SetupScene ();
		ZAudioMgr.Instance.PlaySFX (m_audioThrow);

		m_eState = GAME_STATE.UNLOCK;
	}

	public int GetCurrentCoins()
	{
		return m_coins;
	}

//	public void BuyItem(int p_count)
//	{
//		MPShopManager.Instance.BuyItem (p_count);
//		SetItem (p_count);
//		m_eState = GAME_STATE.SHOP;
//	}

	public int GetBestScore()
	{
		return PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName);
	}

	public int GetScore()
	{
		return m_currentScore;
	}
	
	void ShowTutorialRoutine()
	{
		if (m_eState != GAME_STATE.IDLE)
			return;

		m_objectTextTutorial.SetActive (true);
		Vector3 currentPosition;
		currentPosition = m_objectTextTutorial.gameObject.transform.localPosition;
		m_objectTextTutorial.gameObject.transform.localPosition -= new Vector3 (0, 30, 0);
		LeanTween.moveLocal (m_objectTextTutorial.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic);
	}


	public void LoadLevelPreview()
	{
		m_scriptGamesceneInstance = MPModeSelectManager.Instance.m_currentMode;
		m_scriptGamesceneInstance.Gamescene_SetupLevel ();

//		m_textTutorial.text 	= "" + m_scriptGamesceneInstance.m_gameData.Tutorial;
//		m_textTitleShadow.text 	= "" + m_scriptGamesceneInstance.m_gameData.Name;
//		m_textTitle.text 		= "" + m_scriptGamesceneInstance.m_gameData.Name;
//		m_textTitle.color 		= m_scriptGamesceneInstance.m_gameData.ColorTheme;

//		m_objectTopBar.SetActive (true);
		m_objectTopBar.GetComponent<Image> ().sprite = m_scriptGamesceneInstance.m_gameData.TitleArtwork;

	}

	public void SetupLevel()
	{
		if (!m_isInDevelopment) 
		{
			m_scriptGamesceneInstance = MPModeSelectManager.Instance.m_currentMode;
		} 
		else 
		{
			m_scriptGamesceneInstance = m_inDevelopmentInstance;
		}

		if (!m_scriptGamesceneInstance.gameObject.activeSelf)
		{
			m_scriptGamesceneInstance.gameObject.SetActive (true);
		}

		ZObjectMgr.Instance.ResetAll ();
		MPModeSelectManager.Instance.ResetButtons();

		m_scriptGamesceneInstance.Gamescene_SetupLevel ();
		m_scriptGamesceneInstance.Gamescene_SetCharacter (m_currentItem);

		m_currentScore = 0;
		m_textLevel.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		m_textHit.gameObject.SetActive (true);
		m_objectReadyUnlock.SetActive (false);
		m_objectTextTutorial.SetActive (false);

		m_textBest.gameObject.SetActive (true);
		m_textBest.text = "BEST";


		foreach (Image buttonResults in m_listOfButtonsResults) 
		{
			buttonResults.gameObject.SetActive (false);
		}

		/*
		Vector3 currentPosition;
		int buttonIndex = 0;
		foreach (Image obj in m_listOfButtons) 
		{
			obj.gameObject.SetActive (true);
			if (buttonIndex != 3) 
			{
				currentPosition = obj.gameObject.transform.localPosition;
				obj.gameObject.transform.localPosition -= new Vector3 (0, 50, 0);
				LeanTween.moveLocal (obj.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic);
			}
			buttonIndex++;
			m_textTutorial.gameObject.SetActive (true);
		}
		*/
		LeanTween.delayedCall (0.3f, ShowTutorialRoutine);

		m_textPerScore.text = "+" + GetCurrentCoinAdd () + "/SCORE";

		m_textTutorial.text 	= "" + m_scriptGamesceneInstance.m_gameData.Tutorial;
		m_textTitleShadow.text 	= "" + m_scriptGamesceneInstance.m_gameData.Name;
		m_textTitle.text 		= "" + m_scriptGamesceneInstance.m_gameData.Name;
		m_textTitle.color 		= m_scriptGamesceneInstance.m_gameData.ColorTheme;

		m_objectTopBar.SetActive (true);
		m_objectTopBar.GetComponent<Image> ().sprite = m_scriptGamesceneInstance.m_gameData.TitleArtwork;

	
		m_textTries2.text = "TRIES " + (m_scriptGamesceneInstance.m_gameData.GetAttempts() + 1);

		m_textScore.gameObject.SetActive (true);
		m_textScore.color = m_colorTextSuccess;
		m_textScore.GetComponent<RectTransform>().anchoredPosition = m_previousScorePos;
		m_textScore.gameObject.transform.localScale = new Vector3 (0.7f, 0.7f, 0.7f);
		m_textScore.text =  "" + m_scriptGamesceneInstance.m_gameData.GetBestScore();

		m_textScoreShadow.gameObject.SetActive (true);
		m_textScoreShadow.GetComponent<RectTransform>().anchoredPosition = m_previousShadowScorePos;
		m_textScoreShadow.text =  "" + m_scriptGamesceneInstance.m_gameData.GetBestScore();

		m_scriptGamesceneInstance.m_gameData.UpdateBadge();
		UpdateBadgeUI();

		m_scriptGamesceneInstance.m_gameData.RestartStatus();

		m_rewardedBadgeObject.SetActive(false);

		m_eState = GAME_STATE.IDLE;

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		if (m_gameModeTries == 0) 
		{
//			m_textModeCount.text = "TRIES [ ] [ ] [ ]";
		}

		ZAnalytics.Instance.SendLevelStart ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);
		PlayerPrefs.Save ();

		m_navigationBar.SetActive(true);
	}
	
	void UpdateBadgeUI()
	{
		m_textScoreShadow.color = m_badgeColors[m_scriptGamesceneInstance.m_gameData.GetBadge()];
		m_textBest.color = m_badgeColors[m_scriptGamesceneInstance.m_gameData.GetBadge()];

		if (m_scriptGamesceneInstance.m_gameData.GetBadge() < 3)
		{
			m_textBadge.gameObject.SetActive(true);
			m_textBadge.text = m_scriptGamesceneInstance.m_gameData.GetPointsToNextBadge() + " TO";
			m_imageBadge.color = m_badgeColors[m_scriptGamesceneInstance.m_gameData.GetBadge() + 1];
		}
		else
		{
			m_textBadge.gameObject.SetActive(false);
		}
	}

	void ContinueLevel()
	{
		ZObjectMgr.Instance.ResetAll ();

		m_listButtons [0].SetActive (true);
		m_textLevel.text = "Continue";
		m_textScore.text = "" + m_currentScore;

		m_textLevel.color = m_colorTextSuccess;
		m_textScore.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		m_textHit.gameObject.SetActive (true);

		m_objectTarget.SetActive (false);

		m_objectTutorial.SetActive (true);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		ZAnalytics.Instance.SendSkipComplete ();

		m_eState = GAME_STATE.START;
	}
}
