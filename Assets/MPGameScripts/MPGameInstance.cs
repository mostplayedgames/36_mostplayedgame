﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MPGameInstanceData
{
	[Header("GENERAL DATA")]
	public string Name;
	public int ID;
	public string LeaderboardID_Googleplay;
	public string LeaderboardID_iOS;
	public string Tutorial;

	[Space]
	[Header("GAME CHARACTER DATA")]
	public GameObject ParentCharacter; 		// Parent that holds the characters of the game

	[Space]
	[Header("ARTWORK")]
	public Sprite TitleArtwork;
	public Sprite ButtonArtwork;
	public Color ColorTheme;

/*	private int BestScore;
	private int Attempts;
	private int Currency;
	*/

	public int[] BadgeScores = { 0, 10, 50, 100 };
	public int CurrentBadge;
	public bool CurrentBadgeSet = false;

	public bool NewHighScore;
	public bool NewBadge;

	public int GetBadge()
	{
		if (PlayerPrefs.HasKey("Badge_" + ID))
		{
			int retVal = PlayerPrefs.GetInt("Badge_" + ID, 0);
			return retVal;
		}
		else
		{
			int score = PlayerPrefs.GetInt("Score_" + ID);

			for (int iter = BadgeScores.Length - 1; iter >= 0; iter--)
			{
				if (score >= BadgeScores[iter])
				{
					PlayerPrefs.SetInt("Badge_" + ID, iter);
					return iter;
				}
			}

			return 0;
		}

	}

	public int GetPointsToNextBadge()
	{
		if (GetBadge() < BadgeScores.Length - 1)
		{
			return BadgeScores[GetBadge() + 1];
		}
		else return 0;
	}

	public int GetBestScore()
	{
		int retVal = PlayerPrefs.GetInt ("Score_" + ID);
		return retVal;
	}

	public int GetAttempts()
	{
		int retVal = PlayerPrefs.GetInt ("Attempts_" + ID);
		return retVal;
	}

	public void UpdateAttempts()
	{
		int attemptsIdx = GetAttempts();
		attemptsIdx++;
		PlayerPrefs.SetInt ("Attempts_" + ID, attemptsIdx);
	}

	public void UpdateScore(int p_score, bool p_forceUpdate = false)
	{
		int bestScore = GetBestScore();
		if (p_forceUpdate || bestScore < p_score)
		{
			PlayerPrefs.SetInt("Score_" + ID, p_score);
			NewHighScore = true;

			if (PlayerPrefs.GetInt("Badge_" + ID, 0) < BadgeScores.Length - 1)
			{
				if (p_score >= BadgeScores[PlayerPrefs.GetInt("Badge_" + ID, 0) + 1])
				{
					PlayerPrefs.SetInt("Badge_" + ID, PlayerPrefs.GetInt("Badge_" + ID, 0) + 1);
					NewBadge = true;
				}
			}
		}
	}

	public void UpdateBadge()
	{
		if (!CurrentBadgeSet)
		{
			int bestScore = GetBestScore();
			for (int iter = BadgeScores.Length - 1; iter >= 0; iter--)
			{
				if (bestScore >= BadgeScores[iter])
				{
					PlayerPrefs.SetInt("Badge_" + ID, iter);
					CurrentBadgeSet = true;
					return;
				}
			}
		}
	}

	public void RestartStatus()
	{
		if (NewBadge) NewBadge = false;
		if (NewHighScore) NewHighScore = false;
	}

	public bool GetBadgeStatus()
	{
		return NewBadge;
	}

	public bool GetHighScoreStatus()
	{
		return NewHighScore;
	}
}

public class MPGameInstance : MonoBehaviour 
{
	public MPGameInstanceData m_gameData;

	public virtual void Gamescene_SetupLevel(){}
	public virtual void Gamescene_Update(){}
	public virtual void Gamescene_ButtonDown(){}
	public virtual void Gamescene_ButtonUp(){}
	public virtual void Gamescene_ButtonHold(){}
	public virtual void Gamescene_Die(){}
	public virtual void Gamescene_Score(){}
	public virtual void Gamescene_UnloadLevel()
	{
		this.gameObject.SetActive (false);
	}

	// Set Character Parent where items will be loaded
	public void Gamescene_SetCharacter(int p_idx)
	{
		if (GameScene.instance.m_isInDevelopment) {
			p_idx = 0;
		}

		GameObject parentCharacter = m_gameData.ParentCharacter.transform.GetChild(0).gameObject;

		for (int idx = 0; idx < parentCharacter.transform.childCount; idx++) 
		{
			if (parentCharacter.transform.GetChild(idx).GetComponent<MPShopItemData> ().ItemId == p_idx) 
			{
				parentCharacter.transform.GetChild (idx).gameObject.SetActive (true);
			} 
			else 
			{
				parentCharacter.transform.GetChild (idx).gameObject.SetActive (false);
			}
		}
	}
}
