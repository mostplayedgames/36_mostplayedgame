﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
	IDLE,
	CHARGE,
	JUMP,
	LAND
}

public class FDSpringCharacter : MPCharacter 
{
//	public GameObject m_characterParent;
	public GameObject m_objSpring;
	public PlayerState m_curState;

	private int m_scoreToAdd;
	public float m_curPatternIdx			= 0;
	public float m_platformPatternIdx	= 0;
	public bool m_didLandOnNewPlatform;

	public void Initialize()
	{
		this.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
		m_curState = PlayerState.IDLE;
		m_objSpring.gameObject.SetActive (true);
		m_objSpring.transform.localScale = new Vector3 (1, 1, 1);
		m_objCharacter.transform.localPosition = new Vector3 (m_objCharacter.transform.localPosition.x, 0, m_objCharacter.transform.localPosition.z);
		this.transform.localEulerAngles = Vector3.zero;
		this.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		m_didLandOnNewPlatform = false;
		m_curPatternIdx = 0;
		m_platformPatternIdx = 0;
	}

	public void Update()
	{
		if (PlayerState.JUMP == m_curState && m_didLandOnNewPlatform) 
		{
			if (this.GetComponent<Rigidbody2D> ().angularVelocity < 15 &&
			   this.GetComponent<Rigidbody2D> ().angularVelocity > -15) 
			{
				if (this.transform.localEulerAngles.z < 20 ||
				    this.transform.localEulerAngles.z > 340) {
					Land ();
				}
			}
		}
	}

	public bool canCharacterJump()
	{
		return (m_curState == PlayerState.JUMP) ? false : true; 
	}

	public void ChargeJump()
	{
		m_curState = PlayerState.CHARGE;

		LeanTween.cancel (m_objCharacter);
		LeanTween.cancel (m_objSpring);

		LeanTween.scaleY (m_objSpring, 0, 1f);
		LeanTween.moveLocalY (m_objCharacter, -12, 1f);
		LeanTween.rotateAroundLocal (m_objCharacter, new Vector3 (0, 0, 1),25, 0.7f);

	}

	public void ResetSpringState()
	{
		LeanTween.cancel (m_objCharacter);
		LeanTween.cancel (m_objSpring);

		m_objSpring.gameObject.SetActive (true);

		LeanTween.scaleY (m_objSpring, 1, 0.15f);
		LeanTween.moveLocalY (m_objCharacter, 0, 0.15f);
	}

	public void Jump(float p_jumpValue)
	{
		Debug.Log (p_jumpValue);	
		
		m_curState = PlayerState.JUMP;


		LeanTween.cancel (this.gameObject);
		LeanTween.cancel (m_objCharacter);
		LeanTween.cancel (m_objSpring);

		float xVal = 450f + (700f * p_jumpValue);
		float yVal = 3500f + (2000f * p_jumpValue);

		ResetSpringState ();

		this.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;
		m_objCharacter.transform.localPosition = new Vector3 (m_objCharacter.transform.localPosition.x, -12, m_objCharacter.transform.localPosition.z);
		m_objCharacter.transform.localEulerAngles = Vector3.zero;
		this.transform.localEulerAngles = new Vector3 (0,0,27f);

		this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (xVal, yVal));
		this.GetComponent<Rigidbody2D> ().AddTorque (-1500); //1500|250



		//m_ob`jSpring.gameObject.SetActive (false);
	}


	public void OnCollisionEnter2D(Collision2D p_other)
	{
		if (p_other.gameObject.tag == "Platform" && m_curState == PlayerState.JUMP) 
		{

			m_platformPatternIdx = p_other.gameObject.GetComponent<FDPlatform> ().GetPlatformIdx ();

			//Make sure that player is above platform before getting a scrore;
		//	if (this.transform.position.y > p_other.transform.position.y + 10)
			{
				if (m_platformPatternIdx > m_curPatternIdx) 
				{
					//m_curState = PlayerState.LAND;
					m_didLandOnNewPlatform = true;
					this.GetComponent<Rigidbody2D> ().angularVelocity /= 1.5f;
					this.GetComponent<Rigidbody2D> ().velocity /= 1.5f;
				} 
				else 
				{
					//Die ();
				}
			}
		}

		if (p_other.gameObject.tag == "Floor") {

			Die();
		}
	}



	public void Die()
	{
		m_curState = PlayerState.IDLE;
		LeanTween.cancel (this.gameObject);
		this.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
		this.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		//this.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;

		GameScene.instance.Die ();
	}

	private bool CheckIfLandedOnRightPlatform()
	{
		return (m_platformPatternIdx <= m_curPatternIdx + 1) ? true : false;
	}

	private void Land()
	{
		if (m_curState == PlayerState.IDLE)
			return;
		
		ResetSpringState ();
		m_curState = PlayerState.IDLE;
		m_didLandOnNewPlatform = false;
		this.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		this.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
		this.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
		this.transform.localEulerAngles = Vector3.zero;

		GameScene.instance.Score (1);
		m_curPatternIdx++;

	}
}
