﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Object Should not be attached in a prefab

public class MPModeSelectManager : ZSingleton<MPModeSelectManager>
{
	public GameObject m_objectButtonsParent;
	public GameObject m_objRoot;

	public Dictionary<string, MPGameInstance> m_dictGameInstances = new Dictionary<string, MPGameInstance>();
	public MPGameInstance m_currentMode;
	public int m_currentModeIdx;

	public GameObject m_objModesParent;
	public GameObject m_objModesBtnParent;
	public GameObject m_prefabModesBtn;
	public RectTransform m_rectTransform;
	public ScrollRect m_scrollRect;

	// If no data was fetched from the server used the default template instead
	public List<MPModesButton> m_listOfModesButton = new List<MPModesButton>();
	public List<MPGameInstance> m_listOfGameInstance = new List<MPGameInstance>();
	public List<int> m_listOfGamesToLoad = new List<int>();

	public AudioClip m_audioButton;

	public Color m_colorSelected;
	public Color m_colorDefault;

	public void InitializeManager()
	{
		ReadGameInstancesInResources ();
		SetupButtons ();

	}

	public void NextGame ()
	{
		if (m_currentModeIdx + 1 >= m_listOfGameInstance.Count) SetCurrentGame(0);
		else SetCurrentGame(m_currentModeIdx + 1);
//		GameScene.instance.SetupLevel();
//		GameScene.instance.m_eState = GAME_STATE.SHOP;
	}

	public void PrevGame()
	{
		if (m_currentModeIdx - 1 < 0) SetCurrentGame(m_listOfGameInstance.Count - 1);
		else SetCurrentGame(m_currentModeIdx - 1);
//		GameScene.instance.LoadLevelPreview();
//		GameScene.instance.m_eState = GAME_STATE.SHOP;
	}

	public void SetCurrentGame(int p_curMode)
	{
		m_currentMode = m_listOfGameInstance [p_curMode];
		m_currentModeIdx = p_curMode;

		for (int idx = 0; idx < m_listOfGameInstance.Count; idx++) 
		{
			if (m_listOfModesButton [idx].m_gameData.ID != p_curMode) 
			{
				m_listOfModesButton[idx].GetComponent<Image>().color = m_colorDefault; // Color.white;
			} 
			else 
			{
				m_listOfModesButton[idx].GetComponent<Image>().color = m_colorSelected; // Color.black;
			}
			m_listOfGameInstance [idx].gameObject.SetActive (false);
		}
		m_listOfGameInstance [p_curMode].gameObject.SetActive (true);

		//GameScene.instance.m_

		PlayerPrefs.SetInt ("Mode", p_curMode);

		MPShopManager.Instance.m_objItemParent.transform.parent = m_listOfGameInstance [p_curMode].m_gameData.ParentCharacter.transform;
		MPShopManager.Instance.m_objItemParent.transform.localPosition = Vector3.zero;
		MPShopManager.Instance.m_objItemParent.transform.localEulerAngles = new Vector3 (0, 0, 0);
		MPShopManager.Instance.m_objItemParent.transform.localScale = new Vector3 (1, 1, 1);
		MPShopManager.Instance.m_objItemParent.gameObject.SetActive (true);

		GameScene.instance.m_eState = GAME_STATE.IDLE;
		GameScene.instance.SetupLevel();
	}

	public void LoadRandomGame()
	{
		List<MPGameInstance> tempData = m_listOfGameInstance;
		tempData.Remove (m_currentMode);
		int randVal = Random.Range (0, tempData.Count);
		SetCurrentGame (randVal);
	}

	public void ResetButtons()
	{
		foreach (MPModesButton btn in m_listOfModesButton)
		{
			btn.gameObject.SetActive(true);
			btn.RefreshData();
		}
	}

	public void ShowModeSelectScreen(bool p_willShowModeSelect)
	{
		m_objRoot.gameObject.SetActive (p_willShowModeSelect);

		ResetButtons();

		for (int idx = 0; idx < m_listOfGameInstance.Count; idx++) 
		{
			int curMode = PlayerPrefs.GetInt ("Mode");
			if(m_listOfGameInstance[idx].m_gameData.ID == curMode)
			{
				m_currentModeIdx = idx;
			}
		}

		int test = 0;

		int SubtractedVal = m_currentModeIdx - 2;
		int DividedVal = m_currentModeIdx/2;
		int RoundedVal = Mathf.RoundToInt (DividedVal);

		test -= (100 * RoundedVal) + RoundedVal;

//		float yPos = m_listOfModesButton [m_currentModeIdx].transform.localPosition.y;
//		m_rectTransform.anchoredPosition = new Vector2 (25, (Mathf.Abs(-4590)));
//		Debug.Log ("YPO: " +  (Mathf.Abs(test) - 46));
	}

	public void Back()
	{
		m_objRoot.gameObject.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		GameScene.instance.m_rootUIScore.gameObject.SetActive (true);
		GameScene.instance.m_rootMain.gameObject.SetActive (true);

		GameScene.instance.m_eState = GAME_STATE.IDLE;
		// GameScene.instance.SetupLevel ();
	}

	//Loads All MPGameInstance in the resources folder
	private void ReadGameInstancesInResources()
	{
		Object[] GameInstance = Resources.LoadAll ("MPGames", typeof(GameObject));
		foreach (GameObject instance in GameInstance) 
		{
			Debug.Log ("InstanceLoaded: " + instance.name);
			GameObject obj = Instantiate (instance.gameObject, Vector3.zero, Quaternion.identity);
			obj.transform.parent = m_objModesParent.transform;
			m_listOfGameInstance.Add (obj.GetComponent<MPGameInstance>());
		}
			
		uint buttonCount = (uint)m_listOfGameInstance.Count;
		ZObjectMgr.Instance.AddNewObject (m_prefabModesBtn.name, buttonCount, this.gameObject);
	}

	private void AdjustBottomSize()
	{
		int totalCount = m_listOfGameInstance.Count;
		float rectTransformBottom = -100;

		if (totalCount <= 6) 
		{
			rectTransformBottom = 0;
		}
		else
		{
			rectTransformBottom = -200;

			int SubtractedVal = totalCount - 6;
			int DividedVal = SubtractedVal/2;
			int RoundedVal = Mathf.RoundToInt (DividedVal);

			rectTransformBottom -= 100 * RoundedVal;

			m_rectTransform.offsetMin = new Vector2 (m_rectTransform.offsetMin.x, rectTransformBottom);
		}
	}

	// Create buttons for all the available modes
	// Naming Conventions alphabetical order so possible solution is gawing collection nalang para mas may control si user
	private void SetupButtons()
	{
		for (int idx = 0; idx < m_listOfGameInstance.Count; idx++) 
		{
			int id = m_listOfGamesToLoad [idx];

			GameObject objBtn = ZObjectMgr.Instance.Spawn2D (m_prefabModesBtn.name, Vector2.zero);
			MPGameInstanceData instanceData = m_listOfGameInstance [id].m_gameData;

			objBtn.transform.parent = m_objModesBtnParent.transform;
			objBtn.transform.localScale = new Vector3 (1, 1, 1);
			objBtn.GetComponent<MPModesButton>().Initialize (instanceData, idx);
			m_listOfModesButton.Add (objBtn.GetComponent<MPModesButton>());
		}
		AdjustBottomSize ();
	}



}
