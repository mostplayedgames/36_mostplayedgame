﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameInstanceStates
{
	IDLE,
	INGAME,
	GAMEOVER
}

[System.Serializable]
public class StacksLevelDesign
{
	public string DIRECTION;
	public float SPEED;
	public float SPAWN_DELAY;
}

public class SGameInstance : MPGameInstance 
{
	public GameInstanceStates m_curState;
	public SGameCharacter m_mainCharacter;
	public int m_colorIdx			= 0;
	public float m_spawnTime 		= 0;
	public float m_spawnTimeTarget = 0;
	public int m_platformIdx;

	public List <Vector3> m_listEasyPlatforms = new List<Vector3> ();
	public List<Vector3> m_listMedPlatform = new List<Vector3> ();
	public List<Vector3> m_listHighMedPlatform = new List<Vector3>();
	public List<Vector3> m_listHardPlatform1 = new List<Vector3> ();

	public List<GameObject> m_listOfPlatforms = new List<GameObject>();
	public List<int> m_listOfDirections = new List<int> ();

	public List<string> m_listDirections = new List<string>();
	public List<int> m_listOfDifficulty = new List<int>();
	public List<Color> m_listOfColors = new List<Color>();

	private List<int> m_currentCombo;
	public float m_speedster;
	public int m_comboTargetIdx;
	public int m_curPatternSetIdx;
	public int m_curDirection;
	private int m_curDifficulty;
	private int m_curPlatformDesignIdx;
	public int spawnCount;
	private float m_currentY;

	void Awake () 
	{
		ZObjectMgr.Instance.AddNewObject (m_listOfPlatforms [0].name, 50, this.gameObject);

		m_curDirection = Random.Range (0, m_listDirections.Count);
		m_curDifficulty = Random.Range (0, m_listOfDifficulty.Count);

		m_colorIdx = Random.Range(0, m_listOfColors.Count);
		Camera.main.backgroundColor = m_listOfColors [m_colorIdx];
		m_curState = GameInstanceStates.IDLE;
	}


	public override void Gamescene_SetupLevel ()
	{
		if (m_gameData.GetBestScore () > 20) 
		{
			m_curPlatformDesignIdx = Random.Range (0, m_listEasyPlatforms.Count);
		} 
		else 
		{
			m_curPlatformDesignIdx = Random.Range (0, 10);
		}
		m_spawnTime = 0;
		m_curPatternSetIdx = 0;
		m_comboTargetIdx = -1;
		m_speed = 0;
		m_currentY = -5f;
		m_platformIdx = 0;
		m_colorIdx++;
		m_curDifficulty = 0;
		spawnCount = 0;

		// Set Characters 180
		m_mainCharacter.transform.position = new Vector3 (0, 0, 63.8f);
		m_mainCharacter.transform.localEulerAngles = new Vector3 (0, 180, 0);
		m_mainCharacter.Initialize ();

		// Set Camera
		Camera.main.gameObject.GetComponent<Camera> ().orthographicSize = 200;//163;
		Camera.main.gameObject.GetComponent<Camera> ().transform.position = new Vector3 (0, 75, -250);
		Camera.main.gameObject.GetComponent<Camera> ().transform.eulerAngles = new Vector3 (7, 0, 0);

		// Set BG Color
		if (m_colorIdx >= m_listOfColors.Count - 1) 
		{
			m_colorIdx = 0;
		}

		Camera.main.backgroundColor = m_listOfColors [m_colorIdx];
		m_curState = GameInstanceStates.IDLE;
		m_mainCharacter.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
		m_mainCharacter.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		m_mainCharacter.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
		UpdateCameraMovement ();
		GetData ();
	}
	public override void Gamescene_Update ()
	{
		if (m_curState == GameInstanceStates.INGAME) 
		{
			Controls ();
			SpawnLogic ();
		}
	}
	public override void Gamescene_Die (){}
	public override void Gamescene_Score ()
	{
		UpdateCameraMovement ();
		spawnCount--;
		GameScene.instance.AddCoins (1);
	}
	public override void Gamescene_ButtonDown (){}
	public override void Gamescene_ButtonHold (){}
	public override void Gamescene_ButtonUp ()
	{
		if (m_curState == GameInstanceStates.IDLE) 
		{
		//	SpawnPlatform ();
			m_mainCharacter.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;
			m_curState = GameInstanceStates.INGAME;
		}
	}

	private void UpdateCameraMovement()
	{
		LeanTween.cancel (Camera.main.gameObject);
		LeanTween.moveLocalY (Camera.main.gameObject, m_mainCharacter.transform.localPosition.y + 58.0f, 0.3f);
	}

	private void Controls()
	{
		if (!m_mainCharacter.m_isGrounded)
		{
			return;
		}

		if (Input.GetMouseButtonDown (0)) 
		{
			m_mainCharacter.Jump ();
		} 
	}

	private void SpawnLogic()
	{
		if (m_spawnTimeTarget == 0)
			return;
		
		m_spawnTime += Time.deltaTime;
		if (m_spawnTime >= m_spawnTimeTarget) 
		{
			if (spawnCount < 3) 
			{
				SpawnPlatform ();
				GetData ();
			}
		}
	}
		
	public int m_direction;
	public float m_test;
	private float m_speed = 1;

	private void GetData()
	{
		List<Vector3> m_currentVec = m_listEasyPlatforms;
		int score = GameScene.instance.GetScore ();

		if (score < 15) {
			m_currentVec = m_listEasyPlatforms;
			m_test = 1;
		} else if (score < 30) {
			m_test = 2;
			m_currentVec = m_listMedPlatform;
		} else if (score < 50) {
			m_test = 3;
			m_currentVec = m_listHighMedPlatform;
		} else {
			m_test = 4;
			m_currentVec = m_listHardPlatform1;
		}

		Vector3 platform = m_currentVec [m_curPlatformDesignIdx];

		m_direction = (int)platform.x;
		m_speed = platform.y;
		if (m_platformIdx == 0) 
		{
			m_spawnTimeTarget = 0.1f;
		} else {
			m_spawnTimeTarget = platform.z;
		}

		m_curPlatformDesignIdx++;

		if (m_curPlatformDesignIdx >= m_listEasyPlatforms.Count) 
		{
			m_curPlatformDesignIdx = 0;
		}
	}

//	private void GetData()
//	{
//		// Checks if new pattern or an old one
//		if (m_comboTargetIdx <= m_curPatternSetIdx) 
//		{
//			m_curPatternSetIdx = 0;
//
//			float randVal = Random.Range (0, 100);
//
//			if (randVal > 40) {
//				Debug.Log ("NoCombo");
//				m_comboTargetIdx = 0;
//			} 
//			else 
//			{
//				Debug.Log ("Combo");
//				m_currentCombo = GetComboDirection ();
//				m_comboTargetIdx = m_currentCombo.Count;
//				m_speed = m_listOfDifficulty [m_curDirection];
//			}
//			m_curPatternSetIdx = 0;
//		} 
//		else 
//		{
//			m_curDirection++;
//
//			if (m_curDirection > m_listOfDirections.Count - 1) 
//			{
//				m_curDirection = 0;
//			}
//
//			if (m_comboTargetIdx > 0) 
//			{
//				m_direction =  m_currentCombo[m_curPatternSetIdx];
//				m_spawnTimeTarget = 0.8f;
//			} 
//			else 
//			{
//				m_direction = m_listOfDirections [m_curDirection];
//				m_speed = m_listOfDifficulty [m_curDirection];
//				m_spawnTimeTarget = 1;
//			}
//			m_curPatternSetIdx++;
//		}
//	}

	private void SpawnPlatform()
	{
//		m_curDirection++;
//		if (m_curDirection > m_listDirections.Count - 1) 
//		{
//			m_curDirection = 0;
//		}
//
		GameObject platform = ZObjectMgr.Instance.Spawn2D (m_listOfPlatforms [0].name, Vector2.zero);
//		m_direction = GetSpawnDirection();	
//
//		//float speed = 1;
//		m_spawnTimeTarget = GetDelayTime();
//
//		if (m_comboTargetIdx > 0) 
//		{
//			Debug.Log ("Delay: " + m_spawnTimeTarget + "_ Speed: " + m_speed);
//			//m_spawnTimeTarget -= 0.2f;
//
//			int score = GameScene.instance.GetScore ();
//			float randVal = Random.Range (0, 100);
//			if (score < 20) {// 0.9f/0.8f;
//				if (randVal < 70) {
//					m_spawnTimeTarget = 0.9f;
//				} else {
//					m_spawnTimeTarget = 0.8f;
//				}
//			} else{
//				if (randVal < 70) {
//					m_spawnTimeTarget = 0.75f;
//				} else {
//					m_spawnTimeTarget = 0.65f;
//				}
//			}
//		}
//
//		// Check if there is a combo to follow
//		if (m_comboTargetIdx <= 0) 
//		{
//			m_curDifficulty++;
//			if (m_curDifficulty > m_listOfDifficulty.Count - 1) 
//			{
//				m_curDifficulty = 0;
//			}
//
//			m_speed = GetDifficulty (m_curDifficulty);
//			m_speedster = m_speed;
//
//			m_comboTargetIdx = 0;
//		} 
//		else 
//		{
//			m_comboTargetIdx -= 1;
//		}
//
		//262
		platform.transform.localPosition = new Vector3 (0, m_currentY, 262f);
		platform.transform.localEulerAngles = new Vector3 (0, 50 * m_direction, 0);
		platform.GetComponent<StackPlatform> ().Move (m_platformIdx, m_direction, m_speed, m_spawnTimeTarget);
		m_platformIdx++;
		m_spawnTime = 0;
		m_currentY += 23f;
	}

//	private void SpawnPlatform2()
//	{
//		m_curDirection++;
//		if (m_curDirection > m_listDirections.Count - 1) 
//		{
//			m_curDirection = 0;
//		}
//
//		GameObject platform = ZObjectMgr.Instance.Spawn2D (m_listOfPlatforms [0].name, Vector2.zero);
//		m_direction = GetSpawnDirection();	
//
//		//float speed = 1;
//		m_spawnTimeTarget = GetDelayTime();
//
//		if (m_comboTargetIdx > 0) 
//		{
//			Debug.Log ("Delay: " + m_spawnTimeTarget + "_ Speed: " + m_speed);
//			//m_spawnTimeTarget -= 0.2f;
//
//			int score = GameScene.instance.GetScore ();
//			float randVal = Random.Range (0, 100);
//			if (score < 20) {// 0.9f/0.8f;
//				if (randVal < 70) {
//					m_spawnTimeTarget = 0.9f;
//				} else {
//					m_spawnTimeTarget = 0.8f;
//				}
//			} else{
//				if (randVal < 70) {
//					m_spawnTimeTarget = 0.75f;
//				} else {
//					m_spawnTimeTarget = 0.65f;
//				}
//			}
//		}
//
//		// Check if there is a combo to follow
//		if (m_comboTargetIdx <= 0) 
//		{
//			m_curDifficulty++;
//			if (m_curDifficulty > m_listOfDifficulty.Count - 1) 
//			{
//				m_curDifficulty = 0;
//			}
//
//			m_speed = GetDifficulty (m_curDifficulty);
//			m_speedster = m_speed;
//
//			m_comboTargetIdx = 0;
//		} 
//		else 
//		{
//			m_comboTargetIdx -= 1;
//		}
//
//		//262
//		platform.transform.localPosition = new Vector3 (0, m_currentY, 262f);
//		platform.transform.localEulerAngles = new Vector3 (0, 50 * m_direction, 0);
//		platform.GetComponent<StackPlatform> ().Move (m_platformIdx, m_curDirection, m_speed);
//		m_platformIdx++;
//		m_spawnTime = 0;
//		m_currentY += 23f;
//	}

	private float GetDelayTime()
	{
		float retVal = 1;
		int score = GameScene.instance.GetScore();
//		if (score < 7) {
//			retVal = 1.3f;
//		} else if (score < 15) {
//			float randVal = Random.Range (0, 100);
//
//			if (randVal < 30) {
//				retVal = Random.Range (1f, 1.75f);
//			} else {
//				retVal = Random.Range (0.8f, 0.7f);
//			}
//		}

		if (score < 15) {
			float randVal = Random.Range (0, 100);

			if (randVal < 30) 
			{
				retVal = 0.9f;
			} 
			else 
			{
				retVal = 1f;
			}
		} else {
			float randVal = Random.Range (0, 100);

			if (randVal < 20) {
				retVal = 0.7f;
			} else if (randVal < 90) {
				retVal = 0.9f;
			} else {
				retVal = 1.4f;
			}
		}


		return retVal;
	}

	// There are 2 values in the list
	// L and R
	// L_x and R_x [x is a number value of the combo]
	private int GetSpawnDirection()
	{
		int retVal = 1;
		string dirTuning = m_listDirections [m_curDirection];


		if (dirTuning != "L" && dirTuning != "R") {
			string[] difficultyData = dirTuning.Split (new string[] { "_" }, System.StringSplitOptions.None);
			dirTuning = difficultyData [0];
			m_comboTargetIdx = int.Parse (difficultyData [1]);
		} else {
			dirTuning = m_listDirections [m_curDirection]; 
		}

		if (dirTuning == "L") 
		{
			retVal = -1;
		} 
		else if (dirTuning == "R") 
		{
			retVal = 1;
		} 

		return retVal;
	}

	private GameObject GetRandomPlatform()
	{
		string objName = m_listOfPlatforms [0].name;
//		float randVal = Random.Range (0, 90);
//
//		if (randVal < 30) {
//			objName = m_listOfPlatforms [0].name;
//		} else if (randVal < 60) {
//			objName = m_listOfPlatforms [1].name;
//		} else if (randVal < 90) {
//			objName = m_listOfPlatforms [2].name;
//		}
		return ZObjectMgr.Instance.Spawn2D (objName, Vector2.zero);
	}
		

	private Vector2 GetDirection(int p_direction)
	{
		Vector2 retVal = Vector2.zero;
		retVal = new Vector2 (0, m_currentY);
		return retVal;
	}

	private float GetDifficulty(int p_difficulty)
	{
		int score = GameScene.instance.GetScore ();
		float randVal = Random.Range (0, 100);

		if (score < 15) 
		{
			if (randVal < 30) {
				return 1.1f;
			} else {
				return 1.5f;
			} 
		} 
		else if (score < 50) 
		{
			if (randVal < 30) {
				return 1f;
			} else if (randVal < 90) {
				return 1.1f;
			} else {
				return 0.95f;
			}
		}
		else if (score < 70) 
		{
			if (randVal < 30) 
			{
				return 0.6f;
			} 
			else 
			{
				return 0.6f;
			}
		} 
		else 
		{
			return 1f;
		}
//		else 
//		{
//			return 0.75f;
//		}

//		if (score < 3) {
//			return 1.4f;
//		} else if (score < 7) {
//			return Random.Range (1, 1.2f);
//		} else if (score < 15) {
//			float randVal = Random.Range (0, 100);
//
//			if (randVal < 30) {
//				return Random.Range (1, 1.2f);
//			} else if (randVal < 40) {
//				return 1.5f;
//			} else {
//				return Random.Range (0.9f, 1f);
//			}
//		} else if (score < 25) {
//			float randVal = Random.Range (0, 100);
//
//			if (randVal < 10) {
//				return Random.Range (1, 1.2f);
//			} else if (randVal < 40) {
//				return 1.5f;
//			} else {
//				return Random.Range (0.7f, 0.75f);
//			}
//		}

		return 1.2f;
	}
}
