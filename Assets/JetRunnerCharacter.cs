﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetRunnerCharacter : MPCharacter {

	public void OnCollisionEnter2D(Collision2D p_other)
	{
		if (p_other.gameObject.tag == "Floor") // GameOver 
		{
			GameScene.instance.Die ();
		}
	}
}
