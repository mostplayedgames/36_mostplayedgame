﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FDDieChecker : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D p_other)
	{
		if (p_other.gameObject.tag == "Platform") 
		{
			GameScene.instance.Die ();
		}
	}
}
