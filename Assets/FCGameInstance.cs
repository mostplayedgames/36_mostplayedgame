﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FCGameInstance : MPGameInstance 
{
	public GameObject m_objMainCharacter;
	public GameInstanceStates m_curState;

	public override void Gamescene_SetupLevel ()
	{
		m_curState = GameInstanceStates.IDLE;
	}

	public override void Gamescene_Score ()
	{
	}

	public override void Gamescene_Die ()
	{
	}

	public override void Gamescene_Update ()
	{
	}

	public override void Gamescene_ButtonUp (){}
	public override void Gamescene_ButtonDown (){}
}
