﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SHGameState
{
	IDLE,
	MOVING,
	RESULT
}

public class SHGameInstance : MPGameInstance 
{
	public GameObject m_mainCharacter;

	public SHGameState m_curState;

	public AudioClip m_audioCharge;
	public AudioClip m_audioPlatformLand;

	public GameObject m_prefabPlatform;
	public GameObject m_prefabScalingPlatform;

	public GameObject m_currentScalingPlatform;
	public GameObject m_currentPlatform;
	public GameObject m_previousPlatform;

	public int m_platformIdx;
	public int m_tuningIdx;

	public float m_zScaleValue;
	public float m_currentX;

	public bool m_touched;

	public Color m_colorBg;

	public List<Vector2> m_listOfEasyTuning = new List<Vector2>();
	public List<Vector2> m_listOfMedTuning = new List<Vector2> ();
	public List<Vector2> m_listOfHardTuning = new List<Vector2>();


	void Awake () 
	{
		ZObjectMgr.Instance.AddNewObject (m_prefabPlatform.name, 30, this.gameObject);
		ZObjectMgr.Instance.AddNewObject (m_prefabScalingPlatform.name, 5, this.gameObject);
	}

	public override void Gamescene_SetupLevel ()
	{
		m_curState = SHGameState.IDLE;

		m_tuningIdx = Random.Range (0, m_listOfEasyTuning.Count);

		Camera.main.transform.localPosition = new Vector3 (81, 75, -250);
		Camera.main.transform.localEulerAngles = Vector3.zero;
		Camera.main.orthographicSize = 200f;
		Camera.main.backgroundColor = m_colorBg;

		// -13.3f
		m_mainCharacter.transform.position = new Vector3 (18f, -22.5f, 63.8f);
		m_mainCharacter.transform.localEulerAngles = new Vector3(0, 180, 0);
		m_mainCharacter.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		m_mainCharacter.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		m_mainCharacter.GetComponent<Collider2D> ().isTrigger = false;
		m_mainCharacter.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;

		m_currentX = 0;
		SpawnPlatform ();
		UpdateCamera ();
	}

	public override void Gamescene_Update ()
	{
		Controls ();
	}

	public override void Gamescene_Score ()
	{
		m_curState = SHGameState.IDLE;
		SpawnPlatform ();
		UpdateCamera ();
		GameScene.instance.AddCoins (1);
	}

	public override void Gamescene_ButtonDown (){}
	public override void Gamescene_ButtonUp (){}
	public override void Gamescene_Die (){}

	private void SpawnPlatform()
	{
		m_tuningIdx++;
		if (m_tuningIdx >= m_listOfEasyTuning.Count) 
		{
			m_tuningIdx = 0;
		}

		float dist = 60;
		float size = 25;

		int score = GameScene.instance.GetScore ();

		if (score < 3) {
			dist = m_listOfEasyTuning [m_tuningIdx].x;
			size = m_listOfEasyTuning [m_tuningIdx].y;
		} else if (score < 14) {
			dist = m_listOfMedTuning [m_tuningIdx].x;
			size = m_listOfMedTuning [m_tuningIdx].y;
		} else {
			dist = m_listOfHardTuning [m_tuningIdx].x;
			size = m_listOfHardTuning [m_tuningIdx].y;
		}

		m_currentX += dist * 2;

		m_previousPlatform = m_currentPlatform;

		m_currentPlatform = ZObjectMgr.Instance.Spawn2D (m_prefabPlatform.name, Vector2.zero);
		m_currentPlatform.transform.localPosition = new Vector3 (m_currentX, -108, 59);

		m_currentPlatform.GetComponent<SHPlatform> ().Initialize (size);
	}


	private void Controls()
	{
		if (m_curState == SHGameState.IDLE) {
			IdleControls ();
		}
		else if(m_curState == SHGameState.MOVING)
		{
			MovingControls ();
		}
	}

	private void IdleControls()
	{
		if (Input.GetMouseButtonDown (0)) {
			ZAudioMgr.Instance.PlaySFX (m_audioCharge, true);

			m_currentScalingPlatform = ZObjectMgr.Instance.Spawn2D (m_prefabScalingPlatform.name, m_mainCharacter.transform.position);
			m_currentScalingPlatform.transform.position = new Vector3 (m_mainCharacter.transform.position.x + 10, m_mainCharacter.transform.position.y, 60.5f);
			m_currentScalingPlatform.transform.localEulerAngles = new Vector3 (0, 0, 90);		
			m_currentScalingPlatform.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
			m_currentScalingPlatform.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
			m_currentScalingPlatform.GetComponent<Rigidbody2D> ().angularVelocity = 0;

			m_zScaleValue = 0;
		}
		if (Input.GetMouseButton (0)) 
		{			
			m_zScaleValue += 3.2f * Time.deltaTime;
			m_currentScalingPlatform.transform.localScale = new Vector3 (m_zScaleValue, 1, 1);
			LeanTween.cancel (m_currentScalingPlatform);
			LeanTween.rotateZ (m_currentScalingPlatform, 90, 0.3f).setOnComplete (RotatePlatform);
		}
		if (Input.GetMouseButtonUp (0)) 
		{
			ZAudioMgr.Instance.StopSFX (m_audioCharge);
			m_curState = SHGameState.MOVING;
		}
	}

	private void MovingControls()
	{
//		if (Input.GetMouseButtonDown (0)) 
//		{
//			float xAngle = m_mainCharacter.transform.localEulerAngles.x;
//			xAngle = (xAngle == 0) ? 180 : 0;
//			m_mainCharacter.transform.localEulerAngles = new Vector3 (xAngle, m_mainCharacter.transform.localEulerAngles.y, m_mainCharacter.transform.localEulerAngles.z);
//		}
	}

	private void RotatePlatform()
	{
		m_currentScalingPlatform.GetComponent<SHScalingPlatform> ().Initialize ();
		LeanTween.cancel (m_currentScalingPlatform);
		LeanTween.rotateZ (m_currentScalingPlatform, 0, 0.35f).setOnComplete (StartMoving).setEase(LeanTweenType.easeInOutQuad);
	}

	private void StartMoving()
	{
		ZAudioMgr.Instance.PlaySFX (m_audioPlatformLand);

		m_currentScalingPlatform.GetComponent<SHScalingPlatform> ().StartChecking ();
		int platformStatus = m_currentScalingPlatform.GetComponent<SHScalingPlatform> ().GetPlatformStatus ();
		float updateXpos = m_currentScalingPlatform.GetComponent<SHScalingPlatform>().m_endPoint.transform.position.x;

		if (platformStatus > 0) 
		{
			updateXpos = m_currentScalingPlatform.GetComponent<SHScalingPlatform> ().m_platformLanded.GetBounds ();
		}

		LeanTween.cancel (m_mainCharacter);
		m_mainCharacter.transform.localScale = new Vector3 (1.27f, 1.3f, 1.27f);
		LeanTween.scaleY (m_mainCharacter, 0.9f, 0.1f).setLoopPingPong ().setEase (LeanTweenType.easeInOutExpo);;
		LeanTween.moveLocalX (m_mainCharacter, updateXpos, 0.8f).setOnComplete (DestinationReached);
	}

	private void DestinationReached()
	{
		LeanTween.cancel (m_mainCharacter);
		m_mainCharacter.transform.localScale = new Vector3 (1.27f, 1.3f, 1.27f);

		int platformStatus = m_currentScalingPlatform.GetComponent<SHScalingPlatform> ().GetPlatformStatus ();
		if (platformStatus > 0) 
		{
			if (platformStatus == 1) {
			GameScene.instance.Score (platformStatus);
			} else if (platformStatus == 2) {
				GameScene.instance.Score (platformStatus, true);
			}
			m_previousPlatform.GetComponent<SHPlatform> ().PlatformReached ();
		} 
		else 
		{
			m_currentScalingPlatform.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;
			m_currentScalingPlatform.GetComponent<Collider2D> ().isTrigger = true;
			m_currentScalingPlatform.GetComponent<Rigidbody2D> ().AddTorque (-100f);

			m_mainCharacter.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;
			m_mainCharacter.GetComponent<Collider2D> ().isTrigger = true;
			m_mainCharacter.GetComponent<Rigidbody2D> ().AddTorque (-100f);
			GameScene.instance.Die ();
		}
	}

	private void UpdateCamera()
	{
		LeanTween.cancel (Camera.main.gameObject);
		LeanTween.moveLocalX (Camera.main.gameObject, m_mainCharacter.transform.localPosition.x + 80.0f, 0.3f);
	}
}
