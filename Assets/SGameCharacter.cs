﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SGameCharacter : MonoBehaviour {

	public SGameInstance m_SGameInstance;
	public bool m_isGrounded;
	public RaycastHit2D m_rayHit;
	public int m_curPatternIdx;
	public int m_platformPatternIdx;

	public void Initialize()
	{
		m_curPatternIdx = -1;
		m_platformPatternIdx = 0;
	}

	public void Jump()
	{
		this.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
		//20000
		this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, 18000f));
	}

	public void OnCollisionEnter2D(Collision2D p_other)
	{
		if (m_SGameInstance.m_curState != GameInstanceStates.INGAME) {
			return;
		}

		if (p_other.gameObject.tag == "Die") 
		{
			//int dir = m_SGameInstance.m_listDirections[m_SGameInstance.m_curDirection];
			int dir = m_SGameInstance.m_direction;
			this.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;

			if (dir == 1) {
				this.GetComponent<Rigidbody2D>().AddForce(new Vector2(15000,15000));
			} else {
				this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-15000,15000));
			}
				
			GameScene.instance.Die ();
			m_SGameInstance.m_curState = GameInstanceStates.GAMEOVER;
		} 
		else if (p_other.gameObject.tag == "Platform") 
		{
			LeanTween.cancel (p_other.transform.parent.gameObject);
			m_isGrounded = true;
			if (p_other.transform.parent.gameObject.GetComponent<StackPlatform> ()) 
			{
				m_platformPatternIdx = p_other.transform.parent.gameObject.GetComponent<StackPlatform> ().m_platformIdx;
				bool didReachedEnding = p_other.transform.parent.gameObject.GetComponent<StackPlatform> ().DidReachedEnding ();
		
				if (m_SGameInstance.m_curState != GameInstanceStates.IDLE &&
				   m_platformPatternIdx > m_curPatternIdx) 
				{
					//int scoreValue = (didReachedEnding) ? (2) : (1);
					GameScene.instance.Score (1);
					m_curPatternIdx++;
				}
			}
		}
	}

	public void OnCollisionExit2D(Collision2D p_other)
	{
		if (p_other.gameObject.tag == "Platform") 
		{
			m_isGrounded = false;
		}
	}
}
