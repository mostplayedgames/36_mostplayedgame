﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	bool m_isCurve = false;
	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		if (this.GetComponent<Rigidbody2D> ().velocity.y < 0) {
			this.GetComponent<CircleCollider2D> ().enabled = true;
		} else {
			this.GetComponent<CircleCollider2D> ().enabled = false;
		}

		if (m_isCurve) {
			this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (600 * Time.deltaTime, 0));
		}
	}

	public void Reset()
	{
		m_isCurve = false;
	}

	public void Curve()
	{
		m_isCurve = true;
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Die") {
			GameScene.instance.Die ();
		}
	}
}
