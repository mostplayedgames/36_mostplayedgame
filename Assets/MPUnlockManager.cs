﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MPUnlockManager : ZSingleton<MPUnlockManager> 
{
	public GameObject m_objRoot;

	public GameObject m_boxObject;
	public GameObject m_objButtonUnlock;
	public GameObject m_objectFlash;

	public ParticleSystem m_particleCelebrate;

	public Text m_textRarity;
	public Text m_textName;
	public Text m_textPrice;

	public AudioClip m_audioButton;
	public AudioClip m_audioReward;
	public AudioClip m_audioCharge;
	public AudioClip m_audioPop;
	public AudioClip m_audioCelebrate;
	public AudioClip m_audioFall;

	public List<int> m_listToRandomUnlock = new List<int>();

	public int m_currentUnlockedBird;
	private int m_unlockCount;
	private int m_currentShopRarity;

	private float m_currentTimer;
	private float m_currentTimerUp;

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.M))
		{
			SetupScene ();
			GameScene.instance.m_eState = GAME_STATE.UNLOCK;
		}
	}

	public void SetupScene()
	{
		m_objRoot.gameObject.SetActive (true);

		GameScene.instance.m_rootMain.gameObject.SetActive (false);
		GameScene.instance.m_scriptGamesceneInstance.gameObject.SetActive (false);
		Camera.main.orthographicSize = 100;
		Camera.main.transform.localPosition = new Vector3 (0, 54, -250f);
		Camera.main.transform.localEulerAngles = new Vector3(10,0,0);
		m_objButtonUnlock.gameObject.SetActive (true);

		m_textRarity.text = "";

		m_textName.gameObject.SetActive (true);
		m_textName.text = "UNLOCK\nNEW GIFT!";
		m_textName.transform.localScale = new Vector3 (1, 1, 1);
		LeanTween.scale (m_textName.gameObject, new Vector3 (1.1f, 1.1f, 1.1f), 1f).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);

		m_textPrice.text = "" + MPShopManager.Instance.GetCurrentShopPrice ();
		m_currentUnlockedBird = PlayerPrefs.GetInt ("currentUnlockedBird");

		Vector3 currentPosition;
		currentPosition = m_boxObject.gameObject.transform.localPosition;
		m_boxObject.gameObject.SetActive (true);
		m_boxObject.gameObject.transform.localPosition += new Vector3 (0, 30, 0);
		m_boxObject.transform.localEulerAngles = new Vector3 (0, 0, 0);
		m_boxObject.transform.localScale = new Vector3 (120f, 120f, 120f);
		LeanTween.cancel (m_boxObject);
		LeanTween.moveLocal (m_boxObject.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic);

		ZAudioMgr.Instance.PlaySFX (m_audioFall);
	}

	public void UnlockNow()
	{
		MPShopManager.Instance.BuyItem(m_listToRandomUnlock[m_currentUnlockedBird]);
		m_objButtonUnlock.SetActive (false);
		int unlockType = m_listToRandomUnlock [m_currentUnlockedBird+1];
		if (unlockType >= 20) 
		{
			m_unlockCount = 13;
			m_currentShopRarity = 2;
		} 
		else if (unlockType >= 12) 
		{
			m_unlockCount = 14;
			m_currentShopRarity = 1;
		} 
		else 
		{
			m_unlockCount = 15;
			m_currentShopRarity = 0;
		}
		m_currentTimer = 0.001f;
		m_currentTimerUp = 0;
		m_textRarity.text = "";

		m_textName.text = "";
		UnlockRoutine ();
	}

	void UnlockRoutine()
	{
		if( m_unlockCount < 5 )
			LeanTween.rotateLocal (m_boxObject, m_boxObject.transform.localEulerAngles + new Vector3 (0, 90, 0), 0.2f).setEase(LeanTweenType.easeInOutCubic).setOnComplete(UnlockRoutineStop);
		else
			LeanTween.rotateLocal (m_boxObject, m_boxObject.transform.localEulerAngles + new Vector3 (0, 90, 0), 0.12f).setEase(LeanTweenType.easeInOutCubic).setOnComplete(UnlockRoutineStop);

		m_unlockCount--;
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		m_currentTimerUp += 0.005f;
		m_currentTimer += m_currentTimerUp;
	}

	void UnlockRoutineStop()
	{
		if (m_unlockCount <= 0) {
			LeanTween.scale (m_boxObject, m_boxObject.transform.localScale + new Vector3 (20f, 20f, 20f), 0.2f).setEase (LeanTweenType.easeInOutCubic).setOnComplete(UnlockRoutineStop2);//.setLoopPingPong ().setLoopCount (2);
			ZAudioMgr.Instance.PlaySFX (m_audioReward);
		} else {
			LeanTween.delayedCall (m_currentTimer, UnlockRoutine);
		}
	}

	void UnlockRoutineStop2()
	{
		LeanTween.delayedCall(0.15f, UnlockRoutineShake);
	}

	void UnlockRoutineShake()
	{
		ZAudioMgr.Instance.PlaySFX (m_audioCharge);

		LeanTween.scale (m_boxObject, m_boxObject.transform.localScale - new Vector3 (0, 50f, 0), 2f).setEase (LeanTweenType.easeInOutCubic).setOnComplete(Explode);//.setOnComplete(UnlockRoutineShake);//.setLoopPingPong ().setLoopCount (2);
		m_boxObject.transform.localEulerAngles += new Vector3(0,10,0);
		LeanTween.rotate (m_boxObject, m_boxObject.transform.localEulerAngles + new Vector3 (0, -10, 0), 0.04f).setLoopPingPong ();//.setEase(LeanTweenType.easeInOutQuad;
	}

	void Explode()
	{
		LeanTween.scale (m_boxObject, m_boxObject.transform.localScale + new Vector3 (200f, 200f, 200f), 0.2f);
		m_particleCelebrate.GetComponent<ParticleSystem> ().Clear ();
		m_particleCelebrate.gameObject.SetActive (true);
		m_particleCelebrate.GetComponent<ParticleSystem> ().Play ();
		if (m_currentShopRarity == 2) {
			m_particleCelebrate.GetComponent<ParticleSystem> ().Emit (200);
		} else if (m_currentShopRarity == 1) {
			m_particleCelebrate.GetComponent<ParticleSystem> ().Emit (100);
		}
		LeanTween.delayedCall (0.2f, Explode2);

		ZAudioMgr.Instance.PlaySFX (m_audioPop);
		ZAudioMgr.Instance.StopSFX (m_audioCharge);
	}

	void Explode2()
	{
		m_objectFlash.gameObject.SetActive (true);


		LeanTween.delayedCall (0.1f, DisableFlash);
	}

	void DisableFlash()
	{
		UnlockNextBird ();
		m_objectFlash.gameObject.SetActive (false);
	}

	void UnlockNextBird()
	{
		m_boxObject.gameObject.SetActive (false);
		m_currentUnlockedBird = PlayerPrefs.GetInt ("currentUnlockedBird");
		m_currentUnlockedBird++;
		//GameScene.instance.BuyItem (m_listToRandomUnlock[m_currentUnlockedBird]);
		PlayerPrefs.SetInt ("currentUnlockedBird", m_currentUnlockedBird);
		PlayerPrefs.Save ();

		MPShopManager.Instance.SelectItem (m_currentUnlockedBird - 1);
		ZAudioMgr.Instance.PlaySFX (m_audioCelebrate);

		LeanTween.delayedCall (1f, UnlockNextBirdRoutine);
		GameScene.instance.SetupLevel ();
		GameScene.instance.ShowShop (true);
		m_objRoot.gameObject.SetActive (false);
	}

	void UnlockNextBirdRoutine()
	{
		GameScene.instance.ResetCoins ();
	}
}
