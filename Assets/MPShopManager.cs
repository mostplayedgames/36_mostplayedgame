﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MPShopManager : ZSingleton<MPShopManager> 
{
	public GameObject m_objRoot;

	public GameObject m_prefabShopBtn;
	public GameObject m_objItemParent;
	public GameObject m_objShopBtnParent;

	public List<GameObject> m_listOfShopItems = new List<GameObject>();
	public List<MPShopButton> m_listOfShopButtons = new List<MPShopButton>();
	public List<int> m_listOfPrices = new List<int> ();

	[Space]
	public GameObject m_boxObject;
	public GameObject m_objectShopContainer;
	public GameObject m_objectButton;
	public GameObject m_objectScroll;

	public Color m_colorItemSelected;
	public Color m_colorItemNotSelected;

	public AudioClip m_audioButton;

	public ParticleSystem m_particleCelebrate;

	public Text m_textButtonPrice;
	public Text m_textRarity;

	public Sprite m_spriteLocked;
	public Sprite m_spriteUnlocked;

	public Text m_textName;

	private int m_currentPurchaseCount;
	private int m_currentUnlockedItem;

	public void InitializeManager()
	{
		m_currentUnlockedItem = PlayerPrefs.GetInt ("currentUnlockedItem");
		m_currentPurchaseCount = PlayerPrefs.GetInt ("currentPurchaseCount");
		ReadGameItemsInResources ();
		SetupButtons ();
	}

	public void Back()
	{
		m_objRoot.gameObject.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		GameScene.instance.m_rootUIScore.gameObject.SetActive (true);
		GameScene.instance.m_rootMain.gameObject.SetActive (true);
		GameScene.instance.m_eState = GAME_STATE.IDLE;
		GameScene.instance.SetupLevel ();
	}

	public void ShowShopScreen()
	{
		m_objRoot.gameObject.SetActive (true);

		foreach (MPShopButton btn in m_listOfShopButtons) 
		{
			btn.gameObject.SetActive (true);
		}

		m_textName.transform.localScale = new Vector3 (1, 1, 1);
		m_textButtonPrice.text = "" + GetCurrentShopPrice ();

		int currentItemSelected = GameScene.instance.GetCurrentSelectedItem ();
		SelectItem (currentItemSelected);

		m_objectButton.SetActive (true);
		m_objectShopContainer.SetActive (true);
		Vector3 currentPosition = m_objectShopContainer.gameObject.transform.localPosition;
		m_objectShopContainer.gameObject.transform.localPosition -= new Vector3 (0, 60, 0);
		LeanTween.moveLocal (m_objectShopContainer.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic);

		GameScene.instance.m_rootUIScore.gameObject.SetActive (false);
		GameScene.instance.m_rootMain.gameObject.SetActive (false);
	}

	public void SelectItem(int p_currentSelectedItem)
	{
		for (int idx = 0; idx < m_listOfShopButtons.Count; idx++) 
		{
			bool isItemUnlocked = m_listOfShopItems [idx].GetComponent<MPShopItemData> ().IsItemUnlocked ();

			if (isItemUnlocked) 
			{
				InitializeItemUnlocked (idx, p_currentSelectedItem);
			} 
			else 
			{
				InitializeItemLocked (idx);
			}
		}
	}

	public void BuyItem(int p_count)
	{
		m_listOfShopItems [p_count].GetComponent<MPShopItemData> ().UnlockItem();
		GameScene.instance.SpendCoins(GetCurrentShopPrice());
		GameScene.instance.SetItem (p_count);

		m_currentPurchaseCount++;
		PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
	}

	public int GetCurrentShopPrice()
	{
		return m_listOfPrices [m_currentPurchaseCount];
	}
		
	private bool IsItemUnlocked(int p_itemId)
	{
		return m_listOfShopItems [p_itemId].GetComponent<MPShopItemData> ().IsItemUnlocked ();
	}
		
	private void ReadGameItemsInResources()
	{
		Object[] GameItems = Resources.LoadAll ("MPGameItems", typeof(GameObject));

		foreach (GameObject item in GameItems) 
		{
			GameObject obj = Instantiate (item.gameObject, Vector3.zero, Quaternion.identity);
			obj.transform.parent = m_objItemParent.transform;
			obj.transform.localPosition = Vector3.zero;
			obj.transform.localEulerAngles = Vector3.zero;
			obj.transform.localScale = new Vector3 (1, 1, 1);
			m_listOfShopItems.Add (item);	
		}

		uint buttonCount = (uint)m_listOfShopItems.Count;
		ZObjectMgr.Instance.AddNewObject (m_prefabShopBtn.name, buttonCount, this.gameObject);
	}
		
	private void SetupButtons()
	{
		for (int idx = 0; idx < m_listOfShopItems.Count; idx++) 
		{
			GameObject objBtn = ZObjectMgr.Instance.Spawn2D (m_prefabShopBtn.name, Vector2.zero);
			MPShopItemData itemData = m_listOfShopItems [idx].GetComponent<MPShopItemData> ();

			objBtn.transform.parent = m_objShopBtnParent.transform;
			objBtn.transform.localPosition = new Vector3 (0, 0, 65);
			objBtn.transform.localScale = new Vector3 (1, 1, 1);
			objBtn.GetComponent<MPShopButton> ().Initialize (itemData);
			m_listOfShopButtons.Add (objBtn.GetComponent<MPShopButton> ());
		}
	}

	private void InitializeItemUnlocked(int p_idx, int p_currentItemSelected)
	{
		if (m_listOfShopButtons [p_idx].m_shopItemData.ItemId == p_currentItemSelected) 
		{
			m_listOfShopButtons [p_idx].m_imgBackground.color = m_colorItemSelected;
			m_textName.text = m_listOfShopItems [p_idx].GetComponent<MPShopItemData> ().ItemName;
			ItemRarity rarity = m_listOfShopItems [p_idx].GetComponent<MPShopItemData> ().Rarity;


			if (rarity == ItemRarity.COMMON) 
			{
				m_textRarity.color = Color.yellow;
				m_textRarity.text = "COMMON";
			} 
			else if (rarity == ItemRarity.EPIC) 
			{
				m_textRarity.color = Color.magenta;
				m_textRarity.text = "EPIC";
			} 
			else if(rarity == ItemRarity.RARE) 
			{
				m_textRarity.color = Color.white;
				m_textRarity.text = "RARE";
			}
		} 
		else 
		{
			m_listOfShopButtons [p_idx].m_imgBackground.color = m_colorItemNotSelected;
		}
		m_listOfShopButtons [p_idx].m_textQuestionMark.gameObject.SetActive(false);
		m_listOfShopButtons [p_idx].m_imgOverlay.gameObject.SetActive (false);
	}

	private void InitializeItemLocked(int p_idx)
	{
		m_listOfShopButtons [p_idx].GetComponent<MPShopButton> ().m_imgOverlay.gameObject.SetActive (true);
		m_listOfShopButtons [p_idx].GetComponent<MPShopButton>().m_textQuestionMark.gameObject.SetActive(true);
	}
}
